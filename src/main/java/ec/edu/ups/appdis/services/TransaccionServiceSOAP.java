
package ec.edu.ups.appdis.services;

import ec.edu.ups.appdis.modelo.Mensaje;
import ec.edu.ups.appdis.on.GestionPaginaClienteLocal;
import ec.edu.ups.appdis.on.GestionTransaccionalLocal;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author gguallpa
 */
@WebService
public class TransaccionServiceSOAP {

    @Inject
    private GestionPaginaClienteLocal gpcl;

    @Inject
    private GestionTransaccionalLocal gtl;

    @WebMethod
    public Mensaje guardarDeposito(String numeroCuenta, double monto) throws Exception {
        Mensaje m = gtl.saveTransactionDeposit(numeroCuenta, monto);
        return m;
    }

    @WebMethod
    public Mensaje guardarRetiro(String numeroCuenta, double monto) throws Exception {
        Mensaje m = gtl.saveTransactionWithdrawal(numeroCuenta, monto);
        return m;
    }

    @WebMethod
    public Mensaje guardarTranferencia(String cuentaOrige, String cuentaDestino, double monto) throws Exception {
        Mensaje m = gtl.guardarTranferencia(cuentaOrige, cuentaDestino, monto);
        return m;
    }

}
