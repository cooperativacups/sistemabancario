
package ec.edu.ups.appdis.services;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.servers.Server;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author gguallpa
 */
@OpenAPIDefinition(servers = { @Server(description = "Servidor Local", url = "/SistemaBancario") })
@ApplicationPath("/rsb")
public class RestApplication extends Application{
    
}
