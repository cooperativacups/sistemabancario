package ec.edu.ups.appdis.services;

import ec.edu.ups.appdis.modelo.Mensaje;
import ec.edu.ups.appdis.on.GestionPaginaClienteLocal;
import ec.edu.ups.appdis.on.GestionTransaccionalLocal;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

/**
 *
 * @author gguallpa
 */
@Path("/transacciones")
public class TransaccionServiceREST {

    @Inject
    private GestionPaginaClienteLocal gpcl;

    @Inject
    private GestionTransaccionalLocal gtl;
    
    @GET
    @Path("/guardarDeposito/{cuentaOrigen}/{monto}")
    @Produces("application/json")
    public Mensaje guardarDeposito(@PathParam("cuentaOrigen") String numeroCuenta, @PathParam("monto") double monto) throws Exception {
        Mensaje m = gtl.saveTransactionDeposit(numeroCuenta, monto);
        return m;
    }

    @GET
    @Path("/guardarRetiro/{cuentaOrigen}/{monto}")
    @Produces("application/json")
    public Mensaje guardarRetiro(@PathParam("cuentaOrigen") String numeroCuenta, @PathParam("monto") double monto) throws Exception {
        Mensaje m = gtl.saveTransactionWithdrawal(numeroCuenta, monto);
        return m;
    }

    @GET
    @Path("/guardarTransferencia/{cuentaOrigen}/{cuentaDestino}/{monto}")
    @Produces("application/json")
    public Mensaje guardarTranferencia(@PathParam("cuentaOrigen") String cuentaOrige, @PathParam("cuentaDestino") String cuentaDestino, @PathParam("monto") double monto) throws Exception {
        Mensaje m = gtl.guardarTranferencia(cuentaOrige, cuentaDestino, monto);
        return m;
    }

}
