package ec.edu.ups.appdis.services;

import ec.edu.ups.appdis.modelo.*;
import ec.edu.ups.appdis.on.GestionMovil;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import java.util.List;

@Path("/movil")
public class ClienteMovilREST {

    @Inject
    private GestionMovil gestionMovil;

    @POST
    @Path("/authenticate")
    @Produces("application/json")
    @Consumes("application/json")
    public Mensaje loginCliente(Cliente cliente) throws Exception {
        return gestionMovil.authentication(cliente);
    }

    @GET
    @Path("/password/{correo}")
    @Produces("application/json")
    @Consumes("application/json")
    public Mensaje changePassword(@PathParam("correo") String correo) throws Exception {
        return gestionMovil.changePassword(correo);
    }

    @GET
    @Path("/session/{usuario}")
    @Produces("application/json")
    @Consumes("application/json")
    public CuentaAhorro session(@PathParam("usuario") String usuario) throws Exception {
        return gestionMovil.readCuenta(usuario);
    }

    @GET
    @Path("/resumenCuenta/{numeroCuenta}")
    @Produces("application/json")
    @Consumes("application/json")
    public CuentaAhorro summaryAccount(@PathParam("numeroCuenta") String numeroCuenta) throws Exception {
        return gestionMovil.summaryAccount(numeroCuenta);
    }

    @GET
    @Path("/resumenCredito/{numeroCuenta}")
    @Produces("application/json")
    @Consumes("application/json")
    public Credito summaryCredito(@PathParam("numeroCuenta") String numeroCuenta) throws Exception {
        return gestionMovil.summaryCredito(numeroCuenta);
    }

    @GET
    @Path("/clientesInternos/{numeroCuenta}")
    @Produces("application/json")
    @Consumes("application/json")
    public List<CuentaAhorro> getClientes(@PathParam("numeroCuenta") String numeroCuenta) throws Exception {
        return gestionMovil.getClientes(numeroCuenta);
    }

    @POST
    @Path("/transferenciaInterna")
    @Produces("application/json")
    @Consumes("application/json")
    public Mensaje saveTranferenciaInterna(Transferencia transferencia) throws Exception {
         return gestionMovil.saveTransferenciaInterna(transferencia);
    }

    @POST
    @Path("/transferenciaExterna")
    @Produces("application/json")
    public Mensaje saveTranferenciaExterna(Transferencia transferencia) throws Exception {
        return gestionMovil.saveTransferenciaExterna(transferencia);
    }

    /*@POST
    @Path("/transferenciaExterna")
    @Produces("application/json")
    @Consumes("application/json")
    public Mensaje saveTranferenciaExterna(Transferencia transferencia) throws Exception {
        return gestionMovil.saveTransferenciaExterna(transferencia);
    }*/

}
