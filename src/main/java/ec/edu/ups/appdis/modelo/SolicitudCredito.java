/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.modelo;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

/**
 *
 * @author ismael
 */
@Entity
public class SolicitudCredito implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    @JoinColumn(name = "cedula")
    private Cliente cliente;
    private int plazosMesesCredito;
    private String historialCredito;
    private String propositoCredito;
    private Double montoCredito;
    private String saldoCuentaAhorro;
    private String tiempoEmpleo;
    private Double tasaPago;
    private String estadoCivilSexo;
    private String garante;
    private double avaluoVivienda;
    private String activos;
    private int edad;
    private String vivienda;
    private int cantidadCreditosExistentes;
    private String empleo;
    private String trabajadorEstranjero;
    private String tipoCliente;

   //pdfs
    private String nombreCedula;
    private String nombreRol;
    private String nombrePlanilla;

    @Lob
    private byte[] archivoCedula;
    @Lob
    private byte[] archivoRol;
    @Lob
    private byte[] archivoPlanilla;
    
   //Datos nuevos
    private String cedulapersonaGarante;
    
    private String estadoSolicitud;

    
    //CREACION DE LOS METODOS GET Y SET

    public String getCedulapersonaGarante() {
        return cedulapersonaGarante;
    }

    public void setCedulapersonaGarante(String cedulapersonaGarante) {
        this.cedulapersonaGarante = cedulapersonaGarante;
        
        
        
    }
    
    public String getEstadoSolicitud() {
        return estadoSolicitud;
    }

    public void setEstadoSolicitud(String estadoSolicitud) {
        this.estadoSolicitud = estadoSolicitud;
    }
    

    public SolicitudCredito() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public int getPlazosMesesCredito() {
        return plazosMesesCredito;
    }

    public void setPlazosMesesCredito(int plazosMesesCredito) {
        this.plazosMesesCredito = plazosMesesCredito;
    }

    public String getHistorialCredito() {
        return historialCredito;
    }

    public void setHistorialCredito(String historialCredito) {
        this.historialCredito = historialCredito;
    }

    public String getPropositoCredito() {
        return propositoCredito;
    }

    public void setPropositoCredito(String propositoCredito) {
        this.propositoCredito = propositoCredito;
    }

    public Double getMontoCredito() {
        return montoCredito;
    }

    public void setMontoCredito(Double montoCredito) {
        this.montoCredito = montoCredito;
    }

    public String getSaldoCuentaAhorro() {
        return saldoCuentaAhorro;
    }

    public void setSaldoCuentaAhorro(String saldoCuentaAhorro) {
        this.saldoCuentaAhorro = saldoCuentaAhorro;
    }

    public String getTiempoEmpleo() {
        return tiempoEmpleo;
    }

    public void setTiempoEmpleo(String tiempoEmpleo) {
        this.tiempoEmpleo = tiempoEmpleo;
    }

    public double getTasaPago() {
        return tasaPago;
    }

    public void setTasaPago(double tasaPago) {
        this.tasaPago = tasaPago;
    }

    public String getEstadoCivilSexo() {
        return estadoCivilSexo;
    }

    public void setEstadoCivilSexo(String estadoCivilSexo) {
        this.estadoCivilSexo = estadoCivilSexo;
    }

    public String getGarante() {
        return garante;
    }

    public void setGarante(String garante) {
        this.garante = garante;
    }

    public Double getAvaluoVivienda() {
        return avaluoVivienda;
    }

    public void setAvaluoVivienda(Double avaluoVivienda) {
        this.avaluoVivienda = avaluoVivienda;
    }
    

    public String getActivos() {
        return activos;
    }

    public void setActivos(String activos) {
        this.activos = activos;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getVivienda() {
        return vivienda;
    }

    public void setVivienda(String vivienda) {
        this.vivienda = vivienda;
    }

    public int getCantidadCreditosExistentes() {
        return cantidadCreditosExistentes;
    }

    public void setCantidadCreditosExistentes(int cantidadCreditosExistentes) {
        this.cantidadCreditosExistentes = cantidadCreditosExistentes;
    }

    public String getEmpleo() {
        return empleo;
    }

    public void setEmpleo(String empleo) {
        this.empleo = empleo;
    }

    public String getTrabajadorEstranjero() {
        return trabajadorEstranjero;
    }

    public void setTrabajadorEstranjero(String trabajadorEstranjero) {
        this.trabajadorEstranjero = trabajadorEstranjero;
    }

    public String getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public String getNombreCedula() {
        return nombreCedula;
    }

    public void setNombreCedula(String nombreCedula) {
        this.nombreCedula = nombreCedula;
    }

    public String getNombreRol() {
        return nombreRol;
    }

    public void setNombreRol(String nombreRol) {
        this.nombreRol = nombreRol;
    }

    public String getNombrePlanilla() {
        return nombrePlanilla;
    }

    public void setNombrePlanilla(String nombrePlanilla) {
        this.nombrePlanilla = nombrePlanilla;
    }

    public byte[] getArchivoCedula() {
        return archivoCedula;
    }

    public void setArchivoCedula(byte[] archivoCedula) {
        this.archivoCedula = archivoCedula;
    }

    public byte[] getArchivoRol() {
        return archivoRol;
    }

    public void setArchivoRol(byte[] archivoRol) {
        this.archivoRol = archivoRol;
    }

    public byte[] getArchivoPlanilla() {
        return archivoPlanilla;
    }

    public void setArchivoPlanilla(byte[] archivoPlanilla) {
        this.archivoPlanilla = archivoPlanilla;
    }

    @Override
    public String toString() {
        return "SolicitudCredito{" + "id=" + id + ", cliente=" + cliente + ", plazosMesesCredito=" + plazosMesesCredito + ", historialCredito=" + historialCredito + ", propositoCredito=" + propositoCredito + ", montoCredito=" + montoCredito + ", saldoCuentaAhorro=" + saldoCuentaAhorro + ", tiempoEmpleo=" + tiempoEmpleo + ", tasaPago=" + tasaPago + ", estadoCivilSexo=" + estadoCivilSexo + ", garante=" + garante + ", avaluoVivienda=" + avaluoVivienda + ", activos=" + activos + ", edad=" + edad + ", vivienda=" + vivienda + ", cantidadCreditosExistentes=" + cantidadCreditosExistentes + ", empleo=" + empleo + ", trabajadorEstranjero=" + trabajadorEstranjero + ", tipoCliente=" + tipoCliente + ", nombreCedula=" + nombreCedula + ", nombreRol=" + nombreRol + ", nombrePlanilla=" + nombrePlanilla + ", archivoCedula=" + archivoCedula + ", archivoRol=" + archivoRol + ", archivoPlanilla=" + archivoPlanilla + '}';
    }

   
    
    
    
    

   
}
