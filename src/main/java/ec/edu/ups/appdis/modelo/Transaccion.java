/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * En esta Clase Define los atributos para la entidad Transacción
 * @author Jorge Arévalo
 * @version 08/06/2020
 */
@Entity
public class Transaccion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String fecha;
    private String hora;
    private double monto;
    private String tipo;

    @ManyToOne
    @JoinColumn(name = "numeroCuenta")
    private CuentaAhorro cuentaAhorro;

    public Transaccion() {
    }

    public Transaccion(int id, String fecha, String hora, double monto, String tipo, CuentaAhorro cuentaAhorro) {
        this.id = id;
        this.fecha = fecha;
        this.hora = hora;
        this.monto = monto;
        this.tipo = tipo;
        this.cuentaAhorro = cuentaAhorro;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public CuentaAhorro getCuentaAhorro() {
        return cuentaAhorro;
    }

    public void setCuentaAhorro(CuentaAhorro cuentaAhorro) {
        this.cuentaAhorro = cuentaAhorro;
    }

    @Override
    public String toString() {
        return "Transaccion{" + "id=" + id + ", fecha=" + fecha + ", hora=" + hora + ", monto=" + monto + ", tipo=" + tipo + ", cuentaAhorro=" + cuentaAhorro + '}';
    }

}
