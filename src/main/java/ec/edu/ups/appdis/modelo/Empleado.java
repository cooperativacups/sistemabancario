/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.modelo;

import java.util.Date;
import javax.persistence.Entity;

/**
 * En esta Clase Define los atributos para la entidad Empleado que hereda de la
 * clase Persona
 * @author Ismael Castillo
 * @version 08/06/2020
 */

@Entity
public class Empleado extends Persona{
    
    private Double sueldo;
    private String rol;

    public Empleado() {
    }

    public Empleado(Double sueldo, String rol, String cedula, String nombre, String apellido, String fechaNacimiento, String direccion, String telefono, String operadora, String correo, String usuario, String contrasena) {
        super(cedula, nombre, apellido, fechaNacimiento, direccion, telefono, operadora, correo, usuario, contrasena);
        this.sueldo = sueldo;
        this.rol = rol;
    }

    

    public Double getSueldo() {
        return sueldo;
    }

    public void setSueldo(Double sueldo) {
        this.sueldo = sueldo;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    @Override
    public String toString() {
        return "Empleado{" + "sueldo=" + sueldo + ", rol=" + rol + '}';
    }
}