/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.modelo;

/**
 *
 * @author ISMAEL
 */
public class ClienteDjango {
    
    private int id;
    private String cedula;
    private int edad;
    private String tipoCliente;

    public ClienteDjango() {
    }

    public ClienteDjango(int id, String cedula, int edad, String tipoCliente) {
        this.id = id;
        this.cedula = cedula;
        this.edad = edad;
        this.tipoCliente = tipoCliente;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    @Override
    public String toString() {
        return "ClienteDjango{"
                + "id=" + id
                + ", cedula='" + cedula + '\''
                + ", edad=" + edad
                + ", tipoCliente='" + tipoCliente + '\''
                + '}';
    }

    
}
