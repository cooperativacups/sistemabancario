/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.modelo;

import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author ismael
 */
@Entity
public class Credito {

    @Id
    private int numeroCredito;
    private String tipoCredito;
    private Double valorCredito;
    private Double saldoCredito;
    private Date fechaVencimiento;

    @ManyToOne
    @JoinColumn(name = "numeroCuenta")
    private CuentaAhorro cuentaAhorro;
    
    @OneToMany
    private List<Amortizacion> tablaAmortizacion;
    private String estadoCredito;
    
    
    public Credito() {

    }
    
    
    public int getNumeroCredito() {
        return numeroCredito;
    }

    public void setNumeroCredito(int numeroCredito) {
        this.numeroCredito = numeroCredito;
    }

    public String getTipoCredito() {
        return tipoCredito;
    }

    public void setTipoCredito(String tipoCredito) {
        this.tipoCredito = tipoCredito;
    }

    public Double getValorCredito() {
        return valorCredito;
    }

    public void setValorCredito(Double valorCredito) {
        this.valorCredito = valorCredito;
    }

    public Double getSaldoCredito() {
        return saldoCredito;
    }

    public void setSaldoCredito(Double saldoCredito) {
        this.saldoCredito = saldoCredito;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public CuentaAhorro getCuentaAhorro() {
        return cuentaAhorro;
    }

    public void setCuentaAhorro(CuentaAhorro cuentaAhorro) {
        this.cuentaAhorro = cuentaAhorro;
    }

    public List<Amortizacion> getTablaAmortizacion() {
        return tablaAmortizacion;
    }

    public void setTablaAmortizacion(List<Amortizacion> tablaAmortizacion) {
        this.tablaAmortizacion = tablaAmortizacion;
    }

    public String getEstadoCredito() {
        return estadoCredito;
    }

    public void setEstadoCredito(String estadoCredito) {
        this.estadoCredito = estadoCredito;
    }

    
    
    @Override
    public String toString() {
        return "Credito [numeroCredito=" + numeroCredito + ", tipoCredito=" + tipoCredito + ", valorCredito="
                + valorCredito + ", saldoCredito=" + saldoCredito + ", fechaVencimiento=" + fechaVencimiento
                + ", cuentaAhorro=" + cuentaAhorro + ", tablaAmortizacion=" + tablaAmortizacion + "]";
    }

}
