/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * En esta Clase Define los atributos para la entidad Cliente
 * @author Ismael Castillo
 * @version 08/06/2020
 */
@Entity
//@PrimaryKeyJoinColumn(name="clienteId")
public class Cliente extends Persona{
    
    /*@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cliente")
    private int id;*/
    @OneToMany(fetch = FetchType.EAGER)
    private List<Acceso> accesos;
    
    @OneToMany
    private List<Credito> creditos;
   
    
    
    public Cliente() {
    }

    public Cliente(String cedula, String nombre, String apellido, String fechaNacimiento, String direccion, String telefono, String operadora, String correo, String usuario, String contrasena) {
        super(cedula, nombre, apellido, fechaNacimiento, direccion, telefono, operadora, correo, usuario, contrasena);
    }

    
    
    public List<Acceso> getAccesos() {
        return accesos;
    }

    public void setAccesos(List<Acceso> accesos) {
        this.accesos = accesos;
    }
    
    

}
