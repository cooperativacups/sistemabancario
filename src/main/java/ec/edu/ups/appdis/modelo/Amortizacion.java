/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ismael
 */
@Entity
public class Amortizacion implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int nPeriodos;
    private double saldoInicial;
    private double cuotaFija;
    private double interes;
    private double abonoCapital;
    private double saldoFinal;
    @Temporal(TemporalType.DATE)
    private Date fechaPago;
    private String estadoCuota;
    private double saldoCuota;
    

    @ManyToOne
    @JoinColumn(name = "numeroCredito")
    private Credito credito;

    public Amortizacion() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getnPeriodos() {
        return nPeriodos;
    }

    public void setnPeriodos(int nPeriodos) {
        this.nPeriodos = nPeriodos;
    }

    public double getSaldoInicial() {
        return saldoInicial;
    }

    public void setSaldoInicial(double saldoInicial) {
        this.saldoInicial = saldoInicial;
    }

    public double getCuotaFija() {
        return cuotaFija;
    }

    public void setCuotaFija(double cuotaFija) {
        this.cuotaFija = cuotaFija;
    }

    public double getInteres() {
        return interes;
    }

    public void setInteres(double interes) {
        this.interes = interes;
    }

    public double getAbonoCapital() {
        return abonoCapital;
    }

    public void setAbonoCapital(double abonoCapital) {
        this.abonoCapital = abonoCapital;
    }

    public double getSaldoFinal() {
        return saldoFinal;
    }

    public void setSaldoFinal(double saldoFinal) {
        this.saldoFinal = saldoFinal;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getEstadoCuota() {
        return estadoCuota;
    }

    public void setEstadoCuota(String estadoCuota) {
        this.estadoCuota = estadoCuota;
    }

    public Credito getCredito() {
        return credito;
    }

    public void setCredito(Credito credito) {
        this.credito = credito;
    }

    public double getSaldoCuota() {
        return saldoCuota;
    }

    public void setSaldoCuota(double saldoCuota) {
        this.saldoCuota = saldoCuota;
    }

   @Override
    public String toString() {
        return "Amortizacion{" + "id=" + id + ", nPeriodos=" + nPeriodos + ", saldoInicial=" + saldoInicial + ", cuotaFija=" + cuotaFija + ", interes=" + interes + ", abonoCapital=" + abonoCapital + ", saldoFinal=" + saldoFinal + ", fechaPago=" + fechaPago + ", estadoCuota=" + estadoCuota + ", saldoCuota=" + saldoCuota + ", credito=" + credito + '}';
    }
    
    
   

}
