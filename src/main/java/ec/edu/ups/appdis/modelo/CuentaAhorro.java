/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * En esta Clase Define los atributos para la entidad CuentaAhorro
 *
 * @author Jorge Arévalo
 * @version 08/06/2020
 */
@Entity
public class CuentaAhorro implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String numeroCuenta;
    private double saldo = 0.0;

    @OneToOne
    @JoinColumn(name = "cedula")
    private Cliente cliente;

    //@OneToMany(fetch = FetchType.EAGER)
    @OneToMany
    private List<Transaccion> transaccions;

    @OneToMany
    private List<Credito> listaCreditos;

    public CuentaAhorro() {
    }

    public CuentaAhorro(String numeroCuenta, double saldo, Cliente cliente, List<Transaccion> transaccions) {
        this.numeroCuenta = numeroCuenta;
        this.saldo = saldo;
        this.cliente = cliente;
        this.transaccions = transaccions;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<Transaccion> getTransaccions() {
        return transaccions;
    }

    public void setTransaccions(List<Transaccion> transaccions) {
        this.transaccions = transaccions;
    }

    public void addTransaccion(Transaccion transaccion) {
        if (transaccions == null) {
            transaccions = new ArrayList<Transaccion>();
        }
        transaccions.add(transaccion);
    }

    public List<Credito> getListaCreditos() {
        return listaCreditos;
    }

    public void setListaCreditos(List<Credito> listaCreditos) {
        this.listaCreditos = listaCreditos;
    }

    @Override
    public String toString() {
        return "CuentaAhorro{" + "numeroCuenta=" + numeroCuenta + ", saldo=" + saldo + ", cliente=" + cliente + ", transaccions=" + transaccions + '}';
    }

}
