/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.mb;

import ec.edu.ups.appdis.modelo.Cliente;
import ec.edu.ups.appdis.modelo.CuentaAhorro;
import ec.edu.ups.appdis.on.GestionClientes;
import ec.edu.ups.appdis.on.GestionCorreo;
import ec.edu.ups.appdis.on.GestionCuentas;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

/**
 * Clase CeuntaAhorro de tipo Managen bean para que sea administrativo con el
 * nombre especificado en el atributo de nombre.
 *
 * @author Ismael Castillo
 * @version 08/06/2020
 */
@ManagedBean
@ViewScoped
public class CuentaAhorroBean {

    @Inject
    private GestionCuentas gestionCuentas;
    @Inject
    private GestionClientes gestionClientes;

    @Inject
    private GestionCorreo gestionCorreo;

    private CuentaAhorro newCuentaAhorro;
    private Cliente newCliente;
    private List<CuentaAhorro> listadoCuentaAhorros;
    private String numerodeCuenta;
    private String cedulaIngresada;
    private String cedulaEncontrada;
    private String titulo;
    private int contador = 0;

    private String nombre;
    private String apellido;
    private String fechaNacimiento;
    private String direccion;
    private String telefono;
    private String operadora;
    private String correo;
    private String usuario;
    private String contrasena;
    private List<Cliente> listclientes;

    @PostConstruct
    public void init() {
        try {
            newCliente = new Cliente();
            newCuentaAhorro = new CuentaAhorro();
            loadDataCuentasAhorro();
        } catch (Exception e) {
        }

    }

    public GestionCuentas getGestionCuentas() {
        return gestionCuentas;
    }

    public void setGestionCuentas(GestionCuentas gestionCuentas) {
        this.gestionCuentas = gestionCuentas;
    }

    public CuentaAhorro getNewCuentaAhorro() {
        return newCuentaAhorro;
    }

    public void setNewCuentaAhorro(CuentaAhorro newCuentaAhorro) {
        this.newCuentaAhorro = newCuentaAhorro;
    }

    public List<CuentaAhorro> getListadoCuentaAhorros() {
        return listadoCuentaAhorros;
    }

    public void setListadoCuentaAhorros(List<CuentaAhorro> listadoCuentaAhorros) {
        this.listadoCuentaAhorros = listadoCuentaAhorros;
    }

    public String getNumeroCuenta() {
        return numerodeCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) throws Exception {
        this.numerodeCuenta = numeroCuenta;
        if (numeroCuenta != null) {
            newCuentaAhorro = gestionCuentas.buscarCuenta(numeroCuenta);
            System.out.println(newCuentaAhorro);
            titulo = "Editar registro de la cuenta " + numeroCuenta;
        }
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getCedulaIngresada() {
        return cedulaIngresada;
    }

    public void setCedulaIngresada(String cedulaIngresada) throws Exception {
        this.cedulaIngresada = cedulaIngresada;
        if (cedulaIngresada != null) {
            newCliente = gestionClientes.buscarCliente(cedulaIngresada);
            titulo = "Editar registro de cliente " + cedulaIngresada;
        }
    }

    public String getCedulaEncontrada() {
        return cedulaEncontrada;
    }

    public void setCedulaEncontrada(String cedulaEncontrada) throws Exception {
        this.cedulaEncontrada = cedulaEncontrada;
        if (cedulaEncontrada != null) {
            newCliente = gestionClientes.buscarCliente(cedulaEncontrada);
            titulo = "Editar registro de cliente " + cedulaEncontrada;
        }
    }

    public GestionClientes getGestionClientes() {
        return gestionClientes;
    }

    public void setGestionClientes(GestionClientes gestionClientes) {
        this.gestionClientes = gestionClientes;
    }

    public Cliente getNewCliente() {
        return newCliente;
    }

    public void setNewCliente(Cliente newCliente) {
        this.newCliente = newCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getOperadora() {
        return operadora;
    }

    public void setOperadora(String operadora) {
        this.operadora = operadora;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public List<Cliente> getListclientes() {
        return listclientes;
    }

    public void setListclientes(List<Cliente> listclientes) {
        this.listclientes = listclientes;
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }

    /**
     * Metodo que permite obtener el listado de los de las cuentas de ahorro
     *
     * @throws Exception
     */
    private void loadDataCuentasAhorro() throws Exception {
        listadoCuentaAhorros = gestionCuentas.listarCuentas();
    }

    /**
     * El meetodo ediatr nos permite revalidar los datos de una cuenta de ahorro
     *
     * @param numeroCuenta
     * @return
     * @throws Exception
     */
    public String editar(String numeroCuenta) throws Exception {
        System.out.println(numeroCuenta);
        return "actualizarCuenta?faces-redirect=true&numerodeCuenta=" + numeroCuenta;
    }

    /**
     * El metodo eliminar nos permite eliminar o quitar una cuenta de la base de
     * datos
     *
     * @param numeroCuenta
     * @return
     * @throws Exception
     */
    public String eliminar(String numeroCuenta) throws Exception {
        System.out.println(" NumeroCuenta a eliminar " + numeroCuenta);
        gestionCuentas.eliminarCuenta(numeroCuenta);
        return "listadoCuentas";
    }

    /**
     * El metodo generarNumeroCuenta nos permite generar una cuenta de ahorros
     * en la base de datos.
     *
     * @return
     */
    public String generarNumeroCuenta() {
        this.cedulaEncontrada = this.cedulaEncontrada + "020";
        this.numerodeCuenta = this.cedulaEncontrada;
        System.out.println(this.numerodeCuenta);
        this.newCuentaAhorro.setNumeroCuenta(numerodeCuenta);
        return null;
    }

    public String agregarCliente() throws Exception {

        return null;
    }

    /**
     * El metodo buscarCliente nos permite obtener un cliente de la base de
     * datos.
     *
     * @return
     * @throws Exception
     */
    public String buscarCliente() throws Exception {
        this.cedulaIngresada = this.cedulaIngresada + "";
        System.out.println("Soy la cedula a buscar " + this.cedulaIngresada);
        this.newCliente = gestionClientes.buscarCliente(this.cedulaIngresada);
        this.cedulaEncontrada = this.newCliente.getCedula();
        this.nombre = this.newCliente.getNombre();
        this.apellido = this.newCliente.getApellido();
        this.fechaNacimiento = this.newCliente.getFechaNacimiento();
        this.direccion = this.newCliente.getDireccion();
        this.telefono = this.newCliente.getTelefono();
        this.operadora = this.newCliente.getOperadora();
        this.correo = this.newCliente.getCorreo();
        this.usuario = this.newCliente.getUsuario();
        this.contrasena = this.newCliente.getContrasena();
        this.numerodeCuenta = this.cedulaEncontrada + "020";
        //this.listclientes.add(newCliente);
        /*System.out.println(this.newCliente.toString());*/
        //gestionUsuarios.guardarUsuario(newUsuario);
        //return "listadoUsuarios";

        return null;
    }

    /**
     * El metodo addCliente nos permite agregar un cliente a la lista de
     * clientes para poderlos manipular en el metodo requerido
     *
     * @param cliente
     * @return
     */
    public String addCliente(Cliente cliente) {
        listclientes = new ArrayList<>();
        listclientes.add(newCliente);
        return null;
    }

    /**
     * El metodo guardarCuentaAhorro nos permite guardar una cuenta de ahorro en
     * la base de datos una ves que se hayan obtenido los datos del cliente al
     * cual pertenecera esta cuenta.
     *
     * @return
     * @throws Exception
     */
    public String guardarCuentaAhorro() throws Exception {
        try {
            this.newCuentaAhorro.setNumeroCuenta(String.valueOf(this.numerodeCuenta));
            System.out.println(this.numerodeCuenta);
            Cliente c = gestionClientes.buscarCliente(this.cedulaIngresada);
            System.out.println(c.toString());
            this.newCuentaAhorro.setCliente(c);
            System.out.println("Soy la cedula para crear el cliente con la cuenta " + this.cedulaIngresada);
            this.newCuentaAhorro.setSaldo(0.00);
            gestionCuentas.guardarCuenta(newCuentaAhorro);

            //gestionCuentas.notificacionCorreo(c.getNombre() + " " + c.getApellido(), c.getUsuario(), c.getContrasena(), "01/06/2020", c.getCorreo());
            gestionCorreo.notificacionCorreo(c, "AperturaCuenta");
            return "listadoCuentas";
        } catch (Exception e) {
            throw e;
        }

        //return null;
    }

    /**
     * El metodo actualizarCuentaAhorro nos permite revalidar los datos de una
     * cuenta de ahorro ya creada en la base de datos en caso de que la misma
     * haya almacenado datos erroneos.
     *
     * @return
     * @throws Exception
     */
    public String actualizarCuentaAhorro() throws Exception {

        /*this.newCuentaAhorro.setNumeroCuenta(String.valueOf(this.numerodeCuenta));
        System.out.println(this.numerodeCuenta);
        Cliente c = gestionClientes.buscarCliente(this.cedulaIngresada);
        System.out.println(c.toString());
        this.newCuentaAhorro.setCliente(c);
        System.out.println("Soy la cedula para crear el cliente con la cuenta " + this.cedulaIngresada);
        this.newCuentaAhorro.setSaldo(0.00);*/
        gestionCuentas.guardarCuenta(newCuentaAhorro);

        return "listadoCuentas";

    }

    /**
     * El metodo reenviarCredenciales nos permite que una ves que se haya creado
     * una cuenta de ahorros para un cliente, en caso de que no le hayan llegado
     * las credenciales, volver a reenviar dichos datos a su correo nuevamente
     *
     * @param cedula
     * @return
     * @throws Exception
     */
    public String reenviarCredenciales(String cedula) throws Exception {
        try {
            Cliente c = gestionClientes.buscarCliente(cedula);
            gestionCorreo.notificacionCorreo(c, "AperturaCuenta");
            return "listadoCuentas";
        } catch (Exception e) {
            throw e;
        }
    }

    public int filas() {

        for (int i = 1; i < this.listadoCuentaAhorros.size(); i++) {
            this.contador = this.contador + i;
            System.out.println(this.contador);
            return this.contador;
        }

        return 0;
    }

}
