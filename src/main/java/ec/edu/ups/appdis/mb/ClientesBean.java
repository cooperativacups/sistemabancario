/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.mb;

import ec.edu.ups.appdis.modelo.Cliente;
import ec.edu.ups.appdis.modelo.Persona;
import ec.edu.ups.appdis.on.GestionClientes;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ValueChangeEvent;
import javax.faces.event.ValueChangeListener;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Clase ClienteBean de tipo Managen bean para que sea administrativo con el
 * nombre especificado en el atributo de nombre.
 *
 * @author Ismael Castillo
 * @version 08/06/2020
 */
@ManagedBean
@ViewScoped
public class ClientesBean {

    @Inject
    private GestionClientes gestionClientes;
    private Cliente newCliente;
    private List<Cliente> listadoClientes;
    private String cedula;
    private String titulo;
    private String nombre;
    private Date fechaNacimiento;
    private String usuario;
    private String contrasena;
    private String tipo;
    private int contador = 0;
    private String cedulaEliminar;
    private String cedulaModal = "";
    private String modal;
    
    private List<Cliente> buscarClientes;

    @PostConstruct
    public void init() {
        try {
            newCliente = new Cliente();
            loadDataClientes();
        } catch (Exception e) {
            System.out.println("ERRO EN VLIENTES BEAN");
        }

    }

    public List<Cliente> getBuscarClientes() {
        return buscarClientes;
    }

    public void setBuscarClientes(List<Cliente> buscarClientes) {
        this.buscarClientes = buscarClientes;
    }
    

    public GestionClientes getGestionClientes() {
        return gestionClientes;
    }

    public void setGestionClientes(GestionClientes gestionClientes) {
        this.gestionClientes = gestionClientes;
    }

    public Cliente getNewCliente() {
        return newCliente;
    }

    public void setNewCliente(Cliente newCliente) {
        this.newCliente = newCliente;
    }

    public List<Cliente> getListadoClientes() {
        return listadoClientes;
    }

    public void setListadoClientes(List<Cliente> listadoClientes) {
        this.listadoClientes = listadoClientes;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) throws Exception {
        this.cedula = cedula;
        if (cedula != null) {
            newCliente = gestionClientes.buscarCliente(cedula);
            titulo = "Editar registro de cliente " + cedula;
        }
    }

    public String getModal() {
        return modal;
    }

    public void setModal(String modal) {
        this.modal = modal;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }

    public String getCedulaModal() {
        return cedulaModal;
    }

    public void setCedulaModal(String cedulaModal) throws Exception {
        this.cedulaModal = cedulaModal;
        if (cedulaModal != null) {
            Cliente c = gestionClientes.buscarCliente(cedulaModal);
            cedulaModal = c.getCedula();
            //titulo = "Editar registro de cliente " + cedula;
        }
    }

    /**
     * Metodo que permite obtener el listado de los clientes.
     *
     * @throws Exception
     */
    private void loadDataClientes() throws Exception {
        listadoClientes = gestionClientes.listarClientes();
    }

    /**
     * metodo que me permite gurdar los datos del nuevo cliente.
     *
     * @return
     * @throws Exception
     */
    public String guardarCliente() throws Exception {
        System.out.println(newCliente.toString());
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        newCliente.setFechaNacimiento(sdf.format(fechaNacimiento));
        gestionClientes.guardarCliente(newCliente);
        //gestionUsuarios.guardarUsuario(newUsuario);
        //return "listadoUsuarios";

        return "listadoClientes";
    }
    
    public String actualizarCliente() throws Exception {
        System.out.println(newCliente.toString());
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        newCliente.setFechaNacimiento(sdf.format(fechaNacimiento));
        gestionClientes.guardarCliente(newCliente);
        //gestionUsuarios.guardarUsuario(newUsuario);
        //return "listadoUsuarios";

        return "listadoClientes";
    }

    /**
     * Metodo para poder actulizar los datos de los clientes.
     *
     * @param cedula
     * @return
     * @throws Exception
     */
    public String editar(String cedula) throws Exception {
        System.out.println(cedula);
        return "actualizarCliente?faces-redirect=true&cedula=" + cedula;
    }

    /**
     * El metodo Eliminar nos permite quitar un cliente de la base de datos
     *
     * @param cedula
     * @return
     * @throws Exception
     */
    public String eliminar(String cedula) throws Exception {
        System.out.println(" Cedula a eliminar normal " + cedula);
        gestionClientes.eliminarCliente(cedula);
        return "listadoClientes";
    }

    public String eliminar2(String cedula) throws Exception {
        System.out.println(" Cedula modal a eliminar " + cedula);
        //gestionClientes.eliminarCliente(cedula);
        return "listadoClientes";
    }

    /**
     * El metodo generarUsuario nos permite generar un usuario mediante el in
     * greso del nombre del cliente adjuntado el texto @cups
     *
     * @return
     */
    public String generarUsuario() {
        String user = this.newCliente.getNombre() + "@cups";
        this.usuario = user;
        this.newCliente.setUsuario(user);
        return null;
    }

    /**
     * El metodo generarContrasena nos permite generar una contrase;a atarves
     * del texto que se le adjunta al campo de tipo input CUps2020@#$
     *
     * @return
     */
    public String generarContrasena() {
        this.contrasena = "CUps2020@#$";
        this.newCliente.setContrasena(this.contrasena);
        return null;
    }

    public int filas() {

        for (int i = 1; i < this.listadoClientes.size(); i++) {
            this.contador = this.contador + i;
            System.out.println(this.contador);
            return this.contador;
        }
        return 0;
    }

    public String habilitarModal(String cedula) throws Exception {
        this.cedulaModal = cedula;
        System.out.println("Soy la cedula para el modal " + cedula);
        /*cedulaModal = "";
        for(Cliente cli: listadoClientes){
            if(cli.getCedula() == cedula) {
                cedulaModal = cli.getCedula();
                this.modal = cedulaModal;
                System.out.println("Soy la cedula para el modal " + this.modal );
                //this.modal = "";
            }
        }
        cedulaModal = "";
        //this.modal = "";*/
        return "listadoClientes";
    }

    public String refrescar() {
        return "listadoClientes";
    }

}
