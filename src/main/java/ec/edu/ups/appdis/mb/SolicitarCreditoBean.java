/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.mb;

import ec.edu.ups.appdis.modelo.Amortizacion;
import ec.edu.ups.appdis.modelo.Cliente;
import ec.edu.ups.appdis.modelo.Credito;
import ec.edu.ups.appdis.modelo.CuentaAhorro;
import ec.edu.ups.appdis.modelo.DescripcionCredito;
import ec.edu.ups.appdis.modelo.Mensaje;
import ec.edu.ups.appdis.modelo.SolicitudCredito;
import ec.edu.ups.appdis.on.GestionAmortizacion;
import ec.edu.ups.appdis.on.GestionClientes;
import ec.edu.ups.appdis.on.GestionCorreo;
import ec.edu.ups.appdis.on.GestionCreditos;
import ec.edu.ups.appdis.on.GestionPaginaClienteLocal;
import ec.edu.ups.appdis.on.GestionSolicitudCredito;
import ec.edu.ups.appdis.on.GestionTransaccionalLocal;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.mail.MessagingException;
import org.dom4j.DocumentException;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.file.UploadedFile;

/**
 *
 * @author ismael
 */
@ManagedBean
@ViewScoped
public class SolicitarCreditoBean {

    @Inject
    private GestionSolicitudCredito gestionSolicitudCredito;
    @Inject
    private GestionClientes gestionCliente;
    @Inject
    private ManagerAccount managerAccount;
    @Inject
    private GestionPaginaClienteLocal gestionClientes;
    @Inject
    private GestionCreditos gestionCreditos;
    @Inject
    private GestionAmortizacion gestionAmortizacion;
    @Inject
    private GestionTransaccionalLocal gtl;
    
      @Inject
    private GestionCorreo gestionCorreo;
    
    
    
    private SolicitudCredito newSolicitudCredito;

    private int contador = 0;
    private int idDetalle;
    private Double totalIngresos;
    private Double totalEgresos;

    //ATRIBUTOS DE SOLO LECTURA - DETALLES DE CREDITO PARA MOSTRAR
    private String activos;
    private String empleo;
    private String estadoCivilSexo;
    private String garante;
    private String historialCredito;
    private String propositoCredito;
    private String saldoCuentaAhorro;
    private String tiempoEmpleo;
    private String tipoCliente;
    private String vivienda;

    //Datos para la solicitud
    private Cliente cliente;
    private Integer plazosMesesCredito;
    //private String historialCredito;
    // private String propositoCredito;
    private Double montoCredito;
    // private String saldoCuentaAhorro;
    // private String tiempoEmpleo;
    private double tasaPago;
    //private String estadoCivilSexo;
    // private String garante;
    private Double avaluoVivienda;
    // private String activos;
    private int edad;
    // private String vivienda;
    private int cantidadCreditosExistentes;
    //private String empleo;
    private String trabajadorEstranjero;
    //private String tipoCliente;

    private CuentaAhorro cuentaAhorro;

    //Para poder cargar el archivo pdf
    UploadedFile fileCedula;
    UploadedFile fileRol;
    UploadedFile filePlanilla;

    //Cargar datos de respldos
    private String nombreCedula;
    private String nombreRol;
    private String nombrePlanilla;

    private byte[] archivoCedula;
    private byte[] archivoRol;
    private byte[] archivoPlanilla;

    private StreamedContent file;

    //Variables de almacenamiento
    private double monto;
    private int plazo;
    private String pro;
    private String ca;
    private Cliente perGarante;

    //DATOS PARA EL GARANTE
    private String cedulaIngresada;
    private String cedulapersonaGarante;
    private String nombre;
    private String apellido;
    private String telefono;

    //TABLA AMORTIZACION
    private List<Amortizacion> listaAmortizacions;

    //MOSTRAR SOLICITUDES DEL USUARIO
    private List<SolicitudCredito> listaSolicitudCreditosUsuarios;

    //MOSTRAR CREDITOS POR APROBAR 
    private List<Credito> listadoCreditosAprobar;

    //MOSTRAR TODAS LA SOLICITUDES AL ADMIN
    private List<SolicitudCredito> listaSolicitudCreditosAdmin;

    //MOSTRAS LA LISTA DE LOS CREDITOS QUE HA APROBADO EL USUARIO
    private List<Credito> listaCreditosAprobadosUsuario;

    @PostConstruct
    public void init() {
        try {
            newSolicitudCredito = new SolicitudCredito();
            listaAmortizacions = new ArrayList<>();
            cuentaAhorro = gestionClientes.searchSavingsAccountCedula(managerAccount.getCedula());
            //Cargar todas la solicitudes de credito, para visualizar
            loadDataSolicitudCredito();
        } catch (Exception e) {
            System.out.println("ERROR AL CARGAR LOS DATOS INICIALES");
        }

    }

    public void loadDataSolicitudCredito() {

        listaSolicitudCreditosUsuarios = new ArrayList<>();
        listadoCreditosAprobar = new ArrayList<>();
        listaSolicitudCreditosAdmin = new ArrayList<>();
        listaCreditosAprobadosUsuario = new ArrayList<>();

        try {
            //SOLICITUDES QUE EL ADMIN DEBE PROCESAR O APROBAR
            listaSolicitudCreditosAdmin = gestionSolicitudCredito.listarSolicitudesNoProcesadas();
            ///DAR EL DINERO
            listaCreditosAprobadosUsuario = gestionCreditos.listarCreditosAprobadosUsuario();

        } catch (Exception e) {
            System.out.println("ERROR AL LLISTAR DATOS DE ADMIN");
        }

        try {
            //SOLICITUDES DE CREDITO DE UN USUARIO
            listaSolicitudCreditosUsuarios = gestionSolicitudCredito.listarSolicitudesCreditoUsuario(managerAccount.getCedula());
            //SOLICTUDES DE CREDITO APROBADAS
            listadoCreditosAprobar = gestionCreditos.listarCreditosAprobados(managerAccount.getCuentaAhorro().getNumeroCuenta());
        } catch (Exception e) {
            System.out.println("ERROR AL PRCESAR LOS DATOS DEL CLIENTE");
        }

    }

    public List<Credito> getListaCreditosAprobadosUsuario() {
        return listaCreditosAprobadosUsuario;
    }

    public void setListaCreditosAprobadosUsuario(List<Credito> listaCreditosAprobadosUsuario) {
        this.listaCreditosAprobadosUsuario = listaCreditosAprobadosUsuario;
    }

    public List<Credito> getListadoCreditosAprobar() {
        return listadoCreditosAprobar;
    }

    public void setListadoCreditosAprobar(List<Credito> listadoCreditosAprobar) {
        this.listadoCreditosAprobar = listadoCreditosAprobar;
    }

    public List<SolicitudCredito> getListaSolicitudCreditosAdmin() {
        return listaSolicitudCreditosAdmin;
    }

    public void setListaSolicitudCreditosAdmin(List<SolicitudCredito> listaSolicitudCreditosAdmin) {
        this.listaSolicitudCreditosAdmin = listaSolicitudCreditosAdmin;
    }

    public void setIdDetalle(int idDetalle) {

        try {
            if (idDetalle != 0) {
                newSolicitudCredito = gestionSolicitudCredito.buscarSolicitudCredito(idDetalle);
                System.out.println(newSolicitudCredito.toString());
                DescripcionCredito dc = gestionSolicitudCredito.buscarDescripcion(newSolicitudCredito.getActivos());
                this.activos = dc.getDescripcion();
                DescripcionCredito dc2 = gestionSolicitudCredito.buscarDescripcion(newSolicitudCredito.getEmpleo());
                this.empleo = dc2.getDescripcion();
                DescripcionCredito dc3 = gestionSolicitudCredito.buscarDescripcion(newSolicitudCredito.getEstadoCivilSexo());
                this.estadoCivilSexo = dc3.getDescripcion();
                DescripcionCredito dc4 = gestionSolicitudCredito.buscarDescripcion(newSolicitudCredito.getGarante());
                this.garante = dc4.getDescripcion();
                DescripcionCredito dc5 = gestionSolicitudCredito.buscarDescripcion(newSolicitudCredito.getHistorialCredito());
                this.historialCredito = dc5.getDescripcion();
                DescripcionCredito dc6 = gestionSolicitudCredito.buscarDescripcion(newSolicitudCredito.getPropositoCredito());
                this.propositoCredito = dc6.getDescripcion();
                DescripcionCredito dc7 = gestionSolicitudCredito.buscarDescripcion(newSolicitudCredito.getSaldoCuentaAhorro());
                this.saldoCuentaAhorro = dc7.getDescripcion();
                DescripcionCredito dc8 = gestionSolicitudCredito.buscarDescripcion(newSolicitudCredito.getTiempoEmpleo());
                this.tiempoEmpleo = dc8.getDescripcion();
                DescripcionCredito dc9 = gestionSolicitudCredito.buscarDescripcion(newSolicitudCredito.getTipoCliente());
                this.tipoCliente = dc9.getDescripcion();
                DescripcionCredito dc10 = gestionSolicitudCredito.buscarDescripcion(newSolicitudCredito.getVivienda());
                this.vivienda = dc10.getDescripcion();
                for (SolicitudCredito sc : listaSolicitudCreditosAdmin) {
                    if (sc.getId() == idDetalle) {
                        monto = sc.getMontoCredito();
                        plazo = sc.getPlazosMesesCredito();
                        DescripcionCredito d = gestionSolicitudCredito.buscarDescripcion(sc.getPropositoCredito());
                        pro = String.valueOf(d.getDescripcion());
                        ca = sc.getCliente().getCedula();
                        //BUSCAR EL GARANTE
                        perGarante = gestionCliente.buscarCliente(sc.getCedulapersonaGarante());
                        loadDataTablaAmortizacion();
                    }

                }
                for (SolicitudCredito sc : listaSolicitudCreditosUsuarios) {
                    if (sc.getId() == idDetalle) {
                        perGarante = gestionCliente.buscarCliente(sc.getCedulapersonaGarante());
                    }

                }

            }
        } catch (Exception e) {
            System.out.println("ERROR AL RECUPERAR LOS DATOS DEL DETALLE ID");
        }

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCedulaIngresada() {
        return cedulaIngresada;
    }

    public void setCedulaIngresada(String cedulaIngresada) {
        this.cedulaIngresada = cedulaIngresada;
    }

    public Cliente getPerGarante() {
        return perGarante;
    }

    public void setPerGarante(Cliente perGarante) {
        this.perGarante = perGarante;
    }

    public String getCedulapersonaGarante() {
        return cedulapersonaGarante;
    }

    public void setCedulapersonaGarante(String cedulapersonaGarante) {
        this.cedulapersonaGarante = cedulapersonaGarante;

    }

    public GestionPaginaClienteLocal getGestionClientes() {
        return gestionClientes;
    }

    public void setGestionClientes(GestionPaginaClienteLocal gestionClientes) {
        this.gestionClientes = gestionClientes;
    }

    public GestionCreditos getGestionCreditos() {
        return gestionCreditos;
    }

    public void setGestionCreditos(GestionCreditos gestionCreditos) {
        this.gestionCreditos = gestionCreditos;
    }

    public static byte[] getCedula() {
        return cedula;
    }

    public GestionSolicitudCredito getGestionSolicitudCredito() {
        return gestionSolicitudCredito;
    }

    public void setGestionSolicitudCredito(GestionSolicitudCredito gestionSolicitudCredito) {
        this.gestionSolicitudCredito = gestionSolicitudCredito;
    }

    public SolicitudCredito getNewSolicitudCredito() {
        return newSolicitudCredito;
    }

    public void setNewSolicitudCredito(SolicitudCredito newSolicitudCredito) {
        this.newSolicitudCredito = newSolicitudCredito;
    }

    public List<SolicitudCredito> getListaSolicitudCreditosUsuarios() {
        return listaSolicitudCreditosUsuarios;
    }

    public void setListaSolicitudCreditosUsuarios(List<SolicitudCredito> listaSolicitudCreditosUsuarios) {
        this.listaSolicitudCreditosUsuarios = listaSolicitudCreditosUsuarios;
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }

    public int getIdDetalle() {
        return idDetalle;
    }

    //GES Y SETS DE ATRIBUTOS SOLO LECTURA
    public String getActivos() {
        return activos;
    }

    public void setActivos(String activos) {
        this.activos = activos;
    }

    public String getEmpleo() {
        return empleo;
    }

    public void setEmpleo(String empleo) {
        this.empleo = empleo;
    }

    public String getEstadoCivilSexo() {
        return estadoCivilSexo;
    }

    public void setEstadoCivilSexo(String estadoCivilSexo) {
        this.estadoCivilSexo = estadoCivilSexo;
    }

    public String getGarante() {
        return garante;
    }

    public void setGarante(String garante) {
        this.garante = garante;
    }

    public String getHistorialCredito() {
        return historialCredito;
    }

    public void setHistorialCredito(String historialCredito) {
        this.historialCredito = historialCredito;
    }

    public String getPropositoCredito() {
        return propositoCredito;
    }

    public void setPropositoCredito(String propositoCredito) {
        this.propositoCredito = propositoCredito;
    }

    public String getSaldoCuentaAhorro() {
        return saldoCuentaAhorro;
    }

    public void setSaldoCuentaAhorro(String saldoCuentaAhorro) {
        this.saldoCuentaAhorro = saldoCuentaAhorro;
    }

    public String getTiempoEmpleo() {
        return tiempoEmpleo;
    }

    public void setTiempoEmpleo(String tiempoEmpleo) {
        this.tiempoEmpleo = tiempoEmpleo;
    }

    public String getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public String getVivienda() {
        return vivienda;
    }

    public void setVivienda(String vivienda) {
        this.vivienda = vivienda;
    }

    public GestionClientes getGestionCliente() {
        return gestionCliente;
    }

    public void setGestionCliente(GestionClientes gestionCliente) {
        this.gestionCliente = gestionCliente;
    }

    public Double getTotalIngresos() {
        return totalIngresos;
    }

    public void setTotalIngresos(Double totalIngresos) {
        this.totalIngresos = totalIngresos;
    }

    public Double getTotalEgresos() {
        return totalEgresos;
    }

    public void setTotalEgresos(Double totalEgresos) {
        this.totalEgresos = totalEgresos;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Integer getPlazosMesesCredito() {
        return plazosMesesCredito;
    }

    public void setPlazosMesesCredito(Integer plazosMesesCredito) {
        this.plazosMesesCredito = plazosMesesCredito;
    }

    public Double getMontoCredito() {
        return montoCredito;
    }

    public void setMontoCredito(Double montoCredito) {
        this.montoCredito = montoCredito;
    }

    public double getTasaPago() {
        return tasaPago;
    }

    public void setTasaPago(double tasaPago) {
        this.tasaPago = tasaPago;
    }

    public Double getAvaluoVivienda() {
        return avaluoVivienda;
    }

    public void setAvaluoVivienda(Double avaluoVivienda) {
        this.avaluoVivienda = avaluoVivienda;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getCantidadCreditosExistentes() {
        return cantidadCreditosExistentes;
    }

    public void setCantidadCreditosExistentes(int cantidadCreditosExistentes) {
        this.cantidadCreditosExistentes = cantidadCreditosExistentes;
    }

    public String getTrabajadorEstranjero() {
        return trabajadorEstranjero;
    }

    public void setTrabajadorEstranjero(String trabajadorEstranjero) {
        this.trabajadorEstranjero = trabajadorEstranjero;
    }

    public CuentaAhorro getCuentaAhorro() {
        return cuentaAhorro;
    }

    public void setCuentaAhorro(CuentaAhorro cuentaAhorro) {
        this.cuentaAhorro = cuentaAhorro;
    }

    public UploadedFile getFileCedula() {
        return fileCedula;
    }

    public void setFileCedula(UploadedFile fileCedula) {
        this.fileCedula = fileCedula;
    }

    public UploadedFile getFileRol() {
        return fileRol;
    }

    public void setFileRol(UploadedFile fileRol) {
        this.fileRol = fileRol;
    }

    public UploadedFile getFilePlanilla() {
        return filePlanilla;
    }

    public void setFilePlanilla(UploadedFile filePlanilla) {
        this.filePlanilla = filePlanilla;
    }

    public String getNombreCedula() {
        return nombreCedula;
    }

    public void setNombreCedula(String nombreCedula) {
        this.nombreCedula = nombreCedula;
    }

    public String getNombreRol() {
        return nombreRol;
    }

    public void setNombreRol(String nombreRol) {
        this.nombreRol = nombreRol;
    }

    public String getNombrePlanilla() {
        return nombrePlanilla;
    }

    public void setNombrePlanilla(String nombrePlanilla) {
        this.nombrePlanilla = nombrePlanilla;
    }

    public byte[] getArchivoCedula() {
        return archivoCedula;
    }

    public void setArchivoCedula(byte[] archivoCedula) {
        this.archivoCedula = archivoCedula;
    }

    public byte[] getArchivoRol() {
        return archivoRol;
    }

    public void setArchivoRol(byte[] archivoRol) {
        this.archivoRol = archivoRol;
    }

    public byte[] getArchivoPlanilla() {
        return archivoPlanilla;
    }

    public void setArchivoPlanilla(byte[] archivoPlanilla) {
        this.archivoPlanilla = archivoPlanilla;
    }

    public StreamedContent getFile() {
        return file;
    }

    public void setFile(StreamedContent file) {
        this.file = file;
    }

    public GestionAmortizacion getGestionAmortizacion() {
        return gestionAmortizacion;
    }

    public void setGestionAmortizacion(GestionAmortizacion gestionAmortizacion) {
        this.gestionAmortizacion = gestionAmortizacion;
    }

    public List<Amortizacion> getListaAmortizacions() {
        return listaAmortizacions;
    }

    public void setListaAmortizacions(List<Amortizacion> listaAmortizacions) {
        this.listaAmortizacions = listaAmortizacions;
    }

    public int filas() {
        for (int i = 1; i < this.listaSolicitudCreditosUsuarios.size(); i++) {
            this.contador = this.contador + i;
            System.out.println(this.contador);
            return this.contador;
        }
        return 0;
    }

    public String detallesCreditoAdmin(int idDetalle) {
        return "detalleCredito?faces-redirect=true&idDetalle=" + idDetalle;

    }

    public String detallesCreditoCliente(int idDetalle) {
        
        return "solicitud_credito_detalle?faces-redirect=true&idDetalle=" + idDetalle;
    }

    public void solicitarCredito() {
        String redireccion = null;
        try {

            //1.CEDULA
            cliente = gestionCliente.buscarCliente(managerAccount.getCedula());
            newSolicitudCredito.setCliente(cliente);
            System.out.println("P1 " + cliente.getCedula());
            //2.PLAZOS
            System.out.println("P2 " + this.plazosMesesCredito);
            newSolicitudCredito.setPlazosMesesCredito(plazosMesesCredito);

            //3.HISTORIA DE CREDITO
            historialCredito = "A30";
            newSolicitudCredito.setHistorialCredito(historialCredito);
            //4.PROPOSITO DE CREDITO
            System.out.println("P4 " + this.propositoCredito);
            newSolicitudCredito.setPropositoCredito(propositoCredito);

            //5.MONTO CREDITO
            System.out.println("P5 " + this.montoCredito);
            newSolicitudCredito.setMontoCredito(montoCredito);

            //6.SALDO DE CUENTA DE AHORROS
            //cuentaAhorro.getSaldo();
            saldoCuentaAhorro = gestionSolicitudCredito.codigoSaldoCuenta(cuentaAhorro.getSaldo());
            System.out.println("P6 " + saldoCuentaAhorro);
            newSolicitudCredito.setSaldoCuentaAhorro(saldoCuentaAhorro);

            //7.TIEMPO DE EMPLEO
            System.out.println("P7 " + this.tiempoEmpleo);
            newSolicitudCredito.setTiempoEmpleo(tiempoEmpleo);
            //8.TASA DE PAGO
            tasaPago = (int) ((totalIngresos - totalEgresos) / 100);
            newSolicitudCredito.setTasaPago(tasaPago);

            //9.ESTADO CIVIL SEXO
            System.out.println("P9 " + this.estadoCivilSexo);
            newSolicitudCredito.setEstadoCivilSexo(estadoCivilSexo);

            //10.GARANTE
            //AQUIE ME FATAL DE HACER UN METODO QUE VERIFIQUE SI LA PESONA ES GARANTE EN OTROS CREDITOS
            garante = "A101";
            newSolicitudCredito.setGarante(garante);

            //11.AVALUO VIVIENDA
            System.out.println("P11 " + this.avaluoVivienda);
            newSolicitudCredito.setAvaluoVivienda(avaluoVivienda);

            //12.ACTIVOS
            System.out.println("P12 " + this.activos);
            newSolicitudCredito.setActivos(activos);

            //13.EDAD
            edad = gestionSolicitudCredito.calcularEdad(cliente.getFechaNacimiento());
            System.out.println("P13 " + edad);
            newSolicitudCredito.setEdad(edad);

            //14.VIVIEDA
            System.out.println("P14 " + this.vivienda);
            newSolicitudCredito.setVivienda(vivienda);

            //15.CANTIDA DE CREDITOS EXITENTES
            cantidadCreditosExistentes = gestionCreditos.numeroCreditosUsuario(cuentaAhorro.getNumeroCuenta());
            System.out.println("P15 " + cantidadCreditosExistentes);
            newSolicitudCredito.setCantidadCreditosExistentes(cantidadCreditosExistentes);

            //16.EMPLEO
            System.out.println("P16 " + this.empleo);
            newSolicitudCredito.setEmpleo(empleo);

            //17.TRABAJADOR EXTRANJERO
            System.out.println("P17 " + this.trabajadorEstranjero);
            newSolicitudCredito.setTrabajadorEstranjero(trabajadorEstranjero);

            //18.TIPO DE CLIENTE
            tipoCliente = "3";
            System.out.println("p18 " + tipoCliente);
            newSolicitudCredito.setTipoCliente(tipoCliente);

            //PARA QUE EL NOMBRE NO SE REPITA , DEBO SABER LA CANTIDAD DE SOLICITUDES
            int numSoli = gestionSolicitudCredito.numeroSolicitudesCreditosUsuario(cliente.getCedula());

            //19.CEDULA
            nombreCedula = "cedula" + cliente.getCedula() + numSoli;
            newSolicitudCredito.setNombreCedula(nombreCedula);
            newSolicitudCredito.setArchivoCedula(cedula);

            //20.ROL
            nombreRol = "rol" + cliente.getCedula() + numSoli;
            newSolicitudCredito.setNombreRol(nombreRol);
            newSolicitudCredito.setArchivoRol(rol);

            //21.PLANILLA
            nombrePlanilla = "planilla" + cliente.getCedula() + numSoli;
            newSolicitudCredito.setNombrePlanilla(nombrePlanilla);
            newSolicitudCredito.setArchivoPlanilla(planilla);

            //22. ESTADO DE LA SOLICITUD
            newSolicitudCredito.setEstadoSolicitud("PENDIENTE");

            //23. ESTABLECER LA PERSONA QUE VA A ACTUAR COMO GARANTE
            newSolicitudCredito.setCedulapersonaGarante(cedulapersonaGarante);

            //Insertar        
            Mensaje m = gestionSolicitudCredito.guardarSolicitudCredito(newSolicitudCredito);
            mesajeSolicitudes(m.getCodigo(), m.getMensaje());

        } catch (Exception ex) {
            mesajeSolicitudes(0, "NO SE PUDO ESTABLECER LA CONEXION CON EL SERVIDOR");
        }
    }

    public void mesajeSolicitudes(int codigo, String mensaje) {

        if (codigo == 1) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "SOLICITUD CRÉDITO ", mensaje));
        }
        if (codigo == 0) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "SOLICITUDE CRÉDITO", mensaje));
        }
    }

    static byte[] cedula;

    static byte[] rol;

    static byte[] planilla;

    public void subirCedula(FileUploadEvent event) {
        try {
            FacesMessage msg = new FacesMessage("RESPALDO CÉDULA", event.getFile().getFileName() + " CARGADA CON ÉXITO");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            fileCedula = event.getFile();
            cedula = fileCedula.getContent();
        } catch (Exception e) {
            throw e;
        }

    }

    public void subirRol(FileUploadEvent event) {
        try {
            FacesMessage msg = new FacesMessage("RESPALDO ROL DE PAGOS", event.getFile().getFileName() + " CARGADA CON ÉXITO");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            fileRol = event.getFile();
            rol = fileRol.getContent();
        } catch (Exception e) {
            throw e;
        }

    }

    public void subirPlanilla(FileUploadEvent event) {
        try {
            FacesMessage msg = new FacesMessage("RESPALDO PLANILLA DE SERVICIO BÁSICO", event.getFile().getFileName() + " CARGADA CON ÉXITO");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            filePlanilla = event.getFile();
            planilla = filePlanilla.getContent();
        } catch (Exception e) {
            throw e;
        }

    }

    public void dowloadCedula(String nombre) {
        try {

            SolicitudCredito credito = gestionSolicitudCredito.descargarCedula(nombre);
            byte[] bs = credito.getArchivoCedula();
            bs.getClass().getResourceAsStream("archivoCedula");
            InputStream inputStream = new ByteArrayInputStream(bs);
            file = new DefaultStreamedContent(inputStream, "cedula/pdf", nombre + ".pdf");
            FacesMessage message = new FacesMessage("descarga Exitosa");
            FacesContext.getCurrentInstance().addMessage(null, message);
        } catch (Exception es) {
            FacesMessage message = new FacesMessage("descarga fallida");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public void dowloadPlanilla(String nombre) {
        try {

            SolicitudCredito credito = gestionSolicitudCredito.descargarPlanilla(nombre);
            byte[] bs = credito.getArchivoPlanilla();
            bs.getClass().getResourceAsStream("archivoPlanilla");
            InputStream inputStream = new ByteArrayInputStream(bs);
            file = new DefaultStreamedContent(inputStream, "planilla/pdf", nombre + ".pdf");
            FacesMessage message = new FacesMessage("descarga Exitosa");
            FacesContext.getCurrentInstance().addMessage(null, message);
        } catch (Exception es) {
            FacesMessage message = new FacesMessage("descarga fallida");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public void dowloadRol(String nombre) {
        try {

            SolicitudCredito credito = gestionSolicitudCredito.descargarRol(nombre);
            byte[] bs = credito.getArchivoRol();
            bs.getClass().getResourceAsStream("archivoRol");
            InputStream inputStream = new ByteArrayInputStream(bs);
            file = new DefaultStreamedContent(inputStream, "rol/pdf", nombre + ".pdf");
            FacesMessage message = new FacesMessage("descarga Exitosa");
            FacesContext.getCurrentInstance().addMessage(null, message);
        } catch (Exception es) {
            FacesMessage message = new FacesMessage("descarga fallida");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public void loadDataTablaAmortizacion() {
        try {
            listaAmortizacions = gestionAmortizacion.generarTablaAmortizacion(monto, plazo, pro);
        } catch (Exception e) {
            throw e;
        }

    }

    public void buscarCliente() throws Exception {

        try {
            Cliente cliente = gestionCliente.buscarCliente(this.cedulaIngresada);
            this.cedulapersonaGarante = cliente.getCedula();
            this.nombre = cliente.getNombre();
            this.apellido = cliente.getApellido();
            this.telefono = cliente.getTelefono();
        } catch (Exception e) {
            throw new Exception("ERROR AL BUSCAR EL CLIENTE");
        }

    }

    public String crearCredito(int numSolicitud) {

        String redireccion = null;
        try {
            
            gestionCreditos.crearCredito(listaAmortizacions, pro, ca, monto);
         
            for (SolicitudCredito soli : listaSolicitudCreditosAdmin) {
                if (soli.getId() == numSolicitud) {
                    soli.setEstadoSolicitud("PROCESADA");
                    gestionSolicitudCredito.actulizarSolicitudCredito(soli);
                    
                    
                    redireccion = "listadoSolicitudesAdmin?faces-redirect=true";
                    //AQUI SE DEBE ENVIAR EL CORREO CON LA TABLA DE AMORTIZACION]
                    
                }
            }
        } catch (Exception e) {
            System.out.println("ERROR AL CREAR EL CREDITO");

        }

        return redireccion;
    }

    public String rechazarSolicitudCredito(int numSolicitud) {
        //System.out.println("RECHAZANDO SOLICITUD");
        String redireccion = null;
        try {
            for (SolicitudCredito soli : listaSolicitudCreditosAdmin) {
                if (soli.getId() == numSolicitud) {
                    soli.setEstadoSolicitud("RECHAZADA");
                    gestionSolicitudCredito.actulizarSolicitudCredito(soli);
                    gestionCorreo.notificacionCorreo(newSolicitudCredito.getCliente(), "RechazarCredito");
                    redireccion = "listadoSolicitudesAdmin?faces-redirect=true";
                    ///AQUI SE DEBE ENVIAR EL CORREO AL USUARIO DE QUE SU SOLICITUD FUE NEGADA

                }
            }
        } catch (Exception e) {
            System.out.println("ERROR AL RECHAZAR LA SOLICITUD DE CREDITO");

        }

        return redireccion;
    }

    public String confirmarCredito(int numCredito) throws Exception {
        try {
            String redireccion = "";
            for (Credito credito : listadoCreditosAprobar) {
                if (credito.getNumeroCredito() == numCredito) {
                    credito.setEstadoCredito("ACEPTADO");
                    gestionCreditos.actualizarCredito(credito);
                    //AQUI UN PROCEDMIENOT ALMACENADO QUE DE FORMA AUTOMATICA SE LE HAGA EL DESEMBOLOS DE DINERO
                    gtl.saveTransactionDeposit(credito.getCuentaAhorro().getNumeroCuenta(), credito.getValorCredito());
                    redireccion = "resumen_cliente?faces-redirect=true";

                }
            }
            return redireccion;
        } catch (Exception e) {
            throw e;
        }
    }

    public String rechazarCredito(int numCredito) throws Exception {

        try {
            String redireccion = "";
            for (Credito credito : listadoCreditosAprobar) {
                if (credito.getNumeroCredito() == numCredito) {
                    credito.setEstadoCredito("RECHAZADO");
                    gestionCreditos.actualizarCredito(credito);
                    //SE DEBE ELIMINAR LA CABECERA DE LOS CREDITOS          
                    redireccion = "clienteConfirmarCredito?faces-redirect=true";

                }
            }
            return redireccion;
        } catch (Exception e) {
            throw e;
        }

    }
    
    
    
    

}
