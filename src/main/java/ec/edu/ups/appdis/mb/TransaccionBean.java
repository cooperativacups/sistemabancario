/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.mb;

import ec.edu.ups.appdis.modelo.Cliente;
import ec.edu.ups.appdis.modelo.CuentaAhorro;
import ec.edu.ups.appdis.modelo.Mensaje;
import ec.edu.ups.appdis.modelo.Transaccion;
import ec.edu.ups.appdis.on.GestionPaginaClienteLocal;
import ec.edu.ups.appdis.on.GestionTransaccionalLocal;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.swing.JOptionPane;

/**
 * Clase TransaccionBean de tipo Managen bean para que sea administrativo con el
 * nombre especificado en el atributo de nombre.
 *
 * @author Gustavo Guallpa
 * @version 08/06/2020
 */
@ManagedBean
@ViewScoped
public class TransaccionBean implements Serializable {

    @Inject
    private GestionPaginaClienteLocal gpcl;

    @Inject
    private GestionTransaccionalLocal gtl;

    private String cedulaIngresada;
    private Double cantDinero;
    private Cliente cliente;

    private CuentaAhorro cuentaAhorro;
    private String cedula;
    private String nombre;
    private String apellido;
    private String numeroCuenta;
    private Double saldo;

    private String mensaje;

    public Double getCantDinero() {
        return cantDinero;
    }

    public void setCantDinero(Double cantDinero) {
        this.cantDinero = cantDinero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public String getCedulaIngresada() {
        return cedulaIngresada;
    }

    public void setCedulaIngresada(String cedulaIngresada) throws Exception {
        this.cedulaIngresada = cedulaIngresada;
    }

    public CuentaAhorro getCuentaAhorro() {
        return cuentaAhorro;
    }

    public void setCuentaAhorro(CuentaAhorro cuentaAhorro) {
        this.cuentaAhorro = cuentaAhorro;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /*
    Meto que permite buscar el clente en base a su cedula ingresada
     */
    public String buscarCliente() throws Exception {
        //Buscar por numerodeCuenta
        try {
            this.cuentaAhorro = gpcl.searchSavingsAccount(this.cedulaIngresada);
            this.cliente = gpcl.buscaCliente(this.cuentaAhorro.getCliente().getCedula());
            this.cedula = this.cliente.getCedula();
            this.nombre = this.cliente.getNombre();
            this.apellido = this.cliente.getApellido();
            this.numeroCuenta = this.cuentaAhorro.getNumeroCuenta();
            this.saldo = this.cuentaAhorro.getSaldo();
            return null;
        } catch (Exception e) {
            throw e;
        }

    }


    /*
    Metodo  que me permite guardar el retiro  que realliza el cajero.
     */
    //static Mensaje m;
    public String saveTransactionWithdrawal() {
        String redireccion = null;
        try {

            Mensaje m = gtl.saveTransactionWithdrawal(numeroCuenta, cantDinero);
            mesajeRetiros(m.getCodigo(), m.getMensaje());
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "CUPS", e.getMessage()));
        }
        return null;
    }

    /*
    Metodo  que me permite guardar el deposito  que realliza el cajero.
     */
    public String saveTransactionDeposit() {
        //Deposito
        try {
            Mensaje m = gtl.saveTransactionDeposit(numeroCuenta, cantDinero);
            mesajeDepositos(m.getCodigo(), m.getMensaje());
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "CUPS", e.getMessage()));
            //System.out.println(e.getMessage());
        }
        return null;
    }

    public void mesajeRetiros(int codigo, String mensaje) {
        if (codigo == 0) {

        }
        if (codigo == 1) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "RETIROS", mensaje));
        }
        if (codigo == 2) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "RETIROS", mensaje));
        }
        if (codigo == 3) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "RETIROS", mensaje));
        }

    }

    public void mesajeDepositos(int codigo, String mensaje) {
        if (codigo == 0) {
        }
        if (codigo == 1) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "DEPÓSITOS", mensaje));
        }
        if (codigo == 3) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "DEPÓSITOS", mensaje));
        }
    }

}
