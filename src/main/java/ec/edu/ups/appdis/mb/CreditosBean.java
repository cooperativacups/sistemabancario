/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.mb;

import ec.edu.ups.appdis.modelo.Amortizacion;
import ec.edu.ups.appdis.modelo.Credito;
import ec.edu.ups.appdis.modelo.Empleado;
import ec.edu.ups.appdis.on.GestionAmortizacion;
import ec.edu.ups.appdis.on.GestionCreditos;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

/**
 *
 * @author ROG
 */
@ManagedBean
public class CreditosBean {

    @Inject
    private GestionCreditos gestionCreditos;

    @Inject
    private GestionAmortizacion gestionAmortizacion;

    private List<Credito> listadoCreditos;
    private List<Amortizacion> listadoAmortizaciones;
    private String tipoCredito;
    private Integer numeroCredito;
    private int numero;
    private int idDetalle;

    private List<Credito> busquedaCreditos;
    @PostConstruct
    public void init() {
        try {
            listadoCreditos = new ArrayList<>();
            listadoCreditos = gestionCreditos.selectAll();

        } catch (Exception e) {
        }

    }

    public List<Credito> getBusquedaCreditos() {
        return busquedaCreditos;
    }

    public void setBusquedaCreditos(List<Credito> busquedaCreditos) {
        this.busquedaCreditos = busquedaCreditos;
    }
    

    public GestionCreditos getGestionCreditos() {
        return gestionCreditos;
    }

    public void setGestionCreditos(GestionCreditos gestionCreditos) {
        this.gestionCreditos = gestionCreditos;
    }

    public GestionAmortizacion getGestionAmortizacion() {
        return gestionAmortizacion;
    }

    public void setGestionAmortizacion(GestionAmortizacion gestionAmortizacion) {
        this.gestionAmortizacion = gestionAmortizacion;
    }

    public List<Credito> getListadoCreditos() {
        return listadoCreditos;
    }

    public void setListadoCreditos(List<Credito> listadoCreditos) {
        this.listadoCreditos = listadoCreditos;
    }

    public List<Amortizacion> getListadoAmortizaciones() {
        return listadoAmortizaciones;
    }

    public void setListadoAmortizaciones(List<Amortizacion> listadoAmortizaciones) {
        this.listadoAmortizaciones = listadoAmortizaciones;
    }

    public String getTipoCredito() {
        return tipoCredito;
    }

    public void setTipoCredito(String tipoCredito) {
        this.tipoCredito = tipoCredito;
    }

    public Integer getNumeroCredito() {
        return numeroCredito;
    }

    public void setNumeroCredito(Integer numeroCredito) {
        this.numeroCredito = numeroCredito;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String detallesCreditoAdmin(int idDetalle) {

        return "estado_credito_detalle?faces-redirect=true&idDetalle=" + idDetalle;
    }

    public void setIdDetalle(int idDetalleP) {
        System.out.println("SOY NUMERO " + idDetalleP);
        try {
            if (idDetalleP != 0) {
//               for (Credito credito : listadoCreditos) {
////                    numero = credito.getNumeroCredito();
////                    tipoCredito = credito.getTipoCredito();
////                    numeroCredito = credito.getNumeroCredito();
//
//                }
                listadoAmortizaciones = gestionAmortizacion.mostrarAmortizacionCliente(idDetalleP);
                for (Amortizacion amortizacion : listadoAmortizaciones) {
                    numeroCredito = amortizacion.getCredito().getNumeroCredito();
                    //numeroCredito = amortizacion.getnPeriodos();
                    tipoCredito = amortizacion.getCredito().getTipoCredito();
//                    numeroCredito = credito.getNumeroCredito();

                }
            }
        } catch (Exception e) {
            System.out.println("ERROR AL RECUPERAR LOS DATOS DEL DETALLE ID");
        }

    }

    public int getIdDetalle() {
        return idDetalle;
    }

}
