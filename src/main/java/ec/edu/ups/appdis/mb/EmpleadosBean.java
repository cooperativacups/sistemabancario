/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.mb;

import ec.edu.ups.appdis.modelo.Empleado;
import ec.edu.ups.appdis.on.GestionEmpleado;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

/**
 * Clase EmpleadosBean de tipo Managen bean para que sea administrativo con el
 * nombre especificado en el atributo de nombre.
 *
 * @author Ismael Castillo
 * @version 08/06/2020
 */
@ManagedBean
@ViewScoped
public class EmpleadosBean {

    @Inject
    private GestionEmpleado gestionEmpleado;
    private Empleado newEmpleado;
    private List<Empleado> listadoEmpleados;
    private String cedula;
    private String titulo;
    private String nombre;
    private Date fechaNacimiento;
    private String fechaActualizar;
    private String usuario;
    private String contrasena;
    private String tipo;
    private int contador = 0;
    
    private List<Empleado> buscarEmpleados;

    @PostConstruct
    public void init() {
        try {
            newEmpleado = new Empleado();
            loadDataEmpleados();
        } catch (Exception e) {
        }

    }

    public List<Empleado> getBuscarEmpleados() {
        return buscarEmpleados;
    }

    public void setBuscarEmpleados(List<Empleado> buscarEmpleados) {
        this.buscarEmpleados = buscarEmpleados;
    }
    
    

    public GestionEmpleado getGestionEmpleado() {
        return gestionEmpleado;
    }

    public void setGestionEmpleado(GestionEmpleado gestionEmpleado) {
        this.gestionEmpleado = gestionEmpleado;
    }

    public Empleado getNewEmpleado() {
        return newEmpleado;
    }

    public void setNewEmpleado(Empleado newEmpleado) {
        this.newEmpleado = newEmpleado;
    }

    public List<Empleado> getListadoEmpleados() {
        return listadoEmpleados;
    }

    public void setListadoEmpleados(List<Empleado> listadoEmpleados) {
        this.listadoEmpleados = listadoEmpleados;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) throws Exception {
        this.cedula = cedula;
        if (cedula != null) {
            newEmpleado = gestionEmpleado.buscarEmpleado(cedula);
            titulo = "Editar registro del empleado " + cedula;
        }
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;

    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }

    /**
     * Metodo que permite obtener el listado de los de las cuentas de ahorro
     *
     * @throws Exception
     */
    private void loadDataEmpleados() throws Exception {
        listadoEmpleados = gestionEmpleado.listarEmpleados();
    }

    /**
     * E metodo GuardarEmpleado nos permite guardar un empleado en la base de
     * datos una ves que se hayan ingresado los datos necesarios
     *
     * @return
     * @throws Exception
     */
    public String guardarEmpleado() throws Exception {
        System.out.println(newEmpleado.toString());
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        newEmpleado.setFechaNacimiento(sdf.format(fechaNacimiento));
        gestionEmpleado.guardarEmpleado(newEmpleado);
        //gestionUsuarios.guardarUsuario(newUsuario);
        //return "listadoUsuarios";

        return "listadoUsuarios";
    }

    public String actualizarEmpleado() throws Exception {
        System.out.println(newEmpleado.toString());
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        newEmpleado.setFechaNacimiento(sdf.format(fechaNacimiento));
        gestionEmpleado.guardarEmpleado(newEmpleado);
        //gestionUsuarios.guardarUsuario(newUsuario);
        //return "listadoUsuarios";

        return "listadoUsuarios";
    }

    /**
     * El metodo editar nos permite revalidar los datps de un empleado que se
     * encuentre registrado en la base de datos
     *
     * @param cedula
     * @return
     * @throws Exception
     */
    public String editar(String cedula) throws Exception {
        System.out.println(cedula);
        return "actualizarUsuario?faces-redirect=true&cedula=" + cedula;
    }

    /**
     * El metodo eliminar nos permite quitar o eliminar un registro de un
     * empleado en la base de datos
     *
     * @param cedula
     * @return
     * @throws Exception
     */
    public String eliminar(String cedula) throws Exception {
        System.out.println(" Cedula a eliminar " + cedula);
        gestionEmpleado.eliminarEmpleado(cedula);
        return "listadoUsuarios";
    }

    /**
     * El metodo generarUsuario nos permite generar un usuario mediante el in
     * greso del nombre del cliente adjuntado el texto @cups
     *
     * @return
     */
    public String generarUsuario() {
        String user = this.newEmpleado.getNombre() + "@cups";
        this.usuario = user;
        this.newEmpleado.setUsuario(user);
        return null;
    }

    /**
     * El metodo generarContrasena nos permite generar una contrase;a atarves
     * del texto que se le adjunta al campo de tipo input CUps2020@#$
     *
     * @return
     */
    public String generarContrasena() {
        this.contrasena = "CUps2020@#$";
        this.newEmpleado.setContrasena(this.contrasena);
        return null;
    }

    public int filas() {
        for (int i = 1; i < this.listadoEmpleados.size(); i++) {
            this.contador = this.contador + i;
            System.out.println(this.contador);
            return this.contador;
        }
        return 0;
    }

}
