/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.mb;

import ec.edu.ups.appdis.modelo.Cliente;
import ec.edu.ups.appdis.modelo.CuentaAhorro;
import ec.edu.ups.appdis.on.GestionPaginaClienteLocal;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Clase ManagerAccount de tipo Managen bean para que sea administrativo con el
 * nombre especificado en el atributo de nombre.
 *
 * @author Gustavo Guallpa
 * @version 08/06/2020
 */
@Named
@SessionScoped
public class ManagerAccount implements Serializable {

    @Inject
    private GestionPaginaClienteLocal gestionClientes;
    private Cliente cliente;
    private String cedula;
    private CuentaAhorro cuentaAhorro;

    public Cliente getCliente() {
        return cliente;
    }
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
        if (cedula != null) {
            try {
                cliente = gestionClientes.buscaCliente(cedula);
                cuentaAhorro = gestionClientes.searchSavingsAccountCedula(cedula);
            } catch (Exception e) {

            }

        } else {

        }
    }

    @PostConstruct
    public void init() {
        try {
            cliente = new Cliente();
        } catch (Exception e) {
        }

    }

    public CuentaAhorro getCuentaAhorro() {
        return cuentaAhorro;
    }

    public void setCuentaAhorro(CuentaAhorro cuentaAhorro) {
        this.cuentaAhorro = cuentaAhorro;
    }

    
    
}
