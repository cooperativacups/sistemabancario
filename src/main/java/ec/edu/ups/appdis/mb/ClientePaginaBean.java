/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.mb;

import ec.edu.ups.appdis.modelo.Acceso;
import ec.edu.ups.appdis.modelo.Amortizacion;
import ec.edu.ups.appdis.modelo.Cliente;
import ec.edu.ups.appdis.modelo.Credito;
import ec.edu.ups.appdis.modelo.CuentaAhorro;
import ec.edu.ups.appdis.modelo.Transaccion;
import ec.edu.ups.appdis.on.GestionAmortizacion;
import ec.edu.ups.appdis.on.GestionCreditos;
import ec.edu.ups.appdis.on.GestionPaginaCliente;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import ec.edu.ups.appdis.on.GestionPaginaClienteLocal;
import ec.edu.ups.appdis.on.GestionTransaccionalLocal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Clase ClientePagina de tipo Managen bean para que sea administrativo con el
 * nombre especificado en el atributo de nombre.
 *
 * @author Ismael Castillo
 * @version 08/06/2020
 */
@ManagedBean
@SessionScoped
public class ClientePaginaBean implements Serializable {

    @Inject
    private GestionPaginaClienteLocal gestionPaginaCliente;

    @Inject
    private ManagerAccount mac;  //manager account datos de los clientes pata mover  entre paginas

    @Inject
    private GestionTransaccionalLocal gestionTransaccionalLocal;
    
    //CREDITO
    @Inject
    private GestionCreditos gestionCreditos;
    
    @Inject
    private GestionAmortizacion gestionAmortizacion;
    
    
    
    private List<Credito> listadoCreditos;
    private List<Amortizacion> listadoAmortizaciones;
    
    private String tipoCredito;
    private Integer numeroCredito;
    private int numero;
    ///
    
    private String fecha;
    private CuentaAhorro cuentaAhorro;
    private List<Acceso> accesosCorrectos;
    private List<Acceso> accesosIncorrectos;
    private List<Transaccion> listadoTransaccions;
    private String concepto;
    private String fechadesde;
    private String fechahasta;
    private String numeroCuenta;
    private int contador = 0;
    private Date feFinal;
    private Date feInicio;

    @PostConstruct
    public void init() {
        try {
            cuentaAhorro = new CuentaAhorro();
            accesosCorrectos = new ArrayList<>();
            accesosIncorrectos = new ArrayList<>();
            listadoTransaccions = new ArrayList<>();
            listadoCreditos = new ArrayList<>();
            accesosCorrectos = gestionPaginaCliente.getCorrectAccesses(mac.getCedula());
            accesosIncorrectos = gestionPaginaCliente.getIncorrectAccesses(mac.getCedula());
            cuentaAhorro = gestionPaginaCliente.searchSavingsAccountCedula(mac.getCedula());
            numeroCuenta = cuentaAhorro.getNumeroCuenta();
            fecha = gestionPaginaCliente.getLastDateTransacction(mac.getCuentaAhorro().getNumeroCuenta());
            
            //DETALLES DEL CREDITO
           listadoCreditos = gestionCreditos.detalleCreditoCliente(mac.getCuentaAhorro().getNumeroCuenta());
           
           //FOR GUSTAVO
           for (Credito credito : listadoCreditos) {
                numero = credito.getNumeroCredito();
                tipoCredito = credito.getTipoCredito();
                numeroCredito = credito.getNumeroCredito();
                
            }
            listadoAmortizaciones = gestionAmortizacion.mostrarAmortizacionCliente(numero);
            consultarUltimasTransacciones();
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public List<Credito> getListadoCreditos() {
        return listadoCreditos;
    }

    public void setListadoCreditos(List<Credito> listadoCreditos) {
        this.listadoCreditos = listadoCreditos;
    }

    public List<Amortizacion> getListadoAmortizaciones() {
        return listadoAmortizaciones;
    }

    public void setListadoAmortizaciones(List<Amortizacion> listadoAmortizaciones) {
        this.listadoAmortizaciones = listadoAmortizaciones;
    }
    

    public String getTipoCredito() {
        return tipoCredito;
    }

    public void setTipoCredito(String tipoCredito) {
        this.tipoCredito = tipoCredito;
    }

    public Integer getNumeroCredito() {
        return numeroCredito;
    }

    public void setNumeroCredito(Integer numeroCredito) {
        this.numeroCredito = numeroCredito;
    }

    
    public Date getFeFinal() {
        return feFinal;
    }

    public void setFeFinal(Date feFinal) {
        this.feFinal = feFinal;
    }

    public Date getFeInicio() {
        return feInicio;
    }

    public void setFeInicio(Date feInicio) {
        this.feInicio = feInicio;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public ManagerAccount getMac() {
        return mac;
    }

    public void setMac(ManagerAccount mac) {
        this.mac = mac;
    }

    public CuentaAhorro getCuentaAhorro() {
        return cuentaAhorro;
    }

    public void setCuentaAhorro(CuentaAhorro cuentaAhorro) {
        this.cuentaAhorro = cuentaAhorro;
    }

    public List<Acceso> getAccesosCorrectos() {
        return accesosCorrectos;
    }

    public void setAccesosCorrectos(List<Acceso> accesosCorrectos) {
        this.accesosCorrectos = accesosCorrectos;
    }

    public List<Acceso> getAccesosIncorrectos() {
        return accesosIncorrectos;
    }

    public void setAccesosIncorrectos(List<Acceso> accesosIncorrectos) {
        this.accesosIncorrectos = accesosIncorrectos;
    }

    public List<Transaccion> getListadoTransaccions() {
        return listadoTransaccions;
    }

    public void setListadoTransaccions(List<Transaccion> listadoTransaccions) {
        this.listadoTransaccions = listadoTransaccions;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getFechadesde() {
        return fechadesde;
    }

    public void setFechadesde(String fechadesde) {
        this.fechadesde = fechadesde;
    }

    public String getFechahasta() {
        return fechahasta;
    }

    public void setFechahasta(String fechahasta) {
        this.fechahasta = fechahasta;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    /**
     * Metodo que me permite obtener las transacciones entre el rango de fechas
     * que se especifica
     *
     * @return null ya que los datos los necesito dentro de la misma pagina
     * @throws Exception
     */
    public String consultarTransacciones() throws Exception {
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            String fechaFinal = dateFormat.format(feFinal);
            String fechaInicial = dateFormat.format(feInicio);
            listadoTransaccions = gestionTransaccionalLocal.obtenerTransacciones(mac.getCuentaAhorro().getNumeroCuenta(), this.concepto, fechaInicial, fechaFinal);
            return null;
        } catch (Exception e) {
             throw e;
        }
    }

    /**
     * Metodo para consultar las tranasacciones entre retiros y depositos de los
     * ultimos 30 dias.
     *
     * @return null ya que los datos los necesito dentro de la misma pagina
     * @throws Exception
     */
    public String consultarUltimasTransacciones() throws Exception {
        try {
            
            Calendar ca = Calendar.getInstance();
            feFinal = ca.getTime();
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            String fechaFinal = dateFormat.format(feFinal);
            ca.add(Calendar.DATE, -30);
            feInicio = ca.getTime();
            String fechaInicial = dateFormat.format(feInicio);
            System.out.println("FECHA INICIAL " + fechaInicial);
            listadoTransaccions = gestionTransaccionalLocal.obtenerUltimasTransacciones(mac.getCuentaAhorro().getNumeroCuenta(), fechaInicial, fechaFinal);
            
            return null;
        } catch (Exception ex) {
            throw new Exception("EROR AL CONSULTAR LAS TRANSACCIONES DE LOS ULTIMOS 30 DIAS ");
        }
    }
    
    /*
    Metod para agrgar el numero respectivo a cada fila al momento de listar
     */
    public int filas() {
        for (int i = 1; i < this.listadoTransaccions.size(); i++) {
            this.contador = this.contador + i;
            System.out.println(this.contador);
            return this.contador;
        }
        return 0;
    }

    public int filas2() {
        for (int i = 1; i < this.accesosIncorrectos.size(); i++) {
            this.contador = this.contador + i;
            System.out.println(this.contador);
            return this.contador;
        }
        return 0;
    }

    public int filas3() {
        for (int i = 1; i < this.accesosCorrectos.size(); i++) {
            this.contador = this.contador + i;
            System.out.println(this.contador);
            return this.contador;
        }
        return 0;
    }

}
