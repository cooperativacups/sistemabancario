/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.mb;


import ec.edu.ups.appdis.modelo.Cliente;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import ec.edu.ups.appdis.on.GestionLoginLocal;
import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 * Clase LoginClienteBean de tipo Managen bean para que sea administrativo con
 * el nombre especificado en el atributo de nombre.
 *
 * @author Gustavo Guallpa
 * @version 08/06/2020
 */
@ManagedBean
@SessionScoped
public class LoginClienteBean {

    @Inject
    private GestionLoginLocal gul;

    private Cliente cliente;

    @PostConstruct
    public void init() {
        try {
            cliente = new Cliente();
        } catch (Exception e) {
        }

    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * * El metodo login permite buscar primeramente un usuario si esta
     * registrado en la base de datos, * nos pemite hacer la comparacion con su
     * contraseña, y la contraseña que ingresa , de esta forma validar * si la
     * contrasañeña efectivamente pertenece a dicho usuario y permitirle el
     * ingreso a la plataforma virtual, caso contrario * denegarle el acceso.
     *
     *
     * @return redireccion con el valor null que permite que nos quedemos en la
     * misma pagina.
     */
    public String login() {
        String redireccion = null;
        Cliente cli = null;
        try {
            cli = gul.searchCustomer(cliente.getUsuario());
            if (cli.getContrasena().equals(cliente.getContrasena())) {
                gul.insertCorrectAcces(cli);
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("cliente", cli);
                redireccion = "resumen_cliente?faces-redirect=true&cedula=" + cli.getCedula();
                
            } else {
                gul.insertIncorrectAcces(cli);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "AVISO", " USUARIO O CONTRASEÑA INCORRECTA "));
            }

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ALERTA", "CREDENCIALES INVALIDOS "));
        }
        return redireccion;

    }

    public void cerrarSession() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
    }

}
