package ec.edu.ups.appdis.mb;

import ec.edu.ups.appdis.modelo.Amortizacion;
import ec.edu.ups.appdis.modelo.Credito;
import ec.edu.ups.appdis.modelo.Mensaje;
import ec.edu.ups.appdis.on.GestionAmortizacion;
import ec.edu.ups.appdis.on.GestionCreditos;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 * @author Jorge Arévalo
 * @version 27/07/2020
 */
@ManagedBean
@ViewScoped
public class PagosCreditoBean {

    @Inject
    private ManagerAccount managerAccount;

    @Inject
    private GestionCreditos gestionCreditos;

    @Inject
    private GestionAmortizacion gestionAmortizacion;

    private Credito credito;

    private List<Amortizacion> listadoAmortizaciones;

    private Amortizacion amortizacionPago;

    //Variable necesaria para el pago
    private Double cantDinero;

    private Double abonoDinero;

    @PostConstruct
    public void init() {
        try {
            credito = gestionCreditos.buscarCreditoCliente(managerAccount.getCuentaAhorro().getNumeroCuenta());
            listadoAmortizaciones = gestionAmortizacion.mostrarAmortizacionCliente(credito.getNumeroCredito());

            //BUSCAMOS LA CUOTA A PAGAR
            amortizacionPago = gestionAmortizacion.obtenerUltimaCuoa(credito.getNumeroCredito());

        } catch (Exception e) {

        }
    }

    public void pagarCuota(double monto) {

        try {

            Mensaje mensaje = gestionAmortizacion.pagarCuota(amortizacionPago.getId(), monto, managerAccount.getCuentaAhorro().getNumeroCuenta());
            mesajePagoCuotas(mensaje.getCodigo(), mensaje.getMensaje());

        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "CUPS", "SE HA PRODUCIDO UN ERROR INESPERADO"));
        }
    }

    public void abonarCuota(double monto) {
        try {

            Mensaje mensaje = gestionAmortizacion.abonarCuota(amortizacionPago.getId(), monto, managerAccount.getCuentaAhorro().getNumeroCuenta());
            mesajePagoAbonos(mensaje.getCodigo(), mensaje.getMensaje());

        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "CUPS", "SE HA PRODUCIDO UN ERROR INESPERADO"));
        }
    }

    public Amortizacion getAmortizacionPago() {
        return amortizacionPago;
    }

    public void setAmortizacionPago(Amortizacion amortizacionPago) {
        this.amortizacionPago = amortizacionPago;
    }

    public Double getCantDinero() {
        return cantDinero;
    }

    public void setCantDinero(Double cantDinero) {
        this.cantDinero = cantDinero;
    }

    public Double getAbonoDinero() {
        return abonoDinero;
    }

    public void setAbonoDinero(Double abonoDinero) {
        this.abonoDinero = abonoDinero;
    }

    public void mesajePagoCuotas(int codigo, String mensaje) {
        System.out.println("CODE " + codigo);
        if (codigo == 1) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "PAGOS CUPS", mensaje));
        }
        if (codigo == 6) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "PAGOS CUPS", mensaje));
        }
        if (codigo == 50) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "PAGOS CUPS", mensaje));
        }
        if (codigo == 51) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "PAGOS CUPS", mensaje));
        }
        if (codigo == 52) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "PAGOS CUPS", mensaje));
        } else if (codigo != 1 && codigo != 50 && codigo != 51 && codigo != 52 && codigo != 6) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "CUENTA DE AHORRO", mensaje));
        }

    }

    public void mesajePagoAbonos(int codigo, String mensaje) {
        if (codigo == 1) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "PAGOS CUPS", mensaje));
        }

        if (codigo == 6) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "PAGOS CUPS", mensaje));
        }
        if (codigo == 50) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "PAGOS CUPS", mensaje));
        }
        if (codigo == 51) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "PAGOS CUPS", mensaje));
        } else if (codigo != 1 && codigo != 50 && codigo != 51 && codigo != 6) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "CUENTA DE AHORRO", mensaje));
        }

    }

}
