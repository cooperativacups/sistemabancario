/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.mb;

import ec.edu.ups.appdis.modelo.Cliente;
import ec.edu.ups.appdis.modelo.Empleado;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 * Clase VerificarSessionBean de tipo Managen Bean para verificar si el usuario a iniciado o no sesión.
 *
 * @author Gustavo Guallpa
 * @version 1.0
 * @since 30/07/2020
 */
@ManagedBean
@ViewScoped
public class VerificarSessionBean implements Serializable {
    
    public void verificarSession(){
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            Cliente cliente = (Cliente) context.getExternalContext().getSessionMap().get("cliente");
            
            if (cliente == null) {
                context.getExternalContext().redirect("access.xhtml");
            }
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
    }
    public void verificarSessionAdmin(){
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            Empleado  empleado= (Empleado) context.getExternalContext().getSessionMap().get("empleado");
            
            if (empleado == null) {
                context.getExternalContext().redirect("accessAdmin.xhtml");
            }
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
    }
}