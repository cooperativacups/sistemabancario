/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.mb;

import ec.edu.ups.appdis.modelo.Cliente;
import ec.edu.ups.appdis.modelo.CuentaAhorro;
import ec.edu.ups.appdis.modelo.Empleado;
import ec.edu.ups.appdis.on.GestionLoginLocal;
import ec.edu.ups.appdis.on.GestionPaginaClienteLocal;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author ggual
 */
@Named
@SessionScoped
public class ManagerAccountAdmin implements Serializable {

    @Inject
    private GestionLoginLocal gestionCliente;
    private Empleado empleado;
    private String cedula;
  //  private CuentaAhorro cuentaAhorro;

//    public Cliente getCliente() {
//        return empleado;
//    }
//    public void setCliente(Cliente cliente) {
//        this.empleado = cliente;
//    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
        if (cedula != null) {
            try {
                empleado = gestionCliente.findEmployee(cedula);
                //cuentaAhorro = gestionClientes.searchSavingsAccount(cedula);
            } catch (Exception e) {

            }

        } else {

        }
    }

    
    @PostConstruct
    public void init() {
        try {
            empleado = new Empleado();
        } catch (Exception e) {
        }

    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    

    
    
}
