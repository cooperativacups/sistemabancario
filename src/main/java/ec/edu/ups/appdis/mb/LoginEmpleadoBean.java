/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.mb;

import ec.edu.ups.appdis.modelo.Acceso;
import ec.edu.ups.appdis.modelo.Cliente;
import ec.edu.ups.appdis.modelo.Empleado;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import ec.edu.ups.appdis.on.GestionLoginLocal;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * Clase LoginEmpleadoBean de tipo Managen bean para que sea administrativo con
 * el nombre especificado en el atributo de nombre.
 *
 * @author Gustavo Guallpa
 * @version 08/06/2020
 */
@ManagedBean
@ViewScoped
public class LoginEmpleadoBean {

    @Inject
    private GestionLoginLocal gul;

    private Empleado empleado;

    @PostConstruct
    public void init() {
        //System.out.println("HOLA");
        try {
            empleado = new Empleado();
        } catch (Exception e) {
        }

    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    /**
     * * El metodo login permite buscar primeramente un empleado si esta
     * registrado en la base de datos, * nos pemite hacer la comparacion con su
     * contraseña, y la contraseña que ingresa , de esta forma validar * si la
     * contrasañeña efectivamente pertenece a dicho empleado y permitirle el
     * ingreso a la plataforma virtual, caso contrario * denegarle el acceso.
     *
     *
     * @return redireccion con el valor null que permite que nos quedemos en la
     * misma pagina.
     */
    public String login() {
        String redireccion = null;
        Empleado empl = null;
        try {
            empl = gul.searchEmployee(empleado.getUsuario());
            System.out.println("SU USUARIO ES " + empl.getUsuario());
            if (empl.getContrasena().equals(empleado.getContrasena())) {
                if (empl.getRol().equals("JEFE-DE-CREDITO")) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("empleado", empl);
                    redireccion = "jefe_inicio?faces-redirect=true&cedula=" + empl.getCedula();
                } else {
                    redireccion = "cajero_inicio?faces-redirect=true&cedula=" + empl.getCedula();
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "AVISO", "CONTRASEÑA INCORRECTA "));

            }

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ALERTA", "CREDENCIALES INVALIDOS "));
        }
        return redireccion;

    }

}
