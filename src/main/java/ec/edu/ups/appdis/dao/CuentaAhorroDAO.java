/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.dao;

import ec.edu.ups.appdis.modelo.CuentaAhorro;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * En esta Clase Define los metodos para agregar datos o listar 
 * la base de datos de la tabla CuentaAhorro
 * @author Jorge Arevalo
 * @version 08/06/2020
 */
@Stateless
public class CuentaAhorroDAO {

    @PersistenceContext
    private EntityManager em;

    /**
     * El metodo insert permite persistir un cuentaAhorro en la base de datos
     * @param cuentaAhorro 
     */
    public void insert(CuentaAhorro cuentaAhorro) {
        em.persist(cuentaAhorro);
    }

    /**
     * El metodo update nos permite revalidar los datos de una cuenta de 
     * ahorro ya persistida en la base de datos
     * @param cuentaAhorro 
     */
    public void update(CuentaAhorro cuentaAhorro) {
        em.merge(cuentaAhorro);
    }

    /**
     * El metodo read nos permite buscar o obtener una cuenta de ahorros ya 
     * persistida en la base de datos.
     * @param numeroCuenta
     * @return 
     */
    public CuentaAhorro read(String numeroCuenta) {
        try {
            CuentaAhorro ch = em.find(CuentaAhorro.class, numeroCuenta);
            return ch;
        } catch (Exception e) {
            System.out.println("ERROR DAO ");
        }
        return null;
    }

    /**
     * El metodo delete nos permite eliminar una cuenta de ahorro ya 
     * persistida en la base de datos
     * @param numeroCuenta 
     */
    public void delete(String numeroCuenta) {
        CuentaAhorro ch = em.find(CuentaAhorro.class, numeroCuenta);
        em.remove(ch);
    }

    /**
     * El metodo selectLike nos permite obtener una lista de cuentas de 
     * ahorro en base a un filtro que se le provee.
     * @param filtro
     * @return 
     */
    public List<CuentaAhorro> selectLike(String filtro) {
        String jpql = "SELECT ch FROM CuentaAhorro ch"
                + " WHERE numeroCuenta LIKE :filtro";

        Query q = em.createQuery(jpql, CuentaAhorro.class);
        q.setParameter("filtro", filtro + "%");
        return q.getResultList();
    }

    /**
     * El metodo selectLike nos permite obtener una lista de todas las cuentas
     * de ahorro que se encuentran persistidas en la base de datos
     * @return 
     */
    public List<CuentaAhorro> selectAll() {
        String jpql = "SELECT ch FROM CuentaAhorro ch";

        Query q = em.createQuery(jpql, CuentaAhorro.class);
        return q.getResultList();
    }

    /**
     * El metodo searchSavingsAccount nos permite obtener una cuenta de ahorro
     * persistida en la base de datos en base al parametro cedula que se le provee
     * @param cedula
     * @return 
     */
    public CuentaAhorro searchSavingsAccount(String cedula) {
        ///CuentaAhorro cuentaAhorro;
        System.out.println("BUSCANDO CUENTA DE AHOOROS");
        String consulta;
        try {
            consulta = "SELECT ch FROM CuentaAhorro ch";
            Query query = em.createQuery(consulta);
            List<CuentaAhorro> lista = query.getResultList();
            
            for (CuentaAhorro cuenta : lista) {
                if (cuenta.getCliente().getCedula().equals(cedula)) {
                    System.out.println("CUENTA ENCONTRADA");
                    return cuenta;
                    
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return null;

    }

}
