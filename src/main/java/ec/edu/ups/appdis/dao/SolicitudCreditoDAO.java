package ec.edu.ups.appdis.dao;

import ec.edu.ups.appdis.modelo.SolicitudCredito;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author ismael
 */
@Stateless
public class SolicitudCreditoDAO {

    @PersistenceContext()
    private EntityManager em;

    public boolean insert(SolicitudCredito solicitudCredito) {
        try {
            em.persist(solicitudCredito);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public void update(SolicitudCredito solicitudCredito) {
        try {
            em.merge(solicitudCredito);
        } catch (Exception e) {
            throw e;
        }

    }

    public SolicitudCredito read(int id) {
        try {
            SolicitudCredito sc = em.find(SolicitudCredito.class, id);
            return sc;
        } catch (Exception e) {
            throw e;
        }

    }

    public void delete(int id) {
        try {
            SolicitudCredito sc = em.find(SolicitudCredito.class, id);
            em.remove(sc);
        } catch (Exception e) {
            throw e;
        }

    }

    public List<SolicitudCredito> selectLike(String cedula) {
        try {
            String jpql = "SELECT sc FROM SolicitudCredito sc"
                    + " WHERE sc.Cliente.cedula LIKE :filtro";

            Query q = em.createQuery(jpql, SolicitudCredito.class);
            q.setParameter("filtro", cedula + "%");
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }

    }

    public List<SolicitudCredito> selectLeftJoin() {
        try {
            String jpql = "SELECT sc FROM SolicitudCredito sc"
                    + " WHERE sc.Cliente.cedula LIKE :filtro";

            Query q = em.createQuery(jpql, SolicitudCredito.class);
            // q.setParameter("filtro", cedula + "%");
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }

    }

    public List<SolicitudCredito> selectAll() {
        try {
            String jpql = "SELECT sc FROM SolicitudCredito sc";

            Query q = em.createQuery(jpql, SolicitudCredito.class);
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }

    }

    public List<SolicitudCredito> listarSolicitudesCreditoUsuario(String cedula) {
        try {
            String jpql = "SELECT sc FROM SolicitudCredito sc"
                    + " WHERE sc.cliente.cedula=?1";
            Query q = em.createQuery(jpql, SolicitudCredito.class);
            q.setParameter(1, cedula);
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }

    }

    public List<SolicitudCredito> listarSolicitudesNoProcesadas() {
        try {
            String jpql = "SELECT sc FROM SolicitudCredito sc WHERE sc.estadoSolicitud=" + "'PENDIENTE'";
            Query q = em.createQuery(jpql, SolicitudCredito.class);
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }

    }

    public SolicitudCredito descargarCedula(String nombre) {
        ///CuentaAhorro cuentaAhorro;
        System.out.println("BUSCANDO ARCHIVO PDF");
        String consulta;
        try {
            consulta = "SELECT sc FROM SolicitudCredito sc";
            Query query = em.createQuery(consulta);
            List<SolicitudCredito> lista = query.getResultList();

            for (SolicitudCredito solicitudCredito : lista) {
                //if (solicitudCredito.getCliente().getCedula().equals(nombre)) {
                if (solicitudCredito.getNombreCedula().equals(nombre)) {
                    System.out.println("ARHIVO ENCONTRADO");
                    return solicitudCredito;

                }
            }
        } catch (Exception e) {
            throw e;
        }
        return null;

    }

    public SolicitudCredito descargarPlanilla(String nombre) {
        ///CuentaAhorro cuentaAhorro;
        System.out.println("BUSCANDO ARCHIVO PDF");
        String consulta;
        try {
            consulta = "SELECT sc FROM SolicitudCredito sc";
            Query query = em.createQuery(consulta);
            List<SolicitudCredito> lista = query.getResultList();

            for (SolicitudCredito solicitudCredito : lista) {
                //if (solicitudCredito.getCliente().getCedula().equals(nombre)) {
                if (solicitudCredito.getNombrePlanilla().equals(nombre)) {
                    System.out.println("ARHIVO ENCONTRADO");
                    return solicitudCredito;

                }
            }
        } catch (Exception e) {
            throw e;
        }
        return null;

    }

    public SolicitudCredito descargarRol(String nombre) {
        ///CuentaAhorro cuentaAhorro;
        System.out.println("BUSCANDO ARCHIVO PDF");
        String consulta;
        try {
            consulta = "SELECT sc FROM SolicitudCredito sc";
            Query query = em.createQuery(consulta);
            List<SolicitudCredito> lista = query.getResultList();

            for (SolicitudCredito solicitudCredito : lista) {
                //if (solicitudCredito.getCliente().getCedula().equals(nombre)) {
                if (solicitudCredito.getNombreRol().equals(nombre)) {
                    System.out.println("ARHIVO ENCONTRADO");
                    return solicitudCredito;

                }
            }
        } catch (Exception e) {
            throw e;
        }
        return null;

    }

    public List<SolicitudCredito> numeroSolicitudesCreditosUsuario(String cedula) {
        try {
            String jpql = "SELECT sc FROM SolicitudCredito sc"
                    + " WHERE sc.cliente.cedula=?1";
            Query q = em.createQuery(jpql, SolicitudCredito.class);
            q.setParameter(1, cedula);
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }

    }

}
