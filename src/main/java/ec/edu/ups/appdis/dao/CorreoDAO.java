/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.dao;

import ec.edu.ups.appdis.modelo.Correo;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * En esta Clase Define los metodos para agregar datos o listar la base de datos
 * de la tabla Cliente
 *
 * @author Ismael Castillo
 * @version 08/06/2020
 */
@Stateless
public class CorreoDAO {

    @PersistenceContext()
    private EntityManager em;

    public void insert(Correo correo) {
        try {
            em.persist(correo);
        } catch (Exception e) {
            throw e;
        }

    }

    public void update(Correo correo) {
        try {
            em.merge(correo);
        } catch (Exception e) {
            throw e;
        }

    }

    public Correo read(int id) {
        try {
            Correo c = em.find(Correo.class, id);
            return c;
        } catch (Exception e) {
            throw e;
        }

    }

    public void delete(int id) {
        try {
            Correo c = em.find(Correo.class, id);
            em.remove(c);
        } catch (Exception e) {
            throw e;
        }

    }

    public List<Correo> selectLike(String asunto) {
        try {
            String jpql = "SELECT c FROM Correo c"
                    + " WHERE asunto LIKE :filtro";

            Query q = em.createQuery(jpql, Correo.class);
            q.setParameter("filtro", asunto + "%");
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }

    }

    public List<Correo> selectAll() {
        try {
            String jpql = "SELECT c FROM Correo c";

            Query q = em.createQuery(jpql, Correo.class);
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }

    }

}
