/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.dao;

import ec.edu.ups.appdis.modelo.Acceso;
import ec.edu.ups.appdis.modelo.Cliente;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * En esta Clase Define los metodos para agregar datos o listar la base de datos
 * de la tabla Acceso
 *
 * @author Gustavo Guallpa
 * @version 08/06/2020
 */
@Stateless
public class AccesoDAO implements Serializable {

    @PersistenceContext

    private EntityManager em;

    /**
     * El metodo insertAcces me permite persistir el acceso del cliente en la
     * base de datos
     *
     * @param acceso
     */
    public void insertAcces(Acceso acceso) {
        try {
            em.persist(acceso);
        } catch (Exception e) {
            throw e;
        }

    }

    /**
     * El metodo getCorrectAccesses me permite obtener un acceso correcto de la
     * base de datos
     *
     * @param cedula
     * @return 
     */
    public List<Acceso> getCorrectAccesses(String cedula) {
        String correcto = "CORRECTO";
        List<Acceso> accesoc = new ArrayList<>();
        String consulta;
        try {
            consulta = "SELECT a FROM Acceso a WHERE a.decripcion = ?1";
            Query query = em.createQuery(consulta);
            query.setParameter(1, correcto);
            List<Acceso> lista = query.getResultList();
            for (Acceso acceso : lista) {
                if (acceso.getCliente().getCedula().equals(cedula)) {
                    accesoc.add(acceso);
                }
            }
            return accesoc;
        } catch (Exception e) {
            throw e;
        }

    }

    /**
     * El metodo getIncorrectAccesses me permite obtener un acceso incorrecto 
     * de la base de datos
     * @param cedula
     * @return 
     */
    public List<Acceso> getIncorrectAccesses(String cedula) {
        String incorrecto = "INCORRECTO";
        System.out.println("INCORRECTO");
        List<Acceso> accesoi = new ArrayList<>();
        String consulta;
        try {
            consulta = "SELECT a FROM Acceso a WHERE a.decripcion = ?1";
            Query query = em.createQuery(consulta);
            query.setParameter(1, incorrecto);
            List<Acceso> lista = query.getResultList();
            for (Acceso acceso : lista) {
                if (acceso.getCliente().getCedula().equals(cedula)) {
                    accesoi.add(acceso);
                }
            }
            return accesoi;
        } catch (Exception e) {
            throw e;
        }

    }

}
