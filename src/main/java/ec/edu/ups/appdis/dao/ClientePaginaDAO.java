/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.dao;

import ec.edu.ups.appdis.modelo.Cliente;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * En esta Clase Define los metodos para agregar datos o listar la base de datos
 * de la tabla Cliente
 *
 * @author Ismael Castillo
 * @version 08/06/2020
 */
@Stateless
public class ClientePaginaDAO {

    @PersistenceContext
    private EntityManager em;

    /**
     * El metodo insert permite persistir un cliente en la base de datos
     *
     * @param cliente
     */
    public void insert(Cliente cliente) {
        try {
            em.persist(cliente);
        } catch (Exception e) {
            throw e;
        }
        
    }

    /**
     * El metodo update nos permite revalidar los datos de un cliente existente
     * en la base de datos
     *
     * @param cliente
     */
    public void update(Cliente cliente) {
        try {
            em.merge(cliente);
        } catch (Exception e) {
            throw e;
        }

    }

    /**
     * El metodo Read nos permite buscar o leer un cliente en la base de datos
     *
     * @param cedula
     * @return
     */
    public Cliente read(String cedula) {
        try {
            Cliente c = em.find(Cliente.class, cedula);
            return c;
        } catch (Exception e) {
            throw e;
        }

    }

    /**
     * El metodo delete nos permite eliminar un cliente de la base de datos
     *
     * @param cedula
     */
    public void delete(String cedula) {
        try {
            Cliente c = em.find(Cliente.class, cedula);
            em.remove(c);
        } catch (Exception e) {
            throw e;
        }

    }

    /**
     * El metodo selectLike nos permite obtener una lista de clientes de la base
     * de datos en base a un filtro que se le provea al metodo
     *
     * @param filtro
     * @return
     */
    public List<Cliente> selectLike(String filtro) {
        try {
            String jpql = "SELECT u FROM Cliente u"
                    + " WHERE cedula LIKE :filtro";

            Query q = em.createQuery(jpql, Cliente.class);
            q.setParameter("filtro", filtro + "%");
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }

    }

    /**
     * El metodo selectAll nos permite obtener todos los clientes registrados en
     * la base de datos
     *
     * @return
     */
    public List<Cliente> selectAll() {
        try {
            String jpql = "SELECT u FROM Cliente u";

            Query q = em.createQuery(jpql, Cliente.class);
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }

    }
}
