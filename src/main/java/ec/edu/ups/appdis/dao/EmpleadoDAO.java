/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.dao;

import ec.edu.ups.appdis.modelo.Empleado;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * En esta Clase Define los metodos para agregar datos o listar la base de datos
 * de la tabla Empleado
 *
 * @author Ismael Castillo
 * @version 08/06/2020
 */
@Stateless
public class EmpleadoDAO {

    @PersistenceContext()
    private EntityManager em;

    /**
     * Este metodo insert me permite persistir datos al empleado.
     *
     * @param empleado
     */
    public void insert(Empleado empleado) {
        try {
            em.persist(empleado);
        } catch (Exception e) {
            throw e;
        }

    }

    /**
     * El metodo update permite actualizar datos del empleado
     *
     * @param empleado
     */
    public void update(Empleado empleado) {
        try {
            em.merge(empleado);
        } catch (Exception e) {
            throw e;
        }

    }

    /**
     * El metodo read me permite buscar o leer datos del Empleado
     *
     * @param cedula
     * @return
     */
    public Empleado read(String cedula) {
        try {
            Empleado e = em.find(Empleado.class, cedula);
            return e;
        } catch (Exception e) {
            throw e;
        }

    }

    /**
     * El metodo delete me permite eliminar los datos del Empleado
     *
     * @param cedula
     */
    public void delete(String cedula) {
        try {
            Empleado e = em.find(Empleado.class, cedula);
            em.remove(e);
        } catch (Exception e) {
            throw e;
        }

    }

    /**
     * El metodo selectLike me permite obbtener un listado en base al filtro del
     * paraetro.
     *
     * @param filtro
     * @return
     */
    public List<Empleado> selectLike(String filtro) {
        try {
            String jpql = "SELECT e FROM Empleado e"
                    + " WHERE cedula LIKE :filtro";

            Query q = em.createQuery(jpql, Empleado.class);
            q.setParameter("filtro", filtro + "%");
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }

    }

    /**
     * El metodo seleectAll me prmite listar todos los datos del Empleado
     *
     * @return
     */
    public List<Empleado> selectAll() {
        try {
            String jpql = "SELECT e FROM Empleado e";

            Query q = em.createQuery(jpql, Empleado.class);
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }

    }

}
