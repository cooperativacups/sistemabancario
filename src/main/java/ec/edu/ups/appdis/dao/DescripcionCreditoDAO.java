/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.dao;

import ec.edu.ups.appdis.modelo.DescripcionCredito;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author ismael
 */
@Stateless
public class DescripcionCreditoDAO {

    @PersistenceContext()
    private EntityManager em;

    public void insert(DescripcionCredito descripcionCredito) {
        try {
            em.persist(descripcionCredito);
        } catch (Exception e) {
            throw e;
        }

    }

    public void update(DescripcionCredito descripcionCredito) {
        try {
            em.merge(descripcionCredito);
        } catch (Exception e) {
            throw e;
        }

    }

    public DescripcionCredito read(String codigo) {
        try {
            DescripcionCredito dc = em.find(DescripcionCredito.class, codigo);
            return dc;
        } catch (Exception e) {
            throw e;
        }

    }

    public void delete(String codigo) {
        try {
            DescripcionCredito dc = em.find(DescripcionCredito.class, codigo);
            em.remove(dc);
        } catch (Exception e) {
            throw e;
        }

    }

    public DescripcionCredito selectCodigo(String descripcion) {
        try {
            String jpql = "SELECT dc FROM DescripcionCredito dc"
                    + " WHERE dc.descripcion = :filtro";

            Query q = em.createQuery(jpql, DescripcionCredito.class);
            q.setParameter("filtro", descripcion + "%");
            return (DescripcionCredito) q.getSingleResult();
        } catch (Exception e) {
            throw e;
        }

    }

    public List<DescripcionCredito> selectLike(String descripcion) {
        try {
            String jpql = "SELECT dc FROM DescripcionCredito dc"
                    + " WHERE dc.descripcion LIKE :filtro";

            Query q = em.createQuery(jpql, DescripcionCredito.class);
            q.setParameter("filtro", descripcion + "%");
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }

    }

    public List<DescripcionCredito> selectAll() {
        try {
            String jpql = "SELECT dc FROM DescripcionCredito dc";

            Query q = em.createQuery(jpql, DescripcionCredito.class);
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }

    }

}
