/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.dao;

import ec.edu.ups.appdis.modelo.Credito;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author ggual
 */
@Stateless
public class CreditoDAO {

    @PersistenceContext
    private EntityManager em;

    public void insert(Credito credito) {
        try {
            em.persist(credito);
        } catch (Exception e) {
            throw e;
        }

    }

    public void update(Credito credito) {
        try {
            em.merge(credito);
        } catch (Exception e) {
            throw e;
        }

    }

    public Credito read(int numeroCredito) {
        try {
            Credito cr = em.find(Credito.class, numeroCredito);
            return cr;
        } catch (Exception e) {
            throw e;
        }

    }

    public void delete(int numeroCredito) {
        try {
            Credito cr = em.find(Credito.class, numeroCredito);
            em.remove(cr);
        } catch (Exception e) {
            throw e;
        }

    }

    public List<Credito> selectLike(String numeroCuenta) {
        try {
            String jpql = "SELECT cr FROM Credito cr"
                    + " WHERE cr.CuentaAhorro.numeroCuenta LIKE :filtro";

            Query q = em.createQuery(jpql, Credito.class);
            q.setParameter("filtro", numeroCuenta + "%");
            return q.getResultList();

        } catch (Exception e) {
            throw e;
        }

    }

    /**
     * M[etodo para listar cada uno de los créditos que posees un usuario de la
     * cooperativa
     *
     * @param numeroCuenta
     * @return
     */
    public List<Credito> numeroCreditosUsuario(String numeroCuenta) {
        try {
            String jpql = "SELECT cr FROM Credito cr"
                    + " WHERE cr.cuentaAhorro.numeroCuenta=?1";
            Query q = em.createQuery(jpql, Credito.class);
            q.setParameter(1, numeroCuenta);
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }

    }

    public List<Credito> selectAll() {
        try {
            String jpql = "SELECT cr FROM Credito cr";

            Query q = em.createQuery(jpql, Credito.class);
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }

    }

    public List<Credito> listarCreditosAprobados(String numeroCuenta) {
        try {
            String jpql = "SELECT cr FROM Credito cr"
                    + " WHERE cr.cuentaAhorro.numeroCuenta=?1 and cr.estadoCredito=" + "'ESPERA'";
            Query q = em.createQuery(jpql, Credito.class);
            q.setParameter(1, numeroCuenta);
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }

    }

    public List<Credito> listarCreditosAprobadosUsuario() {
        try {
            String jpql = "SELECT cr FROM Credito cr"
                    + " WHERE cr.estadoCredito=" + "'ACEPTADO'";
            Query q = em.createQuery(jpql, Credito.class);
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }

    }

    /**
     * *
     * Listar el credito aprobado del cliente.
     *
     * @param numeroCuenta
     * @return
     */
    public List<Credito> detalleCreditoCliente(String numeroCuenta) {
        try {
            String jpql = "SELECT cr FROM Credito cr"
                    + " WHERE cr.cuentaAhorro.numeroCuenta=?1 and cr.estadoCredito=" + "'ACEPTADO'";
            Query q = em.createQuery(jpql, Credito.class);
            q.setParameter(1, numeroCuenta);
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }

    }
      /**
     * Buscar el credito del cliente.
     */
    
    public Credito buscarCreditoCliente(String numeroCuenta){
        try {
            String jpql = "SELECT cr FROM Credito cr"
                    + " WHERE cr.cuentaAhorro.numeroCuenta=?1";
            Query q = em.createQuery(jpql, Credito.class);
            q.setParameter(1, numeroCuenta);
            return (Credito) q.getSingleResult();
        } catch (Exception e) {
            throw e;
        }
    }

}
