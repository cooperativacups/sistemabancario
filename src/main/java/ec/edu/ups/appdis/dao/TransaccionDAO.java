/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.dao;

import ec.edu.ups.appdis.modelo.Empleado;
import ec.edu.ups.appdis.modelo.Transaccion;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 * En esta Clase Define los metodos para agregar datos o listar la base de datos
 * de la tabla Transacción
 *
 * @author Jorge Arévalo
 * @version 08/06/2020
 */
@Stateless
public class TransaccionDAO {

    @PersistenceContext
    private EntityManager em;

    /**
     * El metodo insertTransaction me permite persister una Transacción
     *
     * @param transaccion
     */
    public void insertTransaction(Transaccion transaccion) {
        try {
            em.persist(transaccion);
        } catch (Exception e) {

        }
    }

    /**
     * El metodo leer me permite buscar un dato en base en el parametro
     *
     * @param codigo
     * @return
     * @throws Exception
     */
    public Transaccion leer(String codigo) throws Exception {

        try {
            return em.find(Transaccion.class, codigo);

        } catch (Exception e) {
            System.out.println("Error TransaccionDAO");
        }
        return null;
    }

    /**
     * EL metodo listar me devuelve un listado de la Transacción
     *
     * @return
     */
    public List<Transaccion> listar() {
        String jpql = "SELECT t FROM Transaccion t";
        Query q = em.createQuery(jpql, Transaccion.class);
        System.out.println(" Datos de la Base");
        return q.getResultList();
    }

    /**
     * El metodo getLastDateTransacction me permite obtener la ultima fecha que
     * el usuario hizo una transaccion
     *
     * @param cuenta
     * @return
     */
    public String getLastDateTransacction(String cuenta) {
        String fecha = "";
        //System.out.println("LA ULTIMA FECHA DE TRANASACCION ES ");
        String jpql = "SELECT t FROM Transaccion t ORDER by id ASC";
        Query q = em.createQuery(jpql, Transaccion.class);
        List<Transaccion> lista = q.getResultList();
        for (Transaccion t : lista) {
            if (t.getCuentaAhorro().getNumeroCuenta().equals(cuenta)) {
                fecha = t.getFecha();
            }
        }
        return fecha;

    }

    /**
     * Este metodo getListaTransaccionesFechas me permite listar entre fechas en
     * base a los parametros
     *
     * @param numeroCuenta
     * @param concepto
     * @param fechaInicial
     * @param fechaFinal
     * @return
     * @throws Exception
     */
    public List<Transaccion> getListaTransaccionesFechas(String numeroCuenta, String concepto, String fechaInicial, String fechaFinal) throws Exception {

        String jpql = "SELECT e FROM Transaccion e"
                + " WHERE e.cuentaAhorro.numeroCuenta = :numeroCuenta AND e.tipo = :concepto AND e.fecha BETWEEN :fechaInicial AND :fechaFinal";

        Query q = em.createQuery(jpql, Transaccion.class);
        q.setParameter("numeroCuenta", numeroCuenta);
        q.setParameter("concepto", concepto);
        q.setParameter("fechaInicial", fechaInicial);
        q.setParameter("fechaFinal", fechaFinal);
        return q.getResultList();

    }

    /**
     * Este metodo getListaUlimasTransaccionesFechas me permite listar las
     * ultimas transaccion en base a los parametros
     *
     * @param numeroCuenta
     * @param fechaInicial
     * @param fechaFinal
     * @return
     * @throws Exception
     */
    public List<Transaccion> getListaUlimasTransaccionesFechas(String numeroCuenta, String fechaInicial, String fechaFinal) throws Exception {

        String jpql = "SELECT e FROM Transaccion e"
                + " WHERE e.cuentaAhorro.numeroCuenta = :numeroCuenta AND e.fecha BETWEEN :fechaInicial AND :fechaFinal";

        Query q = em.createQuery(jpql, Transaccion.class);
        q.setParameter("numeroCuenta", numeroCuenta);
        q.setParameter("fechaInicial", fechaInicial);
        q.setParameter("fechaFinal", fechaFinal);
        return q.getResultList();

    }

}
