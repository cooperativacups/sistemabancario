/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.dao;

import ec.edu.ups.appdis.modelo.Cliente;
import ec.edu.ups.appdis.modelo.Empleado;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * En esta Clase Define los metodos para agregar datos o listar la base de datos
 * de la tabla Empleado
 *
 * @author Gustavo Guallpa
 * @version 08/06/2020
 */
@Stateless
public class EmpleadoLoginDAO {

    @PersistenceContext

    private EntityManager em;

    /**
     * El metodo searchEmployee me permite buscar un Empleado en base al filtro
     * del parametro.
     *
     * @param nombreUsuario
     * @return
     */
    public Empleado searchEmployee(String nombreUsuario) {
        Empleado empleado = null;
        String consulta = "";
        try {
            consulta = "SELECT e from Empleado e WHERE e.usuario=?1";
            Query query = em.createQuery(consulta);
            query.setParameter(1, nombreUsuario);
            List<Empleado> lista = query.getResultList();
            if (!lista.isEmpty()) {
                empleado = lista.get(0);
            }

        } catch (Exception e) {
            throw e;
        }
        return empleado;
    }

    public Empleado read(String cedula) {
        try {
            Empleado e = em.find(Empleado.class, cedula);
            return e;
        } catch (Exception e) {
            throw e;
        }

    }

//    public Cliente login(Cliente cli) {
//        Cliente empleado = null;
//        String consulta = "";
//        try {
//            consulta = "SELECT c from Cliente c WHERE c.usuario=?1 and c.contrasena=?2";
//            Query query = em.createQuery(consulta);
//            query.setParameter(1, cli.getUsuario());
//            query.setParameter(2, cli.getContrasena());
//            List<Cliente> lista = query.getResultList();
//            if (!lista.isEmpty()) {
//                empleado = lista.get(0);
//            }
//
//        } catch (Exception e) {
//            System.out.println("DAO" + e.getMessage());
//        }
//        return empleado;
//    }
//    public List<Cliente> getCustomers() {
//        String consulta = "";
//        try {
//            consulta = "SELECT u from Usuario u";
//            Query query = em.createQuery(consulta);
//            List<Cliente> lista = query.getResultList();
//            if (!lista.isEmpty()) {
//                return lista;
//            }
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
//        return null;
//    }
}
