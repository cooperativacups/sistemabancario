package ec.edu.ups.appdis.dao;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ec.edu.ups.appdis.modelo.Amortizacion;

/**
 *
 * @author Gustavo Guallpa
 * @author Ismael Castillo
 * @author Jorge Arévalo
 * 
 * Clase AmortizacionDAO permite poder realizar la acciones necesarias en la base de datos de la entidad amortizacion.
 * @version 1.0
 * @since 30/07/2020
 */
@Stateless
public class AmortizacionDAO {

    @PersistenceContext()
    private EntityManager em;

    public void insert(Amortizacion amortizacion) {

        try {
            em.persist(amortizacion);
        } catch (Exception e) {
            throw e;
        }

    }

    public void update(Amortizacion amortizacion) {

        try {
            em.merge(amortizacion);
        } catch (Exception e) {
            throw e;
        }

    }

    public Amortizacion read(int id) {
        try {
            Amortizacion c = em.find(Amortizacion.class, id);
            return c;
        } catch (Exception e) {
            throw e;
        }

    }

    public void delete(int id) {
        try {
            Amortizacion c = em.find(Amortizacion.class, id);
            em.remove(c);
        } catch (Exception e) {
            throw e;
        }

    }

    public List<Amortizacion> selectLike(String numeroCredito) {
        try {
            String jpql = "SELECT c FROM Amortizacion c"
                    + " WHERE c.numeroCredito = :filtro";

            Query q = em.createQuery(jpql, Amortizacion.class);
            q.setParameter("filtro", numeroCredito + "%");
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }

    }

    public List<Amortizacion> selectAll() {
        try {
            String jpql = "SELECT c FROM Amortizacion c";

            Query q = em.createQuery(jpql, Amortizacion.class);
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }

    }

    public List<Amortizacion> mostrarAmortizacionCliente(int numeroCredito) {
        try {
            String jpql = "SELECT c FROM Amortizacion c"
                    + " WHERE c.credito.numeroCredito = " + numeroCredito + "ORDER BY c.nPeriodos";
            Query q = em.createQuery(jpql, Amortizacion.class);
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Amortizacion> mostrarAmortizacionClientePendientes(int numeroCredito) {
        try {
            String jpql = "SELECT c FROM Amortizacion c"
                    + " WHERE c.credito.numeroCredito = " + numeroCredito + "AND c.estadoCuota=" + "'PENDIENTE'" + "ORDER BY c.nPeriodos";
            Query q = em.createQuery(jpql, Amortizacion.class);
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Amortizacion> mostrarAmortizacionClienteVencidas(int numeroCredito) {
        try {
            String jpql = "SELECT c FROM Amortizacion c"
                    + " WHERE c.credito.numeroCredito = " + numeroCredito + "AND c.estadoCuota=" + "'VENCIDO'" + "ORDER BY c.nPeriodos";
            Query q = em.createQuery(jpql, Amortizacion.class);
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Amortizacion> mostrarAmortizacionClienteDes(int numeroCredito) {
        try {
            String jpql = "SELECT c FROM Amortizacion c"
                    + " WHERE c.credito.numeroCredito = " + numeroCredito + "AND c.estadoCuota=" + "'PENDIENTE'" + "ORDER BY c.nPeriodos DESC";
            Query q = em.createQuery(jpql, Amortizacion.class);
            return q.getResultList();
        } catch (Exception e) {
            throw e;
        }
    }

}
