/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.dao;

import ec.edu.ups.appdis.modelo.Cliente;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * En esta Clase Define los metodos para buscar en la tabla del Cliente 
 * la base de datos de la tabla Cliente
 * @author Gustavo Guallpa
 * @version 08/06/2020
 */

@Stateless
public class ClienteLoginDAO {

    @PersistenceContext

    private EntityManager em;

    /**
     * El metodo searchCustomer nos permite obtener un cliente de la base de 
     * datos para comparar con las credenciales proporcionadas
     * para la redireccion a su cuenta respectiva
     * @param nombreUsuario
     * @return 
     */
    public Cliente searchCustomer(String nombreUsuario) {
        Cliente cliente = null;
        String consulta = "";
        try {
            consulta = "SELECT c from Cliente c WHERE c.usuario=?1";
            Query query = em.createQuery(consulta);
            query.setParameter(1, nombreUsuario);
            List<Cliente> lista = query.getResultList();
            if (!lista.isEmpty()) {
                cliente = lista.get(0);
            }

        } catch (Exception e) {
            System.out.println("DAO" + e.getMessage());
        }
        return cliente;
    }
    
    
//    public Cliente login(Cliente cli) {
//        Cliente cliente = null;
//        String consulta = "";
//        try {
//            consulta = "SELECT c from Cliente c WHERE c.usuario=?1 and c.contrasena=?2";
//            Query query = em.createQuery(consulta);
//            query.setParameter(1, cli.getUsuario());
//            query.setParameter(2, cli.getContrasena());
//            List<Cliente> lista = query.getResultList();
//            if (!lista.isEmpty()) {
//                cliente = lista.get(0);
//            }
//
//        } catch (Exception e) {
//            System.out.println("DAO" + e.getMessage());
//        }
//        return cliente;
//    }

//    public List<Cliente> getCustomers() {
//        String consulta = "";
//        try {
//            consulta = "SELECT u from Usuario u";
//            Query query = em.createQuery(consulta);
//            List<Cliente> lista = query.getResultList();
//            if (!lista.isEmpty()) {
//                return lista;
//            }
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
//        return null;
//    }

}
