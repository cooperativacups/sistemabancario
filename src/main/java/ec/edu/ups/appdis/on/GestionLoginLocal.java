/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.on;

import ec.edu.ups.appdis.modelo.Acceso;
import ec.edu.ups.appdis.modelo.Cliente;
import ec.edu.ups.appdis.modelo.Empleado;
import java.util.List;
import javax.ejb.Local;

/**
 * Clase de tipo Interface con los metodos.
 * @author Gustavo Guallpa
 * @version 08/06/2020
 */
@Local
public interface GestionLoginLocal {

    ///public Cliente login(Cliente cli);

    ///public List<Cliente> getUsers();

    public void insertCorrectAcces(Cliente cliente);

    public void insertIncorrectAcces(Cliente cliente);
    
    public Cliente searchCustomer(String nombreUsuario);
    
    
     public Empleado searchEmployee(String nombreUsuario);
    
     public Empleado findEmployee(String cedula);
     
}
