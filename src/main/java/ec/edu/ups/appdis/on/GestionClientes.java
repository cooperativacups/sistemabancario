package ec.edu.ups.appdis.on;

import ec.edu.ups.appdis.dao.ClienteDAO;
import ec.edu.ups.appdis.modelo.Cliente;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;
import java.util.Random;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author Ismael Castillo
 * @version 1.0
 * @since 30/07/2020
 */
@Stateless
public class GestionClientes {

    @Inject
    ClienteDAO clienteDAO;

    /**
     * El metodo guardarCliente nos permite almacenar un cliente atraves del DAO
     * en la base de datos
     *
     * @param cliente
     * @throws Exception
     */
    public boolean guardarCliente(Cliente cliente) throws Exception {
        Cliente c = clienteDAO.read(cliente.getCedula());
        if (c != null) {
            clienteDAO.update(cliente);
            return false;
        } else {
            clienteDAO.insert(cliente);
            return true;
        }
    }

    public boolean guardarCliente1(Cliente cliente) throws Exception {
        boolean valor = validarcedula(cliente.getCedula());
        if (valor == true) {
            clienteDAO.insert(cliente);
            return true;
        } else {
            return false;
        }
    }

    /**
     * El metodo listarClientes nos permite obtener todos los clientes atraves
     * del DAO de la base de datos
     *
     * @return
     * @throws Exception
     */
    public List<Cliente> listarClientes() throws Exception {
        return clienteDAO.selectAll();
    }

    /**
     * El metodo eliminarCliente nos permite eliminar un clinete atraves del DAO
     * de la base de datos
     *
     * @param cedula
     * @throws Exception
     */
    public void eliminarCliente(String cedula) throws Exception {
        clienteDAO.delete(cedula);
    }

    /**
     * El metodo buscarCliente nos permite buscar un cliente atraves del DAO de
     * la base de datos
     *
     * @param cedula
     * @return
     * @throws Exception
     */
    public Cliente buscarCliente(String cedula) throws Exception {
        return clienteDAO.read(cedula);
    }

    public Cliente buscarClienteUsuario(String usuario) throws Exception {
        return clienteDAO.selectCredenciales(usuario);
    }

    public Cliente updatePassword(String correo) throws Exception {
        return clienteDAO.selectUpdatePassword(correo);
    }


    /**
     * El metodo buscarClienteLike nos permite obtener un listado de clientes
     * atraves del DAO de la base de datos
     *
     * @param cedula
     * @return
     * @throws Exception
     */
    public List<Cliente> buscarClienteLike(String cedula) throws Exception {
        List<Cliente> clientes = clienteDAO.selectLike(cedula);
        return clientes;
    }

    public String generarContrasena() throws NoSuchAlgorithmException {
        String[] symbols = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};
        int length = 2;
        Random random = SecureRandom.getInstanceStrong();
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int indexRandom = random.nextInt(symbols.length);
            sb.append(symbols[indexRandom]);
        }
        String password = sb.toString();
        return password;
    }

    public String generarUsuario() throws NoSuchAlgorithmException {
        String[] symbols = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
        int length = 3;
        Random random = SecureRandom.getInstanceStrong();
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int indexRandom = random.nextInt(symbols.length);
            sb.append(symbols[indexRandom]);
        }
        String password = sb.toString();

        return password;
    }

    public boolean validarcedula(String cedula) {
        if (cedula.length() == 10) {
            char digitoN;
            int ultimo = 0;
            int suma = 0;
            int digitoVerificador = 0;

            for (int i = 0; i < cedula.length() - 1; i++) {
                digitoN = cedula.charAt(i);
                int digitoAscii = (int) cedula.charAt(i);
                int resultado = 0;

                switch (digitoAscii) {
                    case 48:
                        digitoN = 0;
                        break;
                    case 49:
                        digitoN = 1;
                        break;
                    case 50:
                        digitoN = 2;
                        break;
                    case 51:
                        digitoN = 3;
                        break;
                    case 52:
                        digitoN = 4;
                        break;
                    case 53:
                        digitoN = 5;
                        break;
                    case 54:
                        digitoN = 6;
                        break;
                    case 55:
                        digitoN = 7;
                        break;
                    case 56:
                        digitoN = 8;
                        break;
                    case 57:
                        digitoN = 9;
                        break;
                }
                if (i % 2 == 0) {
                    resultado = digitoN * 2;
                    if (resultado >= 10) {
                        resultado -= 9;
                    }
                } else {
                    resultado = digitoN * 1;
                }
                suma += resultado;

            }
            digitoVerificador = (((suma / 10) + 1) * 10) - suma;
            if (digitoVerificador == 10) {
                digitoVerificador = 0;
            }
            ultimo = (int) cedula.charAt(9);

            switch (ultimo) {
                case 48:
                    ultimo = 0;
                    break;
                case 49:
                    ultimo = 1;
                    break;
                case 50:
                    ultimo = 2;
                    break;
                case 51:
                    ultimo = 3;
                    break;
                case 52:
                    ultimo = 4;
                    break;
                case 53:
                    ultimo = 5;
                    break;
                case 54:
                    ultimo = 6;
                    break;
                case 55:
                    ultimo = 7;
                    break;
                case 56:
                    ultimo = 8;
                    break;
                case 57:
                    ultimo = 9;
                    break;
            }
            if (ultimo == digitoVerificador) {
                //JOptionPane.showMessageDialog(this, "Numero De Cedula Correcto");
                return true;
            } else {
                return true;
            }
        } else {
            return false;
        }

    }

}
