package ec.edu.ups.appdis.on;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import ec.edu.ups.appdis.dao.ClienteDAO;
import ec.edu.ups.appdis.modelo.Cliente;
import ec.edu.ups.appdis.modelo.ClienteDjango;

/**
 * 
 *@version 1.0
 *@since 30/07/2020
 */
@Stateless
public class GestionDataAnalisis {

	@Inject
	private GestionClientes gestionClientes;
	@Inject
	ClienteDAO clienteDAO;

	private String WS_GET_BUSCARCLIENTE = "http://35.232.57.30:8000/apiAnalisis/clientesFiltroDinamico/?search=104600002&search_fields=cedula";
	private String WS_GET_LISTADOCLIENTES = "http://35.232.57.30:8000/apiAnalisis/clientes/";
	private String WS_POST_PREDECIR = "http://35.232.57.30:8000/apiAnalisis/predecirTipoCliente/";
	private String WS_GET_PORTIPOMALO = "http://35.232.57.30:8000/apiAnalisis/clientesportipo/?search=2";
	private String WS_GET_PORTIPOBUENO = "http://35.232.57.30:8000/apiAnalisis/clientesportipo/?search=1";

	public ClienteDjango obtenerClienteCedula(String cedula) {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(WS_GET_BUSCARCLIENTE).path("/?search=" + cedula + "/&search_fields=cedula");
		ClienteDjango cliente = target.request().get(ClienteDjango.class);
		client.close();
		return cliente;
	}

	public ClienteDjango obtenerClienteId() {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(WS_GET_LISTADOCLIENTES).path("/4");
		ClienteDjango cliente = target.request(MediaType.APPLICATION_JSON).get(ClienteDjango.class);
		client.close();
		return cliente;
	}

	public String predecirSolicitud(String solicitudCredito) {
		//String credito = "{\"credito\":\""+"0100214580;6;A34;A43;2000;A65;A75;4;A93;A101;4;A121;67;A152;2;A173;A201;2"+"\"}";
		System.out.println(solicitudCredito);
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(WS_POST_PREDECIR);
		String respuesta = target.request().post(Entity.json(solicitudCredito), String.class);
		client.close();
		return respuesta;
	}

	public List<ClienteDjango> obtenerClientesMalos() {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(WS_GET_PORTIPOMALO);
		List<ClienteDjango> clientes = target.request(MediaType.APPLICATION_JSON).get(new GenericType<List<ClienteDjango>>(){});
		client.close();
		return clientes;
	}

	public List<ClienteDjango> obtenerClientesBuenos() {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(WS_GET_PORTIPOBUENO);
		List<ClienteDjango> clientes = target.request(MediaType.APPLICATION_JSON).get(new GenericType<List<ClienteDjango>>(){});
		client.close();
		return clientes;
	}

	public List<ClienteDjango> obtenerClientes() {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(WS_GET_LISTADOCLIENTES);
		List<ClienteDjango> clientes = target.request(MediaType.APPLICATION_JSON).get(new GenericType<List<ClienteDjango>>(){});
		client.close();
		return clientes;
	}

	public List<Cliente> getClientes() {
		List<Cliente> clientes = new ArrayList<>();
		List<ClienteDjango> clienteDjangos = obtenerClientes();
		for(ClienteDjango clienteDjango: clienteDjangos) {
			Cliente cliente = clienteDAO.read("0"+clienteDjango.getCedula());
			if(cliente != null) {
				clientes.add(cliente);
				System.out.println(clientes.toString());
			}
		}
		return clientes;
	}

	public List<Cliente> getClientesBuenos() throws Exception{
		List<Cliente> clientes = new ArrayList<>();
		List<ClienteDjango> clienteDjangos = obtenerClientesBuenos();
		for(ClienteDjango clienteDjango: clienteDjangos) {
			Cliente cliente = clienteDAO.read("0"+clienteDjango.getCedula());
			if(cliente != null) {
				clientes.add(cliente);
				System.out.println(clientes.toString());
			}

		}
		return clientes;
	}

	public List<Cliente> getClientesMalos() throws Exception{
		List<Cliente> clientes = new ArrayList<>();
		List<ClienteDjango> clienteDjangos = obtenerClientesMalos();
		for(ClienteDjango clienteDjango: clienteDjangos) {
			Cliente cliente = clienteDAO.read("0"+clienteDjango.getCedula());
			if(cliente != null) {
				clientes.add(cliente);
				System.out.println(clientes.toString());
			}
		}
		return clientes;
	}
	
}
