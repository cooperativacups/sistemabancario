/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.on;

import ec.edu.ups.appdis.dao.AmortizacionDAO;
import ec.edu.ups.appdis.dao.CreditoDAO;
import ec.edu.ups.appdis.modelo.Amortizacion;
import ec.edu.ups.appdis.modelo.Credito;
import ec.edu.ups.appdis.modelo.CuentaAhorro;
import ec.edu.ups.appdis.modelo.Mensaje;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.MessagingException;

/**
 *
 * @author ggual
 */
@Stateless
public class GestionCreditos {

    @Inject
    private CreditoDAO creditoDAO;
    @Inject
    private GestionPaginaClienteLocal gestionClientes;

    @Inject
    private AmortizacionDAO amortizacionDAO;

    @Inject
    private GestionCorreo gestionCorreo;

    @Inject
    private GestionSolicitudCredito gestionSolicitudCredito;

    public int numeroCreditosUsuario(String numeroCuenta) {
        List<Credito> creditos = new ArrayList();
        creditos = creditoDAO.numeroCreditosUsuario(numeroCuenta);
        int numeroCreditos = creditos.size();
        return numeroCreditos;
    }

    public void crearCredito(List<Amortizacion> lista, String proposito, String cedula, double valorCredito) throws MessagingException, Exception {

        try {
            CuentaAhorro cuenta = gestionClientes.searchSavingsAccountCedula(cedula);
            Double saldoCredito = 0.0;
            Date fechaVencimiento = null;
            Credito credito = new Credito();
            List<Amortizacion> tablaAmortizacions = new ArrayList<>();
            List<Credito> listaCredito = creditoDAO.selectAll();

            int numeroCredito = listaCredito.size() + 1;
            //NUMERO DE CREDITO
            credito.setNumeroCredito(numeroCredito);
            String pro = buscarTipoCredito(proposito);
            //TIPO DE CREDITO
            credito.setTipoCredito(pro);
            //VALOR DEL CREDITO
            credito.setValorCredito(valorCredito);
            //A LA TABLA DE AMORTIZACION LE AGREGO EL CREDITO
            for (Amortizacion amo : lista) {
                amo.setCredito(credito);
                tablaAmortizacions.add(amo);
            }
            //FECHA INICIAL DE PAGO
            for (int i = 0; i < 1; i++) {
                Amortizacion m = lista.get(i);
                saldoCredito = m.getSaldoInicial();
                fechaVencimiento = m.getFechaPago();
            }
            //SALDO
            credito.setSaldoCredito(saldoCredito);
            //FECHA DE VENCIMIENTO
            credito.setFechaVencimiento(fechaVencimiento);
            //CUENTA DE AHORROS
            credito.setCuentaAhorro(cuenta);
            credito.setEstadoCredito("ESPERA");
            ///AHORA SOLO INSERTAR DATOS DEL CREDITO Y DE LA TABLA DE AMORTIZACION
            creditoDAO.insert(credito);
            //INSERTO LA TABLA DE AMORTIZACION
            for (Amortizacion amortizacion : tablaAmortizacions) {
                amortizacionDAO.insert(amortizacion);
            }

            //PDF
            gestionSolicitudCredito.generarPdfTablaAmortizacion(String.valueOf(numeroCredito), tablaAmortizacions, cuenta.getCliente());

            //ENVAIR CORREO
            gestionCorreo.notificacionCorreo(cuenta.getCliente(), "AprobacionCredito");
        } catch (Exception e) {

            throw e;
        }

        //ES PRECISO ACTALIZAR EL ESTADO DE LA SOLICITUD
    }

    public String buscarTipoCredito(String proposito) {

        String tipo = "";
        if (proposito.equalsIgnoreCase("Muebles/Equipamiento") || proposito.equalsIgnoreCase("Automóvil") || proposito.equalsIgnoreCase("Electrodomésticos") || proposito.equalsIgnoreCase("Vacaciones")) {
            tipo = "CREDITO DE CONSUMO";
        }
        if (proposito.equalsIgnoreCase("Inmuebles (Casa, Finca, etc)")) {
            tipo = "CREDITO HIPOTECARIO";
        }
        if (proposito.equalsIgnoreCase("Negocios") || proposito.equalsIgnoreCase("Tecnología")) {
            tipo = "CREDITO COMERCIAL";
        }
        if (proposito.equalsIgnoreCase("Capacitación") || proposito.equalsIgnoreCase("Educación")) {
            tipo = "CREDITO EDUCATIVO";
        }
        if (proposito.equalsIgnoreCase("Reparaciones")) {
            tipo = "CREDITO DE VIVIENDA";
        }
        if (proposito.equalsIgnoreCase("Otros")) {
            tipo = "CREDITO EMERGENTE";
        }

        return tipo;
    }

    /**
     * Método para buscar un crédito de un cliente
     *
     * @param numeroCuenta
     * @return Credito del cliente
     * @throws Exception
     */
    public Credito buscarCreditoCliente(String numeroCuenta) throws Exception {
        try {
            return creditoDAO.buscarCreditoCliente(numeroCuenta);
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Metodo para bsucar el credito , para actualizar su fecha con la proxima
     * fecha de pago
     *
     * @param numeroCredito
     * @return
     */
    public Credito buscarCredito(int numeroCredito) {
        try {
            return creditoDAO.read(numeroCredito);
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Credito> detalleCreditoCliente(String numeroCuenta) {
        System.out.println(" HOLA");
        return creditoDAO.detalleCreditoCliente(numeroCuenta);
    }

    public List<Credito> listarCreditosAprobados(String numeroCuenta) {
        return creditoDAO.listarCreditosAprobados(numeroCuenta);
    }

    public void actualizarCredito(Credito credito) {
        creditoDAO.update(credito);

    }

    public List<Credito> listarCreditosAprobadosUsuario() {
        return creditoDAO.listarCreditosAprobadosUsuario();
    }

    public List<Credito> selectAll() {
        try {
            return creditoDAO.selectAll();
        } catch (Exception e) {
            throw e;
        }

    }
}
