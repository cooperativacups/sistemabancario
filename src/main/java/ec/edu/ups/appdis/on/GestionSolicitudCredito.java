/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.on;

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import ec.edu.ups.appdis.dao.DescripcionCreditoDAO;
import ec.edu.ups.appdis.dao.SolicitudCreditoDAO;
import ec.edu.ups.appdis.modelo.Amortizacion;
import ec.edu.ups.appdis.modelo.Cliente;
import ec.edu.ups.appdis.modelo.DescripcionCredito;
import ec.edu.ups.appdis.modelo.Mensaje;
import ec.edu.ups.appdis.modelo.SolicitudCredito;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author ismael
 */
@Stateless
public class GestionSolicitudCredito {

    @Inject
    private SolicitudCreditoDAO solicitudCreditoDAO;
    @Inject
    private DescripcionCreditoDAO descripcionCreditoDAO;

    public Mensaje guardarSolicitudCredito(SolicitudCredito solicitudCredito) {
        try {
            boolean valor = solicitudCreditoDAO.insert(solicitudCredito);
            if (valor == true) {
                return new Mensaje(1, "ENVIADA CON ÉXITO");
            } else {
                return new Mensaje(0, " NO SE PUEDO ESTABLECER LA CONEXION CON EL SERVIDOR");
            }
        } catch (Exception e) {
            return new Mensaje(0, " NO SE PUEDO ESTABLECER LA CONEXION CON EL SERVIDOR");
        }

    }

    public void eliminarSolicitudCredito(int id) {
        solicitudCreditoDAO.delete(id);
    }

    public List<SolicitudCredito> listarSolicitudesCredito() {
        return solicitudCreditoDAO.selectAll();
    }

    public List<SolicitudCredito> listarSolicitudesCreditoUsuario(String cedula) {
        return solicitudCreditoDAO.listarSolicitudesCreditoUsuario(cedula);
    }

    public List<SolicitudCredito> listarSolicitudesNoProcesadas() {
        return solicitudCreditoDAO.listarSolicitudesNoProcesadas();
    }

    public List<SolicitudCredito> listarSolicitudesCreditosFiltro(String filtro) {
        return solicitudCreditoDAO.selectLike(filtro);
    }

    public int numeroSolicitudesCreditosUsuario(String cedula) {
        List<SolicitudCredito> soli = new ArrayList();
        soli = solicitudCreditoDAO.numeroSolicitudesCreditosUsuario(cedula);
        int numeroSolicitudes = soli.size();
        return numeroSolicitudes;
    }

    public SolicitudCredito buscarSolicitudCredito(int id) {
        SolicitudCredito sc = solicitudCreditoDAO.read(id);
        return sc;
    }

    public DescripcionCredito buscarDescripcion(String codigo) {
        return descripcionCreditoDAO.read(codigo);
    }

    public String codigoSaldoCuenta(double saldo) {

        if (saldo < 500) {
            return "A61";
        }
        if (saldo < 1000) {
            return "A62";
        }
        if (saldo < 1500) {
            return "A63";
        }
        if (saldo >= 1500) {
            return "A65";
        }

        return "Sin cuenta de ahorros";
    }

    public int calcularEdad(String fechaNacimiento) {
        String fecha = fechaNacimiento;
        String dia = fecha.substring(0, 2);
        //System.out.println(dia);
        String mes = fecha.substring(3, 5);
        //System.out.println(mes);
        String anio = fecha.substring(6, 10);
        //System.out.println(anio);

        int year = Integer.parseInt(anio);
        int month = Integer.parseInt(mes);
        int day = Integer.parseInt(dia);

        LocalDate today = LocalDate.now();
        LocalDate birthdate = LocalDate.of(year, month, day);

        Period p = Period.between(birthdate, today);
        int edad = p.getYears();
        return edad;
    }

    public SolicitudCredito descargarCedula(String filtro) throws Exception {
        return solicitudCreditoDAO.descargarCedula(filtro);
    }

    public SolicitudCredito descargarRol(String filtro) throws Exception {
        return solicitudCreditoDAO.descargarRol(filtro);
    }

    public SolicitudCredito descargarPlanilla(String filtro) throws Exception {
        return solicitudCreditoDAO.descargarPlanilla(filtro);
    }

    public void actulizarSolicitudCredito(SolicitudCredito solicitudCredito) {
        solicitudCreditoDAO.update(solicitudCredito);
    }
   
  
    
    public static final String destino = "C:\\Users\\ggual\\Videos\\Amortizacion.pdf";
    public static final String logo = "C:\\Users\\ggual\\Videos\\logo.png";
    
    public void generarPdfTablaAmortizacion(String numeroCredito, List<Amortizacion> amortizacions, Cliente cliente) throws IOException, MessagingException {
    //public void generarPdfTablaAmortizacion(List<Amortizacion> amortizacions) throws IOException, MessagingException {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String fecha = dateFormat.format(date);
        PdfWriter writer = new PdfWriter(destino);
        PdfDocument pdf = new PdfDocument(writer);
        Document documento = new Document(pdf, PageSize.A4);
        Paragraph paragraph;
        documento.setMargins(50,50,50,50);
        PdfFont font = PdfFontFactory.createFont(FontConstants.HELVETICA);
        Image image = new Image(ImageDataFactory.create(logo));
        image.setWidth(250);
        image.setHeight(60);
        paragraph = new Paragraph().add(image);
        documento.add(paragraph.setTextAlignment(TextAlignment.CENTER));
        paragraph = new Paragraph("COOPERATIVA CUPS").setBold().setFont(font).setFixedLeading(0);
        documento.add(paragraph.setTextAlignment(TextAlignment.CENTER));
        paragraph = new Paragraph("Nombre: " + cliente.getNombre() + " " + cliente.getApellido() +"\n"
                +"No.C.C.: " + cliente.getCedula() +"\n"
                +"Dirección: "+ cliente.getDireccion() +"\n"
                +"Ciudad: Cuenca"+"\n"
                +"Teléfono: "+ cliente.getTelefono() +"\n"
                +"Crédito #: "+ numeroCredito +"\n"
                +"Fecha de emisión:" + fecha + "\n");
        documento.add(paragraph.setTextAlignment(TextAlignment.LEFT));
        paragraph = new Paragraph("");
        documento.add(paragraph);
        paragraph = new Paragraph("Estimado socio:").setBold().setFont(font).setFixedLeading(0);
        documento.add(paragraph.setTextAlignment(TextAlignment.LEFT));
        paragraph = new Paragraph("En el presente documento se le presenta la tabla de amortización, " +
                "cabe recalcar que dicho documento es de carácter informativo para que se mantenga al día en el pago de las cuotas de su crédito vigente:");
        documento.add(paragraph.setTextAlignment(TextAlignment.JUSTIFIED));
        paragraph = new Paragraph("TABLA DE AMORTIZACIÓN").setBold().setFont(font);
        documento.add(paragraph.setTextAlignment(TextAlignment.CENTER));

        Table table = new Table(6);
        table.setWidth(500);
        table.addCell(new Paragraph("Cuotas").setBold().setFont(font).setTextAlignment(TextAlignment.CENTER));
        table.addCell(new Paragraph("Saldo Inicial").setBold().setFont(font).setTextAlignment(TextAlignment.CENTER));
        table.addCell(new Paragraph("Cuota Fija").setBold().setFont(font).setTextAlignment(TextAlignment.CENTER));
        table.addCell(new Paragraph("Interes").setBold().setFont(font).setTextAlignment(TextAlignment.CENTER));
        table.addCell(new Paragraph("Abono a Capital").setBold().setFont(font).setTextAlignment(TextAlignment.CENTER));
        table.addCell(new Paragraph("Saldo Final").setBold().setFont(font).setTextAlignment(TextAlignment.CENTER));
        for (int i = 0; i < amortizacions.size(); i++) {
            System.out.println("Estoy agregando al PDF ////////////////////////////////////////");
            table.addCell(String.valueOf(i+1)).setTextAlignment(TextAlignment.CENTER);
            table.addCell(String.valueOf(amortizacions.get(i).getSaldoInicial())).setTextAlignment(TextAlignment.CENTER);
            table.addCell(String.valueOf(amortizacions.get(i).getCuotaFija())).setTextAlignment(TextAlignment.CENTER);
            table.addCell(String.valueOf(amortizacions.get(i).getInteres())).setTextAlignment(TextAlignment.CENTER);
            table.addCell(String.valueOf(amortizacions.get(i).getAbonoCapital())).setTextAlignment(TextAlignment.CENTER);
            table.addCell(String.valueOf(amortizacions.get(i).getSaldoFinal())).setTextAlignment(TextAlignment.CENTER);
        }
        documento.add(table);
        documento.close();
        //aprobarCredito(cliente.getCorreo());
    }
    

}
