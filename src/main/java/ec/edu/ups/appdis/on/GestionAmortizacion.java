package ec.edu.ups.appdis.on;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.appdis.dao.AmortizacionDAO;
import ec.edu.ups.appdis.modelo.Amortizacion;
import ec.edu.ups.appdis.modelo.Credito;
import ec.edu.ups.appdis.modelo.Mensaje;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase GestionAmortizacion objeto de negocio para gestinar todo lo referente a
 * tabla de amortizacion del cliente de la cooperativa
 *
 * @author Gustavo Guallpa
 * @author Ismael Castillo
 * @author Jorge Arévalo
 * @version 1.0
 * @since 30/07/2020
 */
@Stateless
public class GestionAmortizacion {

    @Inject
    AmortizacionDAO amortizacionDAO;

    @Inject
    GestionTransaccionalLocal gestionTransaccionalLocal;

    @Inject
    GestionCreditos gestionCredito;

    private double auxsaldoInicial;
    private double auxsaldoFinal;
    private double auxCuotaFija;

    public void guardarAmortizacion(Amortizacion amortizacion) throws Exception {
        Amortizacion a = amortizacionDAO.read(amortizacion.getId());
        if (a != null) {
            amortizacionDAO.update(amortizacion);
        } else {
            amortizacionDAO.insert(amortizacion);
        }
    }

    public List<Amortizacion> listarAmortizacions() throws Exception {
        return amortizacionDAO.selectAll();
    }

    public void eliminarAmortizacion(int id) throws Exception {
        amortizacionDAO.delete(id);
    }

    public Amortizacion buscarAmortizacion(int id) throws Exception {
        return amortizacionDAO.read(id);
    }

    public List<Amortizacion> buscarAmortizacionLike(String numeroCredito) throws Exception {
        return amortizacionDAO.selectLike(numeroCredito);
    }

    /**
     * *
     * Metodo que permite generar la tabla de amortizacion
     *
     * @param monto de credto
     * @param plazos del credito
     * @param pro hacer referencia al proposito del credito.
     * @return List<Amortizacion> de la tabla de amortizacion
     */
    public List<Amortizacion> generarTablaAmortizacion(double monto, int plazos, String pro) {
        System.out.println("SU PROPOSITO ES" + pro);
        List<Amortizacion> tablaAmortizacions = new ArrayList<>();
        ///System.out.println("VALORES  A CALCULAR " + monto + " " + plazos);
        double interes = calcularInteres(pro);
        System.out.println("SU INTERES ES DE " + interes);
        Date date = new Date();
        List<Date> fechaPagos = new ArrayList<>();
        fechaPagos = generarFecha(plazos);
        Double pagomensual = monto / plazos;
        //Double cuota = redondearDecimales(pagomensual, 2);
        //FORMULA
        //A = VP ((i(1 + i)^n) / ((1 + i)^n – 1))
        //VP=ANUALIDAD
        //i=TADA DE INTERES
        //n=NUMERO DE PERIODOS O PLAZO
        //10000((2.0(1 + 0.2)^12) / ((1 +2.0)^12 – 1))
        Double cuota = redondearDecimales((monto * ((interes * (Math.pow((1 + interes), plazos))) / ((Math.pow((1 + interes), plazos)) - 1))), 2);

        for (int i = 0; i < plazos; i++) {
            if (i == 0) {
                Amortizacion amortizacion = new Amortizacion();
                //PERIODOS
                amortizacion.setnPeriodos(i + 1);
                //SALDO INICIAL 
                amortizacion.setSaldoInicial(monto);
                //CUOTaA FIJA
                amortizacion.setCuotaFija(cuota);
                //INTERES
                double interesgenerado = redondearDecimales(interes * monto, 2);
                amortizacion.setInteres(interesgenerado);

                //ABONO CAPITAL
                double abonoCapital = redondearDecimales(cuota - interesgenerado, 2);
                amortizacion.setAbonoCapital(abonoCapital);

                //SALFDO FINAL
                amortizacion.setSaldoFinal(monto - abonoCapital);
                //amortizacion.setCredito(credito);
                //ESATDO
                amortizacion.setEstadoCuota("PENDIENTE");
                //FECHA
                amortizacion.setFechaPago(fechaPagos.get(i));
                amortizacion.setSaldoCuota(cuota);
                tablaAmortizacions.add(amortizacion);

                //VALORES A ACTUALIZARSE
                auxsaldoInicial = redondearDecimales(monto - abonoCapital, 2);
                auxsaldoFinal = redondearDecimales(monto - abonoCapital, 2);

            } else {
                if (i == plazos - 1) {
                    if (auxsaldoInicial > cuota) {
                        double nuevaCuota = redondearDecimales(auxsaldoInicial - cuota, 2);
                        cuota = redondearDecimales(cuota + nuevaCuota, 2);

                    }
                    if (cuota > auxsaldoInicial) {
                        ///double nuevaCuota = redondearDecimales(cuota - auxsaldoInicial, 2);
                        //cuota = redondearDecimales(cuota - nuevaCuota, 2);
                        Amortizacion amortizacion = new Amortizacion();
                        //PERIODO
                        amortizacion.setnPeriodos(i + 1);
                        //SALDO INICIAL 
                        amortizacion.setSaldoInicial(auxsaldoInicial);
                        //CUOTA
                        amortizacion.setCuotaFija(cuota);
                        //INTERES
                        double interesgenerado = redondearDecimales(auxsaldoInicial * interes, 2);
                        amortizacion.setInteres(interesgenerado);
                        //ABONO CAPITAL

                        double abonoCapital = redondearDecimales(auxsaldoInicial, 2);
                        amortizacion.setAbonoCapital(abonoCapital);

                        //SALDO FINAL
                        double saldo = redondearDecimales(auxsaldoInicial - abonoCapital, 2);
                        amortizacion.setSaldoFinal(saldo);
                        //amortizacion.setCredito(credito);
                        //ESTADO
                        amortizacion.setEstadoCuota("PENDIENTE");
                        //FECHA
                        amortizacion.setFechaPago(fechaPagos.get(i));
                        amortizacion.setSaldoCuota(cuota);
                        tablaAmortizacions.add(amortizacion);

                    }
                } else {
                    Amortizacion amortizacion = new Amortizacion();
                    //PERIODO
                    amortizacion.setnPeriodos(i + 1);
                    //SALDO INICIAL 
                    amortizacion.setSaldoInicial(auxsaldoInicial);
                    //CUOTA
                    amortizacion.setCuotaFija(cuota);
                    //INTERES
                    double interesgenerado = redondearDecimales(auxsaldoInicial * interes, 2);
                    amortizacion.setInteres(interesgenerado);
                    //ABONO CAPITAL
                    double abonoCapital = redondearDecimales(cuota - interesgenerado, 2);
                    amortizacion.setAbonoCapital(abonoCapital);

                    //SALDO FINAL
                    double saldo = redondearDecimales(auxsaldoInicial - abonoCapital, 2);
                    amortizacion.setSaldoFinal(saldo);
                    //amortizacion.setCredito(credito);
                    //ESTADO
                    amortizacion.setEstadoCuota("PENDIENTE");
                    //FECHA
                    amortizacion.setFechaPago(fechaPagos.get(i));
                    amortizacion.setSaldoCuota(cuota);
                    tablaAmortizacions.add(amortizacion);
                    auxsaldoInicial = redondearDecimales(saldo, 2);
                }
            }

        }

        return tablaAmortizacions;

    }

    /**
     * *
     * Metodo para redonder decimales
     *
     * @param valorInicial representa la cantidad a redondear.
     * @param numeroDecimales que se quiere tener
     * @return
     */
    public static double redondearDecimales(double valorInicial, int numeroDecimales) {
        double parteEntera, resultado;
        resultado = valorInicial;
        parteEntera = Math.floor(resultado);
        resultado = (resultado - parteEntera) * Math.pow(10, numeroDecimales);
        resultado = Math.round(resultado);
        resultado = (resultado / Math.pow(10, numeroDecimales)) + parteEntera;
        return resultado;
    }

    /**
     * *
     * Metodo para poder generar la fecha de pago de cada una de las cuotas
     *
     * @param plazos para en base a ello generar las fechas
     * @return List<Date> listado de las fechas de pago de la cuotas de la tabla
     * de amortizacion
     */
    public List<Date> generarFecha(int plazos) {
        String auxFechaActual = null;
        List<String> fechas = new ArrayList<>();
        List<Date> fechaPagos = new ArrayList<>();
        for (int i = 0; i < plazos; i++) {
            if (i == 0) {
                String fechaActual = obtenerFecha();
                LocalDate date = LocalDate.parse(fechaActual);
                LocalDate newDate = date.plusMonths(1);
                String convertir = String.valueOf(newDate);
                fechas.add(convertir);
                auxFechaActual = convertir;
            } else {
                //System.out.println("soy aux " + auxFechaActual);
                LocalDate date = LocalDate.parse(auxFechaActual);
                LocalDate newDate = date.plusMonths(1);
                String convertir = String.valueOf(newDate);
                fechas.add(convertir);
                auxFechaActual = convertir;
            }
        }
        //System.out.println("SOY LA FECHA");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        for (String string : fechas) {
            try {
                Date date = dateFormat.parse(string);
                //System.out.println("SOY FECHA DE TIPO DATE ------------->>>" + date);
                fechaPagos.add(date);
            } catch (ParseException ex) {
                Logger.getLogger(GestionAmortizacion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return fechaPagos;

    }

    /**
     * Metodo para obtener la fecha actual
     *
     * @return fecha actual
     */
    public String obtenerFecha() {
        Date feInicio;
        Calendar ca = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        feInicio = ca.getTime();
        String fechaInicial = dateFormat.format(feInicio);
        return fechaInicial;
    }

    /**
     * Metodo que permite asignar el interes respectivo a cada credito , toodo
     * ello dependiendo del proposito del mismo.
     *
     * @param proposito del credito.
     * @return cantidan que se le asignara al credito
     */
    public double calcularInteres(String proposito) {

        double interes = 0.00;
        if (proposito.equalsIgnoreCase("Muebles/Equipamiento") || proposito.equalsIgnoreCase("Automóvil") || proposito.equalsIgnoreCase("Electrodomésticos") || proposito.equalsIgnoreCase("Vacaciones")) {
            //CREDITO DE CONSUMO
            interes = 0.016;
        }
        if (proposito.equalsIgnoreCase("Inmuebles (Casa, Finca, etc)")) {
            //CREDITO HIPOTECARIO
            interes = 0.016;
        }
        if (proposito.equalsIgnoreCase("Negocios") || proposito.equalsIgnoreCase("Tecnología")) {
            //CREDITO COMERCIAL
            interes = 0.013;
        }
        if (proposito.equalsIgnoreCase("Capacitación") || proposito.equalsIgnoreCase("Educación")) {
            //CREDITO EDUCATIVO
            interes = 0.012;
        }
        if (proposito.equalsIgnoreCase("Reparaciones")) {
            //CREDITO DE VIVIENDA
            interes = 0.016;
        }
        if (proposito.equalsIgnoreCase("Otros")) {
            //CREDITO EMERGENTE
            interes = 0.011;
        }
        return interes;
    }

    public List<Amortizacion> mostrarAmortizacionCliente(int numeroCredito) {
        try {
            return amortizacionDAO.mostrarAmortizacionCliente(numeroCredito);
        } catch (Exception e) {
            throw e;
        }

    }

    public List<Amortizacion> mostrarAmortizacionClientePendientes(int numeroCredito) {
        try {
            return amortizacionDAO.mostrarAmortizacionClientePendientes(numeroCredito);
        } catch (Exception e) {
            throw e;
        }

    }

    public List<Amortizacion> mostrarAmortizacionClienteVencidas(int numeroCredito) {
        try {
            return amortizacionDAO.mostrarAmortizacionClienteVencidas(numeroCredito);
        } catch (Exception e) {
            throw e;
        }

    }

    /**
     * Metodo para saber la cuota que le corresponde pagar al cliente
     *
     * @param numeroCredito
     * @return Amortizacion
     */
    public Amortizacion obtenerUltimaCuoa(int numeroCredito) {

        List<Amortizacion> amortizacion = amortizacionDAO.mostrarAmortizacionClienteDes(numeroCredito);
        for (int i = 0; i < amortizacion.size(); i++) {
            if (i == amortizacion.size() - 1) {
                return amortizacion.get(i);
            }
        }
        return null;
    }

//    public Amortizacion obtenerPenultimaCuoa(int numeroCredito) {
//        
//        List<Amortizacion> amortizacion = amortizacionDAO.mostrarAmortizacionClienteDes(numeroCredito);
//        for (int i = 0; i < amortizacion.size(); i++) {
//            if (i == amortizacion.size() - 2) {
//                return amortizacion.get(i);
//            }
//        }
//        return null;
//    }
    /**
     * Método para pagar na cuota de un cliente que se mantiene al dia con sus
     * pagos.
     *
     * @param idAmortizacion
     * @param cantidad
     * @param numeroCuenta
     * @return Mensaje
     */
    public Mensaje pagarCuota(int idAmortizacion, double cantidad, String numeroCuenta) {

        try {
            Amortizacion m = amortizacionDAO.read(idAmortizacion);
            if (m.getCuotaFija() == m.getSaldoCuota()) {
                int paso = validarFecha(m);
                if (paso == 0) {
                    return new Mensaje(6, "LA CUOTA QUE DESEA CANCELAR CORRESPONDE AL SIGUIENTE MES ");
                }
                if (cantidad > m.getCuotaFija()) {
                    return new Mensaje(50, "LA CANTIDAD INGRESADA NO ES EQUIVALETE AL MONTO DE SU CUOTA A PAGAR ");
                }
                if (cantidad < m.getCuotaFija()) {
                    return new Mensaje(51, "LA CANTIDAD INGRESADA NO ES EQUIVALETE AL MONTO DE SU CUOTA A PAGAR ");
                } else {
                    //DESCONTAMOS DEINERO DEL CLIENTE
                    Mensaje mensaje = gestionTransaccionalLocal.saveTransactionWithdrawal(numeroCuenta, cantidad);
                    if (mensaje.getCodigo() == 1) {
                        //PRIEMRO ACTUALIZAMOS EL ESTADO DE LA CUOTA
                        m.setEstadoCuota("PAGADA");
                        m.setSaldoCuota(m.getCuotaFija() - cantidad);
                        amortizacionDAO.update(m);

                        //AQUI DEBEMOS ACTUALIZAR EL ESTAO DEL LA FECHA DEL CREDITO DEL CLIENTE
                        Credito c = gestionCredito.buscarCredito(m.getCredito().getNumeroCredito());

                        Amortizacion am = obtenerUltimaCuoa(m.getCredito().getNumeroCredito());
                        c.setFechaVencimiento(am.getFechaPago());

                        c.setSaldoCredito(m.getSaldoFinal());

                        gestionCredito.actualizarCredito(c);

                        return mensaje;
                    } else {
                        return mensaje;
                    }

                }

            } else {
                return new Mensaje(52, "NO SE PUEDE COBRAR LA CUOTA YA QUE SU PAGO SE ESTAN REALIZANDO POR ABONOS");
            }

        } catch (Exception e) {
        }

        return null;
    }
    /***
     * Metodo que permite realizar un abono de dinero a la cuota que le corresponde.
     * @param idAmortizacion  cuota a buscar para pagar
     * @param cantidad que se va a abonar
     * @param numeroCuenta  de ahorros para poder verificar si el cliente posee dinero.
     * @return Mensaje si se poudo  o no commpletar la operacion
     */

    public Mensaje abonarCuota(int idAmortizacion, double cantidad, String numeroCuenta) {

        try {
            Amortizacion m = amortizacionDAO.read(idAmortizacion);

            int paso = validarFecha(m);
            if (paso == 0) {
                return new Mensaje(6, "EL ABONO QUE DESEA REALIZAR PUEDE HACER EL SIGUIENTE MES ");
            }
            if (cantidad > m.getSaldoCuota()) {
                return new Mensaje(50, "LA CANTIDAD INGRESADA NO ES EQUIVALETE AL SALDO A PAGAR");
            }
            if (cantidad <= 0) {
                return new Mensaje(51, "LA CANTIDAD A ABONAR DEBE SER MAYOR A CERO ");
            } else {
                //DESCONTAMOS DEINERO DEL CLIENTE
                Mensaje mensaje = gestionTransaccionalLocal.saveTransactionWithdrawal(numeroCuenta, cantidad);
                if (mensaje.getCodigo() == 1) {
                    //PRIEMRO ACTUALIZAMOS EL ESTADO DE LA CUOTA
                    m.setSaldoCuota(redondearDecimales(m.getSaldoCuota() - cantidad, 2));
                    amortizacionDAO.update(m);

                    if (m.getSaldoCuota() == 0) {

                        m.setEstadoCuota("PAGADA");
                        m.setSaldoCuota(0);
                        amortizacionDAO.update(m);
                        //AQUI DEBEMOS ACTUALIZAR EL ESTAO DEL LA FECHA DEL CREDITO DEL CLIENTE
                        Credito c = gestionCredito.buscarCredito(m.getCredito().getNumeroCredito());

                        Amortizacion am = obtenerUltimaCuoa(m.getCredito().getNumeroCredito());
                        c.setFechaVencimiento(am.getFechaPago());

                        c.setSaldoCredito(m.getSaldoFinal());

                        gestionCredito.actualizarCredito(c);

                    }

                    return mensaje;
                } else {
                    return mensaje;
                }

            }

        } catch (Exception e) {
        }

        return null;
    }
    /***
     * Metodo que valida que se pueda hacer ya sea un abono  o pago de cuotas dentro de la fecha  que corresponde.
     * @param amortizacion
     * @return un numero
     */
    public int validarFecha(Amortizacion amortizacion) {
        try {
            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String fecha = dateFormat.format(date);
            //Date fechActual;
            Date fechActual = dateFormat.parse(fecha);

            int m1 = fechActual.getMonth();
            int m2 = amortizacion.getFechaPago().getMonth();
            System.out.println("m1 " + m1);
            System.out.println("m2 " + m2);

            if ((m2 - m1) == 0 || (m2 - m1) == 1) {
                return 1;
            } else {
                return 0;
            }

        } catch (Exception e) {
        }
        return 0;
    }

    /**
     * Conocer la cantida de cuotas pendientes del cliente.
     * @param numeroCredito
     * @return numeor de cuotas pendientes.
     * @throws ParseException 
     */
    public int numeroCuotasPendientes(int numeroCredito) throws ParseException {

        int numPendiente = 0;
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String fecha = dateFormat.format(date);
        //Date fechActual;
        Date fechActual = dateFormat.parse(fecha);

        List<Amortizacion> amortizacion = amortizacionDAO.mostrarAmortizacionClienteDes(numeroCredito);
        System.out.println("HOLA");
        for (Amortizacion am : amortizacion) {
            System.out.println("HOLA 1");
            System.out.println(am.getFechaPago());
            String fecha1 = dateFormat.format(am.getFechaPago());
            //Date fechActual;
            Date fe = dateFormat.parse(fecha1);
            if (fe.before(fechActual)) {
                System.out.println("SI HAY RETRASOS");
                numPendiente = numPendiente + 1;
            }
            System.out.println("NO");
        }
        return numPendiente;

    }

}
