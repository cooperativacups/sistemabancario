/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.on;

import ec.edu.ups.appdis.dao.EmpleadoDAO;
import ec.edu.ups.appdis.modelo.Empleado;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * En esta Clase Define el CRUD para la tabla Empleado
 * @author Ismael Castillo
 * @version 08/06/2020
 */
@Stateless
public class GestionEmpleado {
    
    @Inject
    EmpleadoDAO empleadoDAO;
    
    public void guardarEmpleado(Empleado empleado) throws Exception {
        Empleado e = empleadoDAO.read(empleado.getCedula());
        if(e != null){
            empleadoDAO.update(empleado);
        }else{           
            empleadoDAO.insert(empleado);          
        }
    }
   
    public List<Empleado> listarEmpleados() throws Exception {
        return empleadoDAO.selectAll();
    }
   
    public void eliminarEmpleado(String cedula) throws Exception {
        empleadoDAO.delete(cedula);
    }
   
    public Empleado buscarEmpleado(String cedula) throws Exception {
        return empleadoDAO.read(cedula);
    }
       
    public List<Empleado> buscarEmpleadoLike(String cedula) throws Exception {
        List<Empleado> empleados = empleadoDAO.selectLike(cedula);
        return empleados;
    }
    
}
