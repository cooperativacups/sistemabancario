/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.on;

import ec.edu.ups.appdis.dao.ClienteDAO;
import ec.edu.ups.appdis.dao.ClientePaginaDAO;
import ec.edu.ups.appdis.dao.TransaccionDAO;
import ec.edu.ups.appdis.modelo.Cliente;
import ec.edu.ups.appdis.modelo.CuentaAhorro;
import ec.edu.ups.appdis.modelo.Mensaje;
import ec.edu.ups.appdis.modelo.Transaccion;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * En esta Clase Define el CRUD para la Transaccion
 *
 * @author Jorge Arévalo
 * @version 08/06/2020
 */
@Stateless
public class GestionTransaccional implements GestionTransaccionalLocal {

    @Inject
    private GestionPaginaClienteLocal gpcl;

    @Inject
    private TransaccionDAO tDAO;

    @Override
    public Mensaje saveTransactionDeposit(String numeroCuenta, double monto) throws Exception {

        
        CuentaAhorro cuentaAhorro = gpcl.searchSavingsAccount(numeroCuenta);
        if (cuentaAhorro != null) {
            Transaccion transaccion = new Transaccion();
            Date date = new Date();
            //fecha
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            String fecha = dateFormat.format(date);
            transaccion.setFecha(fecha);
            //Hora
            DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
            String hora = hourFormat.format(date);
            transaccion.setHora(hora);
            transaccion.setMonto(monto);
            transaccion.setTipo("DEPOSITO");
            transaccion.setCuentaAhorro(cuentaAhorro);

            if (monto <= 0) {
                return new Mensaje(3, "EL MONTO A DEPOSITAR DEBE SER MAYOR A CERO ");
            } else {
                tDAO.insertTransaction(transaccion);
                //Actualizar con el nuevo saldo
                double saldoNuevo = cuentaAhorro.getSaldo() + transaccion.getMonto();
                cuentaAhorro.setSaldo(saldoNuevo);
                gpcl.updateBalance(cuentaAhorro);
                return new Mensaje(1, "TRANSACCION EXITOSA");
            }

        } else {
            return new Mensaje(0, "LA CUENTA DE AHORROS INGRESADA NO EXISTE");
        }

    }

    @Override
    public Mensaje saveTransactionWithdrawal(String numeroCuenta, double monto) throws Exception {

        CuentaAhorro cuentaAhorro = gpcl.searchSavingsAccount(numeroCuenta);
        if (cuentaAhorro != null) {
            double saldo = cuentaAhorro.getSaldo();
            Transaccion transaccion = new Transaccion();
            Date date = new Date();
            //fecha
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            String fecha = dateFormat.format(date);
            transaccion.setFecha(fecha);
            //Hora
            DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
            String hora = hourFormat.format(date);
            transaccion.setHora(hora);
            transaccion.setMonto(monto);
            transaccion.setTipo("RETIRO");
            transaccion.setCuentaAhorro(cuentaAhorro);
            if (monto > saldo) {
                return new Mensaje(2, "CANTIDAD DE DINERO NO DISPONIBLE");
            }
            if (monto <= 0) {
                return new Mensaje(3, "EL MONTO A RETIRAR DEBE SER MAYOR A CERO");
            } else {
                tDAO.insertTransaction(transaccion);
                double saldoNuevo = saldo - transaccion.getMonto();
                cuentaAhorro.setSaldo(saldoNuevo);
                gpcl.updateBalance(cuentaAhorro);
                return new Mensaje(1, "TRANSACCION EXITOSA");
            }
        } else {
            return new Mensaje(0, "LA CUENTA DE AHORROS INGRESADA NO EXISTE");
        }
    }

    @Override
    public Mensaje guardarTranferencia(String cuentaOrige, String cuentaDestino, double monto) throws Exception {

        CuentaAhorro cuentaAhorro = gpcl.searchSavingsAccount(cuentaOrige);
        CuentaAhorro cuentaAhorro1 = gpcl.searchSavingsAccount(cuentaDestino);
        if (cuentaAhorro == null) {
            return new Mensaje(0, "LA CUENTA DE AHORROS DE ORIGEN NO ES CORRECTA ");
        }
        if (cuentaAhorro1 == null) {
            return new Mensaje(0, "LA CUENTA DE AHORROS DE DESTINO NO ES CORRECTA");
        }
        if (monto <= 0) {
            return new Mensaje(3, "EL MONTO A TRANSFERIR DEBE SER MAYOR A CERO ");
        }
        if (cuentaAhorro.getSaldo() < monto) {
            return new Mensaje(2, "NO SE PUEDE REALIZAR LA TRANSFERENCIA SALDO NO DISPONBLE");
        }
        if (cuentaAhorro != null && cuentaAhorro1 != null && monto > 0 && cuentaAhorro.getSaldo() >= monto) {
            //RETIRO A LA CUENTA ORIGEN
            ///double saldo = cuentaAhorro.getSaldo();
            Transaccion transaccion = new Transaccion();
            Date date = new Date();
            //fecha
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            String fecha = dateFormat.format(date);
            transaccion.setFecha(fecha);
            //Hora
            DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
            String hora = hourFormat.format(date);
            transaccion.setHora(hora);
            transaccion.setMonto(monto);
            transaccion.setTipo("RETIRO");
            transaccion.setCuentaAhorro(cuentaAhorro);
            tDAO.insertTransaction(transaccion);
            double saldoNuevo = cuentaAhorro.getSaldo() - monto;
            cuentaAhorro.setSaldo(saldoNuevo);
            gpcl.updateBalance(cuentaAhorro);

            //DEPOSITO A LA CUENTA DESTINO
            Transaccion transaccion1 = new Transaccion();
            transaccion1.setFecha(fecha);;
            transaccion1.setHora(hora);
            transaccion1.setMonto(monto);
            transaccion1.setTipo("DEPOSITO");
            transaccion1.setCuentaAhorro(cuentaAhorro1);
            tDAO.insertTransaction(transaccion1);
            double saldoNuevo1 = cuentaAhorro1.getSaldo() + monto;
            cuentaAhorro1.setSaldo(saldoNuevo1);
            gpcl.updateBalance(cuentaAhorro1);
            return new Mensaje(1, "TRANSFERENCIA REALIZADA CON ÉXITO");

        } else {
            return new Mensaje(4, "NO SE PUDO REALIZAR SU TRANFERECNIA");
        }

    }

    @Override
    public List<Transaccion> obtenerTransacciones(String numeroCuenta, String concepto, String fechaInicial, String fechaFinal) throws Exception {
        return tDAO.getListaTransaccionesFechas(numeroCuenta, concepto, fechaInicial, fechaFinal);

    }

    @Override
    public List<Transaccion> getTransaccions() {
        return tDAO.listar();
    }

    @Override
    public List<Transaccion> obtenerUltimasTransacciones(String numeroCuenta, String fechaInicial, String fechaFinal) throws Exception {

        return tDAO.getListaUlimasTransaccionesFechas(numeroCuenta, fechaInicial, fechaFinal);

    }
    
    @Override
    public Mensaje guardarTransferenciaInterBancaria(String cuentaDestino, double monto) throws Exception {
        //CuentaAhorro cuentaAhorro = gpcl.searchSavingsAccount(cuentaOrige);
        CuentaAhorro cuentaAhorro1 = gpcl.searchSavingsAccount(cuentaDestino);

        if (cuentaAhorro1 == null) {
            return new Mensaje(0, "LA CUENTA DE AHORROS DE DESTINO NO ES CORRECTA");
        }
        if (monto <= 0) {
            return new Mensaje(3, "EL MONTO A TRANSFERIR DEBE SER MAYOR A CERO ");
        }
        if (cuentaAhorro1 != null && monto > 0) {
            Transaccion transaccion = new Transaccion();
            Date date = new Date();
            //fecha
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            String fecha = dateFormat.format(date);
            transaccion.setFecha(fecha);
            //Hora
            DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
            String hora = hourFormat.format(date);
            //DEPOSITO A LA CUENTA DESTINO
            Transaccion transaccion1 = new Transaccion();
            transaccion1.setFecha(fecha);;
            transaccion1.setHora(hora);
            transaccion1.setMonto(monto);
            transaccion1.setTipo("DEPOSITO");
            transaccion1.setCuentaAhorro(cuentaAhorro1);
            tDAO.insertTransaction(transaccion1);
            double saldoNuevo1 = cuentaAhorro1.getSaldo() + monto;
            cuentaAhorro1.setSaldo(saldoNuevo1);
            gpcl.updateBalance(cuentaAhorro1);
            return new Mensaje(1, "TRANSFERENCIA REALIZADA CON ÉXITO");

        } else {
            return new Mensaje(4, "NO SE PUDO REALIZAR SU TRANFERECNIA");
        }
    }

}
