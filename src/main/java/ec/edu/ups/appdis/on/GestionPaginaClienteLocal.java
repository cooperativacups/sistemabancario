/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.on;

import ec.edu.ups.appdis.modelo.Acceso;
import ec.edu.ups.appdis.modelo.Cliente;
import ec.edu.ups.appdis.modelo.CuentaAhorro;
import ec.edu.ups.appdis.modelo.Transaccion;
import java.util.List;
import javax.ejb.Local;

/**
 * En esta Clase es de manera local y tenemos los metodos 
 * @author Jorge Arévalo, Gustavo Guallpa
 * @version 08/06/2020
 */
@Local
public interface GestionPaginaClienteLocal {
    
    public CuentaAhorro buscaCuentaAhorro(String numCuenta) throws Exception;

    public Transaccion buscaTransaccion(String codigo) throws Exception;

    public Cliente buscaCliente(String numCuenta) throws Exception;

    public List<Cliente> listarPersonas();

    public List<CuentaAhorro> listarCuentas();

    public List<Transaccion> listarTransacciones();
    
    public CuentaAhorro searchSavingsAccount(String numeroCuenta);
    
    
    public CuentaAhorro searchSavingsAccountCedula(String cedula);
    
    public List<Acceso> getCorrectAccesses(String cedula);
            
    public List<Acceso> getIncorrectAccesses(String cedula);
    
    public void updateBalance(CuentaAhorro cuentaAhorro);
    
    public String getLastDateTransacction(String cuenta);
}
