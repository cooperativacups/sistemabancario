package ec.edu.ups.appdis.on;

import ec.edu.ups.appdis.modelo.Cliente;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.ejb.Stateless;
import javax.mail.*;
import javax.mail.internet.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

@Stateless
public class GestionCorreo {

    public void generarCorreoSinArchivosAdjuntos(String correoDestinoReceptado, String asuntoReceptado, String cuerpoReceptado) throws MessagingException {
        try {
            Properties props = new Properties();
            props.setProperty("mail.smtp.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.auth", "true");
            Session session = Session.getDefaultInstance(props);
            session.setDebug(true);
            String correoRemitente = "cupsvirtual@gmail.com";
            String passwordRemitente = "@AdminCups";
            String correoDestino = correoDestinoReceptado;
            String asunto = asuntoReceptado;
            String cuerpo = cuerpoReceptado;
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(correoRemitente));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(correoDestino));
            message.setSubject(asunto);
            message.setText(cuerpo, "ISO-8859-1", "html");
            Transport t = session.getTransport("smtp");
            t.connect(correoRemitente, passwordRemitente);
            t.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
            t.close();
        } catch (AddressException ex) {
            System.out.println("Error en el AddressException de generarCorreoSinArchivosAdjuntos" + ex.getMessage());
        } catch (MessagingException ex) {
            System.out.println("Error en el MessagingException de generarCorreoSinArchivosAdjuntos" + ex.getMessage());
        }
    }

    public void generarCorreoConArchivosAdjuntos(String correoDestinoReceptado, String asuntoReceptado, String cuerpoReceptado, String textoInformativo, String rutaArchivo, String nombreArchivo) throws MessagingException {
        try {
            Properties props = new Properties();
            props.setProperty("mail.smtp.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.auth", "true");
            Session session = Session.getDefaultInstance(props);
            session.setDebug(true);
            String correoRemitente = "cupsvirtual@gmail.com";
            String passwordRemitente = "@AdminCups";
            String correoDestino = correoDestinoReceptado;
            String asunto = asuntoReceptado;
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(correoRemitente));
            MimeBodyPart textPart = new MimeBodyPart();
            textPart.setText( cuerpoReceptado, "ISO-8859-1", "html");
            BodyPart texto = new MimeBodyPart();
            texto.setText(textoInformativo);
            BodyPart adjunto = new MimeBodyPart();
            adjunto.setDataHandler(new DataHandler(new FileDataSource(rutaArchivo)));
            adjunto.setFileName(nombreArchivo + ".pdf");
            MimeMultipart multiParte = new MimeMultipart();
            multiParte.addBodyPart(texto);
            multiParte.addBodyPart(adjunto);
            multiParte.addBodyPart(textPart);
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(correoDestino));
            message.setSubject(asunto);
            message.setContent(multiParte);
            Transport t = session.getTransport("smtp");
            t.connect(correoRemitente, passwordRemitente);
            t.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
            t.close();
        } catch (AddressException ex) {
            System.out.println("Error en el AddressException de generarCorreoConArchivosAdjuntos" + ex.getMessage());
        } catch (MessagingException ex) {
            System.out.println("Error en el MessagingException de generarCorreoConArchivosAdjuntos" + ex.getMessage());
        }
    }

    public void notificacionCorreo(Cliente cliente, String tipoCorreo) throws MessagingException {
        String asunto = null;
        String cuerpo = null;
        //Hora
        Date date = new Date();
        DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
        String hora = hourFormat.format(date);
        //fecha
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String fecha = dateFormat.format(date);
        if (cliente != null) {
            switch (tipoCorreo) {
                case "AperturaCuenta":
                     asunto = "Apertura de cuenta de ahorros en la Cooperativa CUPS";
                     cuerpo = "<html>" +
                                    "<head>" +
                                    "<style>" +
                                    "        #heading {" +
                                    "            color: #F1c232;" +
                                    "            text-align: center;" +
                                    "        }" +
                                    "" +
                                    "        h1 {" +
                                    "            color: black;" +
                                    "            text-align: center;" +
                                    "        }" +
                                    "" +
                                    "        h2 {" +
                                    "            color: black;" +
                                    "            text-align: center;" +
                                    "        }" +
                                    "" +
                                    "        p {" +
                                    "            color: black;" +
                                    "            text-align: center;" +
                                    "        }" +
                                    "" +
                                    "        header {" +
                                    "            background-color: #073763;" +
                                    "        }" +
                                    "" +
                                    "        footer {" +
                                    "            background-color: #073763;" +
                                    "            margin-top: -30px;" +
                                    "            padding-top: 50px;" +
                                    "            padding-bottom: 50px;" +
                                    "            font: 1em arial, helvetica, sans-serif;" +
                                    "        }" +
                                    "" +
                                    "        #contenedor {" +
                                    "            width: 100%;" +

                                    "            font: 2em arial, helvetica, sans-serif;" +
                                    "            background-color: #073763;" +
                                    "        }" +
                                    "" +
                                    "        #tam {" +
                                    "            margin-top: -40px;" +
                                    "            padding-top: 50px;" +
                                    "            padding-bottom: 50px;" +
                                    "            font: 1.5em arial, helvetica, sans-serif;" +
                                    "            width: 100%;" +
                                    "        }" +
                                    ".cuerpocorreo{" +
                                    "           color: #F1c232;" +
                                    "           background-color: #F1c232;" +
                                    "        }" +
                                    "</style>" +
                                    "<title>La Cooperativa CUPS le da la bienvenida por formar de esta gran familia de socios de la cooperativa.</title>" +
                                    "</head>" +
                                    "<body bgcolor='#F1c232'>" +
                                    "<div id='contenedor'>" +
                                    "            <h1 id='heading'> CUPS COOPERATIVA AHORRO Y CREDITO </h1>" +
                                    "            <h2 id='heading'>SISTEMA TRANSACCIONAL</h2>" +
                                    "</div>" +
                                    "<div id='tam' class='cuerpocorreo'> " +
                                    "<h2>APERTURA DE CUENTA</h2>"+
                                    "<p>Estimado(a):</p>" +
                                    "<h2> " + cliente.getNombre() + " " + cliente.getApellido() + "</h2>" +
                                    "<p><b>COOPERATIVA CUPS</b> le informa que sus credenciales para el acceso al sistema web transaccional son:</p>" +
                                    "<p>Usuario</p>" +
                                    "<h2> " + cliente.getUsuario() + "</h2>" +
                                    "<p>Contraseña</p>" +
                                    "<h2> " + cliente.getContrasena() + "</h2>" +
                                    "<p>Fecha:" + fecha + "&nbsp;&nbsp;" + "Hora:" + hora + "</p>" +
                                    "<p>Si ud. NO ha intentado acceder a nuestra plataforma , comuníquese con nuestro Call Center: <b>(593)0939279776</b></p>" +
                                    " </div>" +
                                    "<footer>" +
                                    " <p id='heading'> 2020 Cups. All Rights Reserved</p>" +
                                    " </footer>" +
                                    "</body>" +
                                    "</html>";
                    generarCorreoSinArchivosAdjuntos(cliente.getCorreo(), asunto, cuerpo);
                    break;
                case "AprobacionCredito":
                     asunto = "Aprobación de crédito en la Cooperativa CUPS";
                     cuerpo = "<html>" +
                                    "<head>" +
                                    "<style>" +
                                    "        #heading {" +
                                    "            color: #F1c232;" +
                                    "            text-align: center;" +
                                    "        }" +
                                    "" +
                                    "        h1 {" +
                                    "            color: black;" +
                                    "            text-align: center;" +
                                    "        }" +
                                    "" +
                                    "        h2 {" +
                                    "            color: black;" +
                                    "            text-align: center;" +
                                    "        }" +
                                    "" +
                                    "        p {" +
                                    "            color: black;" +
                                    "            text-align: center;" +
                                    "        }" +
                                    "" +
                                    "        header {" +
                                    "            background-color: #073763;" +
                                    "        }" +
                                    "" +
                                    "        footer {" +
                                    "            background-color: #073763;" +
                                    "            margin-top: -30px;" +
                                    "            padding-top: 50px;" +
                                    "            padding-bottom: 50px;" +
                                    "            font: 1em arial, helvetica, sans-serif;" +
                                    "        }" +
                                    "" +
                                    "        #contenedor {" +
                                    "            width: 100%;" +

                                    "            font: 2em arial, helvetica, sans-serif;" +
                                    "            background-color: #073763;" +
                                    "        }" +
                                    "" +
                                    "        #tam {" +
                                    "            margin-top: -40px;" +
                                    "            padding-top: 50px;" +
                                    "            padding-bottom: 50px;" +
                                    "            font: 1.5em arial, helvetica, sans-serif;" +
                                    "            width: 100%;" +
                                    "        }" +
                                    ".cuerpocorreo{" +
                                    "           color: #F1c232;" +
                                    "           background-color: #F1c232;" +
                                    "        }" +
                                    "</style>" +
                                    "<title>La Cooperativa CUPS le informa que su crédito fue aprobado.</title>" +
                                    "</head>" +
                                    "<body bgcolor='#F1c232'>" +
                                    "<div id='contenedor'>" +
                                    "            <h1 id='heading'> CUPS COOPERATIVA AHORRO Y CREDITO </h1>" +
                                    "            <h2 id='heading'>SISTEMA TRANSACCIONAL</h2>" +
                                    "</div>" +
                                    "<div id='tam' class='cuerpocorreo'> " +
                                    "<h2>APROBACIÓN DE CRÉDITO</h2>"+
                                    "<p>Estimado(a):</p>" +
                                    "<h2> " + cliente.getNombre() + " " + cliente.getApellido() + "</h2>" +
                                    "<p><b>COOPERATIVA CUPS</b> le informa que su crédito fue aprobado, el dinero se desembolsará a su cuenta de ahorros al momento que confirme desde su cuenta que está de acuerdo con pagar las cuotas que presenta la tabla de amortización adjunta.</p>" +
                                    "<p>Fecha:" + fecha + "&nbsp;&nbsp;" + "Hora:" + hora + "</p>" +
                                    "<p>Ud mantiene un crédito vigente en la COOPERATIVA CUPS , para más información comuníquese con nuestro Call Center: <b>(593)0939279776</b></p>" +
                                    " </div>" +
                                    "<footer>" +
                                    " <p id='heading'> 2020 Cups. All Rights Reserved</p>" +
                                    " </footer>" +
                                    "</body>" +
                                    "</html>";
                    String textoInformativo = "APROBACION DE CREDITO - TABLA DE AMORTIZACION";
                    String rutaArchivo = "C:\\Users\\ggual\\Videos\\Amortizacion.pdf";
                    String nombreArchivo = "TablaAmortizacionCreditoCooperativaCups";
                    generarCorreoConArchivosAdjuntos(cliente.getCorreo(), asunto, cuerpo, textoInformativo, rutaArchivo, nombreArchivo);
                    break;
                 case "RechazarCredito":
                     asunto = "Rechazo de crédito en la Cooperativa CUPS";
                     cuerpo = "<html>" +
                                    "<head>" +
                                    "<style>" +
                                    "        #heading {" +
                                    "            color: #F1c232;" +
                                    "            text-align: center;" +
                                    "        }" +
                                    "" +
                                    "        h1 {" +
                                    "            color: black;" +
                                    "            text-align: center;" +
                                    "        }" +
                                    "" +
                                    "        h2 {" +
                                    "            color: black;" +
                                    "            text-align: center;" +
                                    "        }" +
                                    "" +
                                    "        p {" +
                                    "            color: black;" +
                                    "            text-align: center;" +
                                    "        }" +
                                    "" +
                                    "        header {" +
                                    "            background-color: #073763;" +
                                    "        }" +
                                    "" +
                                    "        footer {" +
                                    "            background-color: #073763;" +
                                    "            margin-top: -30px;" +
                                    "            padding-top: 50px;" +
                                    "            padding-bottom: 50px;" +
                                    "            font: 1em arial, helvetica, sans-serif;" +
                                    "        }" +
                                    "" +
                                    "        #contenedor {" +
                                    "            width: 100%;" +

                                    "            font: 2em arial, helvetica, sans-serif;" +
                                    "            background-color: #073763;" +
                                    "        }" +
                                    "" +
                                    "        #tam {" +
                                    "            margin-top: -40px;" +
                                    "            padding-top: 50px;" +
                                    "            padding-bottom: 50px;" +
                                    "            font: 1.5em arial, helvetica, sans-serif;" +
                                    "            width: 100%;" +
                                    "        }" +
                                    ".cuerpocorreo{" +
                                    "           color: #F1c232;" +
                                    "           background-color: #F1c232;" +
                                    "        }" +
                                    "</style>" +
                                    "<title>La Cooperativa CUPS le informa que su crédito fue rechazado.</title>" +
                                    "</head>" +
                                    "<body bgcolor='#F1c232'>" +
                                    "<div id='contenedor'>" +
                                    "            <h1 id='heading'> CUPS COOPERATIVA AHORRO Y CREDITO </h1>" +
                                    "            <h2 id='heading'>SISTEMA TRANSACCIONAL</h2>" +
                                    "</div>" +
                                    "<div id='tam' class='cuerpocorreo'> " +
                                    "<h2>RECHAZO DE CRÉDITO</h2>"+
                                    "<p>Estimado(a):</p>" +
                                    "<h2> " + cliente.getNombre() + " " + cliente.getApellido() + "</h2>" +
                                    "<p><b>COOPERATIVA CUPS</b> le informa que su crédito fue rechazado debido a que no cumple con los requesitos mínimos para la aprobación del mismo</p>" +
                                    "<p>Fecha:" + fecha + "&nbsp;&nbsp;" + "Hora:" + hora + "</p>" +
                                    "<p>Para más información comuníquese con nuestro Call Center: <b>(593)0939279776</b></p>" +
                                    " </div>" +
                                    "<footer>" +
                                    " <p id='heading'> 2020 Cups. All Rights Reserved</p>" +
                                    " </footer>" +
                                    "</body>" +
                                    "</html>";
                    //String textoInformativo = "APROBACION DE CREDITO - TABLA DE AMORTIZACION";
                    ///String rutaArchivo = "C:\\Users\\ggual\\Videos\\Amortizacion.pdf";
                    //String nombreArchivo = "TablaAmortizacionCreditoCooperativaCups";
                    generarCorreoSinArchivosAdjuntos(cliente.getCorreo(), asunto, cuerpo);
                    break;
                    
                case "AccesoCorrecto":
                     asunto = "Inicio Sesion";
                     cuerpo = "<html>" +
                                    "<head>" +
                                    "<style>" +
                                    "        #heading {" +
                                    "            color: #F1c232;" +
                                    "            text-align: center;" +
                                    "        }" +
                                    "" +
                                    "        h1 {" +
                                    "            color: black;" +
                                    "            text-align: center;" +
                                    "        }" +
                                    "" +
                                    "        h2 {" +
                                    "            color: black;" +
                                    "            text-align: center;" +
                                    "        }" +
                                    "" +
                                    "        p {" +
                                    "            color: black;" +
                                    "            text-align: center;" +
                                    "        }" +
                                    "" +
                                    "        header {" +
                                    "            background-color: #073763;" +
                                    "        }" +
                                    "" +
                                    "        footer {" +
                                    "            background-color: #073763;" +
                                    "            margin-top: -30px;" +
                                    "            padding-top: 50px;" +
                                    "            padding-bottom: 50px;" +
                                    "            font: 1em arial, helvetica, sans-serif;" +
                                    "        }" +
                                    "" +
                                    "        #contenedor {" +
                                    "            width: 100%;" +

                                    "            font: 2em arial, helvetica, sans-serif;" +
                                    "            background-color: #073763;" +
                                    "        }" +
                                    "" +
                                    "        #tam {" +
                                    "            margin-top: -40px;" +
                                    "            padding-top: 50px;" +
                                    "            padding-bottom: 50px;" +
                                    "            font: 1.5em arial, helvetica, sans-serif;" +
                                    "            width: 100%;" +
                                    "        }" +
                                    ".cuerpocorreo{" +
                                    "           color: #F1c232;" +
                                    "           background-color: #F1c232;" +
                                    "        }" +
                                    "</style>" +
                                    "<title>COOPERATIVA UPS le informa que el acceso a la plataforma CUPS Virtual fue:</title>" +
                                    "</head>" +
                                    "<body bgcolor='#F1c232'>" +
                                    "<div id='contenedor'>" +
                                    "            <h1 id='heading'> CUPS COOPERATIVA AHORRO Y CREDITO </h1>" +
                                    "            <h2 id='heading'>SISTEMA TRANSACCIONAL</h2>" +
                                    "</div>" +
                                    "<div id='tam' class='cuerpocorreo'> " +
                                    "<h2>INICIO DE SESIÓN</h2>"+
                                    "<p>Estimado(a):</p>" +
                                    "<h2> " + cliente.getNombre() + " " + cliente.getApellido() + "</h2>" +
                                    "<p><b>COOPERATIVA UPS</b> le informa que el acceso a la plataforma CUPS Virtual fue:</p>" +
                                    "<h2> CORRECTO </h2>"+
                                    "<p>Fecha:" + fecha + "&nbsp;&nbsp;" + "Hora:" + hora + "</p>" +
                                    "<p>Si ud. NO ha intentado acceder a nuestra plataforma , comuníquese con nuestro Call Center: (593)0939279776</p>" +
                                    " </div>" +
                                    "<footer>" +
                                    " <p id='heading'> 2020 Cups. All Rights Reserved</p>" +
                                    " </footer>" +
                                    "</body>" +
                                    "</html>";
                    generarCorreoSinArchivosAdjuntos(cliente.getCorreo(), asunto, cuerpo);
                    break;
                case "AccesoIncorrecto":
                    asunto = "Inicio Sesion";
                    cuerpo = "<html>" +
                                    "<head>" +
                                    "<style>" +
                                    "        #heading {" +
                                    "            color: #F1c232;" +
                                    "            text-align: center;" +
                                    "        }" +
                                    "" +
                                    "        h1 {" +
                                    "            color: black;" +
                                    "            text-align: center;" +
                                    "        }" +
                                    "" +
                                    "        h2 {" +
                                    "            color: black;" +
                                    "            text-align: center;" +
                                    "        }" +
                                    "" +
                                    "        p {" +
                                    "            color: black;" +
                                    "            text-align: center;" +
                                    "        }" +
                                    "" +
                                    "        header {" +
                                    "            background-color: #073763;" +
                                    "        }" +
                                    "" +
                                    "        footer {" +
                                    "            background-color: #073763;" +
                                    "            margin-top: -30px;" +
                                    "            padding-top: 50px;" +
                                    "            padding-bottom: 50px;" +
                                    "            font: 1em arial, helvetica, sans-serif;" +
                                    "        }" +
                                    "" +
                                    "        #contenedor {" +
                                    "            width: 100%;" +

                                    "            font: 2em arial, helvetica, sans-serif;" +
                                    "            background-color: #073763;" +
                                    "        }" +
                                    "" +
                                    "        #tam {" +
                                    "            margin-top: -40px;" +
                                    "            padding-top: 50px;" +
                                    "            padding-bottom: 50px;" +
                                    "            font: 1.5em arial, helvetica, sans-serif;" +
                                    "            width: 100%;" +
                                    "        }" +
                                    ".cuerpocorreo{" +
                                    "           color: #F1c232;" +
                                    "           background-color: #F1c232;" +
                                    "        }" +
                                    "</style>" +
                                    "<title>COOPERATIVA UPS le informa que el acceso a la plataforma CUPS Virtual fue:</title>" +
                                    "</head>" +
                                    "<body bgcolor='#F1c232'>" +
                                    "<div id='contenedor'>" +
                                    "            <h1 id='heading'> CUPS COOPERATIVA AHORRO Y CREDITO </h1>" +
                                    "            <h2 id='heading'>SISTEMA TRANSACCIONAL</h2>" +
                                    "</div>" +
                                    "<div id='tam' class='cuerpocorreo'> " +
                                    "<h2>INICIO DE SESIÓN</h2>"+
                                    "<p>Estimado(a):</p>" +
                                    "<h2> " + cliente.getNombre() + " " + cliente.getApellido() + "</h2>" +
                                    "<p><b>COOPERATIVA UPS</b> le informa que el acceso a la plataforma CUPS Virtual fue:</p>" +
                                    "<h2> INCORRECTO </h2>"+
                                    "<p>Fecha:" + fecha + "&nbsp;&nbsp;" + "Hora:" + hora + "</p>" +
                                    "<p>Si ud. NO ha intentado acceder a nuestra plataforma , comuníquese con nuestro Call Center: (593)0939279776</p>" +
                                    " </div>" +
                                    "<footer>" +
                                    " <p id='heading'> 2020 Cups. All Rights Reserved</p>" +
                                    " </footer>" +
                                    "</body>" +
                                    "</html>";
                    generarCorreoSinArchivosAdjuntos(cliente.getCorreo(), asunto, cuerpo);
                    break;
                default:
            }
        }else{
            System.out.println("Los datos del cliente son NULL");
        }
    }


}
