package ec.edu.ups.appdis.on;

import ec.edu.ups.appdis.dao.ClienteDAO;
import ec.edu.ups.appdis.dao.CuentaAhorroDAO;
import ec.edu.ups.appdis.modelo.*;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;

@Stateless
public class GestionMovil {

    @Inject
    private GestionLoginLocal gestionLoginLocal;
    @Inject
    private GestionCorreo gestionCorreo;
    @Inject
    private GestionCuentas gestionCuentas;
    @Inject
    private GestionClientes gestionClientes;
    @Inject
    private GestionTransaccionalLocal gestionTransaccionalLocal;
    @Inject
    private GestionCreditos gestionCreditos;

    public Mensaje authentication(Cliente cliente) throws Exception {
        System.out.println("LLEGUE");
        Mensaje mensaje = new Mensaje();
        Cliente clienteBuscado = gestionClientes.buscarClienteUsuario(cliente.getUsuario());
        if (clienteBuscado == null) {
            System.out.println("ENTRE AL NULO PRINCIPAL");
            mensaje.setCodigo(2);
            mensaje.setMensaje("ERROR");
            return mensaje;
        } else {
            if (cliente.getUsuario().equals(clienteBuscado.getUsuario()) && cliente.getContrasena().equals(clienteBuscado.getContrasena())) {
                CompletableFuture.runAsync(() -> {
                    try {
                        gestionLoginLocal.insertCorrectAcces(clienteBuscado);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
                System.out.println("ENTRE AL NULO PRINCIPAL");
                mensaje.setCodigo(1);
                mensaje.setMensaje("OK");
                return mensaje;
            } else {
                CompletableFuture.runAsync(() -> {
                    try {
                        gestionLoginLocal.insertIncorrectAcces(clienteBuscado);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
                mensaje.setCodigo(2);
                mensaje.setMensaje("ERROR");
                return mensaje;
            }
        }
    }

    public Mensaje changePassword(String correo) throws Exception {
        Mensaje mensaje = new Mensaje();
        Date date = new Date();
        if (correo.isEmpty()) {
            mensaje.setCodigo(2);
            mensaje.setMensaje("ERROR");
            return mensaje;
        } else {
            Cliente clienteBuscado = gestionClientes.updatePassword(correo);
            if (clienteBuscado != null) {
                String contrasenaNueva = "CUps@#$/2020";
                clienteBuscado.setContrasena(contrasenaNueva);
                gestionClientes.guardarCliente(clienteBuscado);
                CompletableFuture.runAsync(()->{
                    try {
                        gestionCorreo.notificacionCorreo(clienteBuscado, "CambioContrasena");
                    } catch (MessagingException e) {
                        e.printStackTrace();
                    }
                });
                mensaje.setCodigo(1);
                mensaje.setMensaje("OK");
                return mensaje;
            } else {
                mensaje.setCodigo(2);
                mensaje.setMensaje("ERROR");
                return mensaje;
            }
        }
        //return mensaje;
    }

    public CuentaAhorro summaryAccount(String numeroCuenta) throws Exception {
        return gestionCuentas.buscarCuenta(numeroCuenta);
    }

    public List<CuentaAhorro> getClientes(String numeroCuenta) throws Exception {
        return gestionCuentas.buscarCuentaLike(numeroCuenta);
    }

    public Mensaje saveTransferenciaInterna(Transferencia transferencia) throws Exception {
        return gestionTransaccionalLocal.guardarTranferencia(transferencia.getCuentaOrigen(), transferencia.getCuentaDestino(), transferencia.getMonto());
    }

    public Mensaje saveTransferenciaExterna(Transferencia transferencia) throws Exception {
        return gestionTransaccionalLocal.guardarTransferenciaInterBancaria(transferencia.getCuentaDestino(), transferencia.getMonto());
    }

    public Credito summaryCredito(String numeroCuenta) throws Exception {
        return gestionCreditos.buscarCreditoCliente(numeroCuenta);
    }

    public CuentaAhorro readCuenta(String usuario) throws Exception {
        Cliente clienteBuscado = gestionClientes.buscarClienteUsuario(usuario);
        if (clienteBuscado != null) {
            return gestionCuentas.searchSavingsAccount(clienteBuscado.getCedula());
        }
        return null;
    }


}
