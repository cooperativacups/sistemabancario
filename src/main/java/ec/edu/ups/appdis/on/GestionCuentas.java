/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.on;

import ec.edu.ups.appdis.dao.CuentaAhorroDAO;
import ec.edu.ups.appdis.modelo.CuentaAhorro;
import java.util.List;
import java.util.Properties;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * En esta Clase Define el CRUD para la tabla CuentaAhorro
 * @author Ismael Castillo
 * @version 08/06/2020
 */
@Stateless
public class GestionCuentas {
    
    @Inject
    CuentaAhorroDAO cuentaAhorroDAO;
    
    public void guardarCuenta(CuentaAhorro cuentaAhorro) throws Exception {
        CuentaAhorro u = cuentaAhorroDAO.read(cuentaAhorro.getNumeroCuenta());
        if(u != null){
            cuentaAhorroDAO.update(cuentaAhorro);
        }else{           
            cuentaAhorroDAO.insert(cuentaAhorro);          
        }
    }
   
    public List<CuentaAhorro> listarCuentas() throws Exception {
        return cuentaAhorroDAO.selectAll();
    }
   
    public void eliminarCuenta(String numeroCuenta) throws Exception {
        cuentaAhorroDAO.delete(numeroCuenta);
    }
   
    public CuentaAhorro buscarCuenta(String numeroCuenta) throws Exception {
        return cuentaAhorroDAO.read(numeroCuenta);
    }
       
    public List<CuentaAhorro> buscarCuentaLike(String numeroCuenta) throws Exception {
        List<CuentaAhorro> cuentasAhorros = cuentaAhorroDAO.selectLike(numeroCuenta);
        return cuentasAhorros;
    }
    
    public CuentaAhorro searchSavingsAccount(String cedula){
        return cuentaAhorroDAO.searchSavingsAccount(cedula);
    }
    
  
    
}
