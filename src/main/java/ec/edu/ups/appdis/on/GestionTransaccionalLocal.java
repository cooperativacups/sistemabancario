/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.on;

import ec.edu.ups.appdis.dao.ClientePaginaDAO;
import ec.edu.ups.appdis.modelo.Cliente;
import ec.edu.ups.appdis.modelo.Mensaje;
import ec.edu.ups.appdis.modelo.Transaccion;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * En esta Clase es de manera local y tenemos los metodos
 *
 * @author Jorge Arévalo
 * @version 08/06/2020
 */
@Local
public interface GestionTransaccionalLocal {

    public Mensaje saveTransactionDeposit(String numeroCuenta, double monto)throws Exception;
    
    public Mensaje saveTransactionWithdrawal(String numeroCuenta, double monto)throws Exception;
    
    public Mensaje guardarTranferencia(String cuentaOrige, String cuentaDestino, double monto) throws Exception;
    
    public List<Transaccion> obtenerTransacciones(String numeroCuenta, String concepto, String fechaInicial, String fechaFinal) throws Exception;

    public List<Transaccion> getTransaccions();

    public List<Transaccion> obtenerUltimasTransacciones(String numeroCuenta, String fechaInicial, String fechaFinal) throws Exception;
    
    public Mensaje guardarTransferenciaInterBancaria(String cuentaDestino, double monto) throws Exception;

}
