/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.on;

import ec.edu.ups.appdis.dao.AccesoDAO;
import ec.edu.ups.appdis.dao.ClientePaginaDAO;
import ec.edu.ups.appdis.dao.CuentaAhorroDAO;
import ec.edu.ups.appdis.dao.TransaccionDAO;
import ec.edu.ups.appdis.modelo.Acceso;
import ec.edu.ups.appdis.modelo.Cliente;
import ec.edu.ups.appdis.modelo.CuentaAhorro;
import ec.edu.ups.appdis.modelo.Transaccion;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * En esta Clase Define el CRUD de CuentaAhorro, Transaccion y CLiente
 * @author Jorge Arévalo, Gustavo Guallpa
 * @version 08/06/2020
 */
@Stateless
public class GestionPaginaCliente implements GestionPaginaClienteLocal {

    @Inject
    private CuentaAhorroDAO cuentaAhorroDAO;

    @Inject
    private TransaccionDAO transaccionDAO;

    @Inject
    private ClientePaginaDAO usuarioDAO;

    @Inject
    private AccesoDAO accesoDAO;

    @Override
    public CuentaAhorro buscaCuentaAhorro(String numCuenta) throws Exception {

        return cuentaAhorroDAO.read(numCuenta);

    }

    @Override
    public Transaccion buscaTransaccion(String codigo) throws Exception {
        return transaccionDAO.leer(codigo);
    }

    @Override
    public Cliente buscaCliente(String cedula) throws Exception {
        return usuarioDAO.read(cedula);
    }

    @Override
    public List<Cliente> listarPersonas() {
        List<Cliente> personas = new ArrayList<>();
        personas = usuarioDAO.selectAll();
        return personas;
    }

    @Override
    public List<CuentaAhorro> listarCuentas() {
        List<CuentaAhorro> cuentaAhorros = new ArrayList<>();
        cuentaAhorros = cuentaAhorroDAO.selectAll();
        return cuentaAhorros;
    }

    @Override
    public List<Transaccion> listarTransacciones() {
        List<Transaccion> transaccions = new ArrayList<>();
        transaccions = transaccionDAO.listar();
        return transaccions;
    }

    @Override
    public CuentaAhorro searchSavingsAccount(String NumeroCuenta) {
        return cuentaAhorroDAO.read(NumeroCuenta);
    }

    @Override
    public CuentaAhorro searchSavingsAccountCedula(String cedula) {
        return cuentaAhorroDAO.searchSavingsAccount(cedula);
    }

    @Override
    public List<Acceso> getCorrectAccesses(String cedula) {
        try {
            return accesoDAO.getCorrectAccesses(cedula);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public List<Acceso> getIncorrectAccesses(String cedula) {
        try {
            return accesoDAO.getIncorrectAccesses(cedula);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void updateBalance(CuentaAhorro cuentaAhorro) {
        try {
            cuentaAhorroDAO.update(cuentaAhorro);
        } catch (Exception e) {
        }
    }

    @Override
    public String getLastDateTransacction(String cuenta) {
        System.out.println("FECHA ULTIMA TRANSACCION");
        try {
            
            System.out.println(transaccionDAO.getLastDateTransacction(cuenta));
            return transaccionDAO.getLastDateTransacction(cuenta);
            
        } catch (Exception e) {
            throw e;
        }
        
    }

}