/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appdis.on;

import ec.edu.ups.appdis.dao.AccesoDAO;
import ec.edu.ups.appdis.dao.ClienteLoginDAO;
import ec.edu.ups.appdis.dao.EmpleadoLoginDAO;
import ec.edu.ups.appdis.modelo.Acceso;
import ec.edu.ups.appdis.modelo.Cliente;
import ec.edu.ups.appdis.modelo.Empleado;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;

/**
 * En esta Clase define  los metoodo que  a ser accedidos desde el Bean
 * @author Gustavo Guallpa
 * @version 08/06/2020
 */
@Stateless
public class GestionLogin implements GestionLoginLocal {

    @Inject
    private ClienteLoginDAO udao;

    @Inject
    private AccesoDAO adao;

    @Inject
    private EmpleadoLoginDAO edao;
    
    @Inject
    private GestionLoginLocal gul;
    
    @Inject 
    private GestionCorreo gestionCorreo;

    @Override
    public Cliente searchCustomer(String nombreUsuario) {
        try {
            return udao.searchCustomer(nombreUsuario);
        } catch (Exception e) {
            System.out.println("ERRROR ON");
        }
        return null;
    }

    @Override
    public Empleado searchEmployee(String nombreUsuario) {
        try {
            return edao.searchEmployee(nombreUsuario);
        } catch (Exception e) {
            System.out.println("ERRROR ON");
        }
        return null;
    }

    @Override
    public Empleado findEmployee(String cedula) {
        try {
            return edao.read(cedula);
        } catch (Exception e) {
            System.out.println("ERRROR ON");
        }
        return null;
    }

    
   
  
    
    @Override
    public void insertCorrectAcces(Cliente cliente) {
        try {
             Acceso acceso = new Acceso();
            //Hora
            Date date = new Date();
            DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
            String hora = hourFormat.format(date);
            acceso.setHora(hora);
            //fecha
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            String fecha = dateFormat.format(date);
            acceso.setFecha(fecha);

            acceso.setCliente(cliente);
            acceso.setDecripcion("CORRECTO");
            //adao.insertAcces(acceso);
            //gestionCorreo.notificacionCorreo(cliente, "AccesoCorrecto");
           
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
    
    
     @Override
    public void insertIncorrectAcces(Cliente cliente) {
        try {

            Acceso acceso = new Acceso();
            //Hora
            Date date = new Date();
            DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
            String hora = hourFormat.format(date);
            acceso.setHora(hora);
            //fecha
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            String fecha = dateFormat.format(date);
            acceso.setFecha(fecha);

            acceso.setCliente(cliente);
            acceso.setDecripcion("INCORRECTO");
            //adao.insertAcces(acceso);
            //gestionCorreo.notificacionCorreo(cliente, "AccesoIncorrecto");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

}
