package ec.edu.ups.appdis.on;

import ec.edu.ups.appdis.dao.AmortizacionDAO;
import ec.edu.ups.appdis.modelo.Amortizacion;
import ec.edu.ups.appdis.modelo.Credito;
import ec.edu.ups.appdis.modelo.CuentaAhorro;
import ec.edu.ups.appdis.modelo.Mensaje;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author Jorge Arevalo
 * @version 1.0
 * @since 30/07/2020
 */
@Stateless
public class GestionCobrosAutomaticos {

    @Inject
    private GestionCreditos gestionCreditos;
    @Inject
    private GestionAmortizacion gestionAmortizaciones;
    @Inject
    AmortizacionDAO amortizacionDAO;
    @Inject
    private GestionPaginaClienteLocal gestionPaginaClienteLocal;
    @Inject
    GestionTransaccionalLocal gestionTransaccionalLocal;

    /**
     * Metodo para realizar los cobros automaticos
     * 
     * Este metodo recorre cada uno de los creditos aprobados dentro de la cooperativas
     * con cada uno de los creditos va a verificar estado de cada uno de sus pagos de su tabla de amortizacion.
     */
    public void realizarCobrosAutomaticos() {
        try {
            List<Credito> listadoCreditos = gestionCreditos.selectAll();
            for (Credito credito : listadoCreditos) {
                verificarPagosActuales(credito.getNumeroCredito(), credito.getCuentaAhorro().getNumeroCuenta());
            }
        } catch (Exception e) {
        }

    }
    
    /**
     * Metodo que verifica si el cliente tiene saldo para en base a ello poder realizar los cobros
     * @param numeroCuenta del cliente.
     * @return el saldo de la cuenta de ahorros
     */
    public double verificarSaldoCuentaAhorro(String numeroCuenta) {
        CuentaAhorro cuentaAhorro = gestionPaginaClienteLocal.searchSavingsAccount(numeroCuenta);
        double saldoCuenta = cuentaAhorro.getSaldo();
        return saldoCuenta;
    }

    /***
     * Metodo que va verificar el respectivo pago de cada una de las cuotas
     * @param codigoCredito para buscar la tabla de amortizacion y ver el estado de los pagos.
     * @param numeroCuenta de ahorros del cleinte para poder debitar el dinero del pago de cada una las cuotas o pagos que realice.
     */
    public void verificarPagosActuales(int codigoCredito, String numeroCuenta) {
        try {

            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String fecha = dateFormat.format(date);
            //Date fechActual;
            Date fechActual = dateFormat.parse(fecha);
            //****VERIFICA LA CUOTAS PENDIENTES
            //List<Amortizacion> listaAmortizaciones = gestionAmortizaciones.mostrarAmortizacionCliente(codigoCredito);
            List<Amortizacion> listaAmortizaciones = gestionAmortizaciones.mostrarAmortizacionClientePendientes(codigoCredito);
            for (Amortizacion amo : listaAmortizaciones) {
                String fecha1 = dateFormat.format(amo.getFechaPago());
                Date fe = dateFormat.parse(fecha1);
                //1. PROCESO QUE HACE EL COBRO DE LA CUOTA LA FECHA QUE ES CUANDO EL USUARIO TIENE LA CANTIDA DE LA CUOTA
                //HORA:18:00
                if (fe.equals(fechActual)) {
                    //VERIFICAMOS SI TIENE EL DIENRO PARA PAGAR LA CUOTA
                    double saldoCuenta = verificarSaldoCuentaAhorro(numeroCuenta);
                    if (saldoCuenta >= amo.getSaldoCuota()) {
                        Mensaje mensaje = gestionTransaccionalLocal.saveTransactionWithdrawal(numeroCuenta, amo.getSaldoCuota());
                        if (mensaje.getCodigo() == 1) {
                            amo.setEstadoCuota("PAGADO");
                            amo.setSaldoCuota(0);
                            amortizacionDAO.update(amo);
                            //AQUI DEBEMOS ACTUALIZAR EL ESTAO DEL LA FECHA DEL CREDITO DEL CLIENTE
                            Credito c = gestionCreditos.buscarCredito(amo.getCredito().getNumeroCredito());
                            Amortizacion am = gestionAmortizaciones.obtenerUltimaCuoa(amo.getCredito().getNumeroCredito());
                            c.setFechaVencimiento(am.getFechaPago());
                            c.setSaldoCredito(amo.getSaldoFinal());
                            gestionCreditos.actualizarCredito(c);
                            System.out.println("CUOTA COBRADA");
                        }
                    } /*else {
                        //CUANDO USUARIO NO TIENE DINERO
                        amo.setEstadoCuota("VENCIDO");
                    }*/
                }
                ///VERIFICA QUE LA FECHA DE PAGO DE LA AMORTIZACION ES ANTES A LA FECHA ACTUAL
                else if (fe.before(fechActual)){
                    amo.setEstadoCuota("VENCIDO");
                }

                 
            }
            //****LISTADO CON LA VENCIDAS
            List<Amortizacion> listaAmortizaciones1 = gestionAmortizaciones.mostrarAmortizacionClienteVencidas(codigoCredito);
            for (Amortizacion amo2 : listaAmortizaciones1) {
                double saldoCuenta = verificarSaldoCuentaAhorro(numeroCuenta);
                if (saldoCuenta >= 0) {
                    //USUARIO TIENE MAS DE LO QUE DEBE PAGAR DE LA CUOTA
                    if (saldoCuenta >= amo2.getSaldoCuota()) {
                        Mensaje mensaje = gestionTransaccionalLocal.saveTransactionWithdrawal(numeroCuenta, amo2.getSaldoCuota());
                        if (mensaje.getCodigo() == 1) {
                            amo2.setEstadoCuota("PAGADO");
                            amo2.setSaldoCuota(0);
                            amortizacionDAO.update(amo2);
                            //AQUI DEBEMOS ACTUALIZAR EL ESTAO DEL LA FECHA DEL CREDITO DEL CLIENTE
                            Credito c = gestionCreditos.buscarCredito(amo2.getCredito().getNumeroCredito());
                            Amortizacion am = gestionAmortizaciones.obtenerUltimaCuoa(amo2.getCredito().getNumeroCredito());
                            c.setFechaVencimiento(am.getFechaPago());
                            c.setSaldoCredito(amo2.getSaldoFinal());
                            gestionCreditos.actualizarCredito(c);
                            System.out.println("CUOTA COBRADA");
                        }
                    } else {
                        //LE DESCUENTO TODO
                        Mensaje mensaje = gestionTransaccionalLocal.saveTransactionWithdrawal(numeroCuenta, saldoCuenta);
                        if (mensaje.getCodigo() == 1) {
                            //CUANDO TENGA UN VALOR MENOR AL DE LA CUOTA
                            double valorPendiente = redondearDecimales(amo2.getSaldoCuota() - saldoCuenta, 2);
                            if (valorPendiente == 0) {
                                amo2.setEstadoCuota("PAGADO");
                                amo2.setSaldoCuota(0);
                                amortizacionDAO.update(amo2);
                                //AQUI DEBEMOS ACTUALIZAR EL ESTAO DEL LA FECHA DEL CREDITO DEL CLIENTE
                                Credito c = gestionCreditos.buscarCredito(amo2.getCredito().getNumeroCredito());
                                Amortizacion am = gestionAmortizaciones.obtenerUltimaCuoa(amo2.getCredito().getNumeroCredito());
                                c.setFechaVencimiento(am.getFechaPago());
                                c.setSaldoCredito(amo2.getSaldoFinal());
                                gestionCreditos.actualizarCredito(c);
                                System.out.println("CUOTA PENDIENTE COBRADA");
                            } else {
                                amo2.setSaldoCuota(valorPendiente);
                                amortizacionDAO.update(amo2);
                                System.out.println("FALTA PAGAR EL ESTADO DE SU CUOTA");
                            }

                        }
                    }
                }
            }
        } catch (Exception e) {
        }

    }
    
    /***
     * Metodo para redonder decimales
     * @param valorInicial representa la cantidad a redondear.
     * @param numeroDecimales que se quiere tener
     * @return 
     */

    public static double redondearDecimales(double valorInicial, int numeroDecimales) {
        double parteEntera, resultado;
        resultado = valorInicial;
        parteEntera = Math.floor(resultado);
        resultado = (resultado - parteEntera) * Math.pow(10, numeroDecimales);
        resultado = Math.round(resultado);
        resultado = (resultado / Math.pow(10, numeroDecimales)) + parteEntera;
        return resultado;
    }

}
